package com.alacritystudios.encore.application;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.media.audiofx.BassBoost;
import android.media.audiofx.Equalizer;
import android.os.CountDownTimer;
import android.support.v4.content.LocalBroadcastManager;
import android.util.TypedValue;

import com.alacritystudios.encore.R;
import com.alacritystudios.encore.activities.LandingActivity;
import com.alacritystudios.encore.interfaces.MusicProviderCallback;
import com.alacritystudios.encore.providers.MusicProvider;
import com.alacritystudios.encore.utils.AppStartUtil;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

/**
 * Application class to be used.
 */

public class EncoreApplication extends Application {
    private static Context mContext;
    private static MusicProvider mMusicProvider;

    public static Context getmContext() {
        return mContext;
    }

    public static void setmContext(Context mContext) {
        EncoreApplication.mContext = mContext;
    }

    public static MusicProvider getmMusicProvider() {
        return mMusicProvider;
    }

    public static void setmMusicProvider(MusicProvider mMusicProvider) {
        EncoreApplication.mMusicProvider = mMusicProvider;
    }

    public static void updateMusicProvider() {
        final MusicProvider musicProvider = new MusicProvider();
        musicProvider.retrieveMediaAsync(new MusicProviderCallback() {
            @Override
            public void onMusicCatalogReady(boolean success) {
                mMusicProvider = musicProvider;
                AppStartUtil.sendReloadLibraryLocalBroadcast();
            }
        });
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        AppStartUtil.setDefaultColorScheme(mContext);
        mContext.setTheme(AppStartUtil.getCurrentColorSchemeTheme(mContext, false));
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
    }
}
