package com.alacritystudios.encore.enums;

/**
 * Represents the type of MediaItem
 */

public enum MediaItemType {
    MEDIA_ITEM_SONG,
    MEDIA_ITEM_ARTIST,
    MEDIA_ITEM_ALBUM,
    MEDIA_ITEM_PLAYLIST
}
