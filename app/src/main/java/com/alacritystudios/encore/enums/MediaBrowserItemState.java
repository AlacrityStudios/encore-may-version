package com.alacritystudios.encore.enums;

/**
 * Tells the adapter how to render the media item.
 */

public enum MediaBrowserItemState {

    ALBUM_ART_ONLY,
    ANIMATION_ONLY,
    OPTIONS_ONLY,
    ALBUM_ART_ANIMATION,
    ALBUM_ART_OPTIONS,
    ANIMATION_OPTIONS,
    ALL,
    NONE
}
