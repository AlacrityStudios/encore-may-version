package com.alacritystudios.encore.enums;

/**
 * Represents the state the MusicProvider instance is in.
 */

public enum MusicProviderState {
    NON_INITIALIZED,
    INITIALIZING,
    INITIALIZED
}
