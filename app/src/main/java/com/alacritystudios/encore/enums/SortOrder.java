package com.alacritystudios.encore.enums;

/**
 * Created by anuj on 9/5/17.
 */

public enum SortOrder {
    ASCENDING,
    DESCENDING
}
