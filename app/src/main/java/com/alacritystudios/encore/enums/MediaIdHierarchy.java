package com.alacritystudios.encore.enums;

/**
 * Represents the hierarchy of media item
 */

public enum MediaIdHierarchy {
    ROOT_TIER,
    FIRST_TIER,
    SECOND_TIER,
    THIRD_TIER,
    UNKNOWN_TIER
}
