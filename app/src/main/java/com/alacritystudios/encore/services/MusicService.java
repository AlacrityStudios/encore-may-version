package com.alacritystudios.encore.services;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaMetadata;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaBrowserServiceCompat;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;

import com.alacritystudios.encore.R;
import com.alacritystudios.encore.application.EncoreApplication;
import com.alacritystudios.encore.constants.CatalogueConstants;
import com.alacritystudios.encore.enums.MusicProviderState;
import com.alacritystudios.encore.interfaces.MediaSessionServiceCallback;
import com.alacritystudios.encore.interfaces.MusicProviderCallback;
import com.alacritystudios.encore.notifications.MediaNotificationManager;
import com.alacritystudios.encore.playback.PlaybackManager;
import com.alacritystudios.encore.utils.MediaIdUtil;
import com.alacritystudios.encore.utils.PreferenceUtil;
import com.alacritystudios.encore.utils.ProviderUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;

/**
 * Service to fetch and run the music.
 */

public class MusicService extends MediaBrowserServiceCompat implements MediaSessionServiceCallback {

    private String MEDIA_ID_ROOT = "__ROOT__";
    private MediaSessionCompat mMediaSessionCompat;
    private PlaybackManager mPlaybackManager;
    private MediaNotificationManager mMediaNotificationManager;

    // The action of the incoming Intent indicating that it contains a command
    // to be executed (see {@link #onStartCommand})
    public static final String ACTION_CMD = "com.example.android.uamp.ACTION_CMD";
    // The key in the extras of the incoming Intent indicating the command that
    // should be executed (see {@link #onStartCommand})
    public static final String CMD_NAME = "CMD_NAME";
    // A value of a CMD_NAME key in the extras of the incoming Intent that
    // indicates that the music playback should be paused (see {@link #onStartCommand})
    public static final String CMD_PAUSE = "CMD_PAUSE";

    @Override
    public void onCreate() {
        super.onCreate();
        mMediaSessionCompat = new MediaSessionCompat(this, "MusicService");
        setSessionToken(mMediaSessionCompat.getSessionToken());
        mMediaSessionCompat.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS |
                MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS | MediaSessionCompat.FLAG_HANDLES_QUEUE_COMMANDS);
        mPlaybackManager = new PlaybackManager(this, EncoreApplication.getmMusicProvider(), this);
        mMediaSessionCompat.setCallback(mPlaybackManager.getmMediaSessionCallback());
        mMediaNotificationManager = new MediaNotificationManager(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            String action = intent.getAction();
            String command = intent.getStringExtra(CMD_NAME);
            if (ACTION_CMD.equals(action)) {
                if (CMD_PAUSE.equals(command)) {
                    mPlaybackManager.handlePauseRequest();
                }
            }
        }
        return START_STICKY;
    }

    @Override
    public void onNotificationRequired() {
        mMediaNotificationManager.startNotification();
    }

    @Override
    public void onPlaybackStateChanged(PlaybackStateCompat playbackStateCompat) {
        mMediaSessionCompat.setPlaybackState(playbackStateCompat);
        if (playbackStateCompat.getState() == PlaybackStateCompat.STATE_PLAYING) {
            mMediaSessionCompat.setActive(true);
            startService(new Intent(getApplicationContext(), MusicService.class));
        } else if (playbackStateCompat.getState() == PlaybackStateCompat.STATE_STOPPED || playbackStateCompat.getState() == PlaybackStateCompat.STATE_ERROR) {
            mMediaSessionCompat.setActive(false);
            stopForeground(true);
        }
    }

    @Override
    public void onMetadataUpdate(MediaMetadataCompat mediaMetadataCompat) {
        MediaMetadataCompat.Builder metadataBuilder = new MediaMetadataCompat.Builder(mediaMetadataCompat);
        if (PreferenceUtil.getShowNowPlayingOnLockScreenPreference(getApplicationContext())) {
            Bitmap art;
            Uri albumArtUri = ProviderUtil.getAlbumArtworkUri(mediaMetadataCompat.getBundle().getLong(CatalogueConstants.GeneralConstants.CUSTOM_METADATA_ALBUM_ID));
            try {
                art = MediaStore.Images.Media.getBitmap(EncoreApplication.getmContext().getContentResolver(), albumArtUri);
                metadataBuilder.putBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, art);
                metadataBuilder.putBitmap(MediaMetadata.METADATA_KEY_DISPLAY_ICON, art);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        mMediaSessionCompat.setMetadata(metadataBuilder.build());
    }

    @Override
    public void onQueueUpdated(List<MediaSessionCompat.QueueItem> queueItems) {
        mMediaSessionCompat.setQueue(queueItems);
    }

    @Override
    public MediaSessionCompat getMediaSession() {
        return mMediaSessionCompat;
    }

    @Nullable
    @Override
    public BrowserRoot onGetRoot(@NonNull String clientPackageName, int clientUid, @Nullable Bundle rootHints) {
        return new BrowserRoot(MEDIA_ID_ROOT, null);
    }

    @Override
    public void onLoadChildren(@NonNull final String parentId, @NonNull final Result<List<MediaBrowserCompat.MediaItem>> result) {
        Log.i("TAG", "Loading children of parent Id : " + parentId);
        if (parentId.equals(CatalogueConstants.CATALOGUE_REFRESH)) {
            EncoreApplication.getmMusicProvider().setmCurrentState(MusicProviderState.NON_INITIALIZED);
        }
        if (!EncoreApplication.getmMusicProvider().isInitialized()) {
            result.detach();
            EncoreApplication.getmMusicProvider().retrieveMediaAsync(new MusicProviderCallback() {
                @Override
                public void onMusicCatalogReady(boolean success) {
                    if (success) {
                        onLoadChildrenImpl(parentId, result);
                    } else {
                        result.sendResult(Collections.<MediaBrowserCompat.MediaItem>emptyList());
                    }
                }
            });
        } else {
            result.detach();
            AsyncTaskParams asyncTaskParams = new AsyncTaskParams(parentId, result);
            LoadChildrenAsyncTask loadChildrenAsyncTask = new LoadChildrenAsyncTask();
            loadChildrenAsyncTask.execute(asyncTaskParams);

        }
    }

    public void onLoadChildrenImpl(@NonNull String parentId, @NonNull Result<List<MediaBrowserCompat.MediaItem>> result) {


    }

    public class LoadChildrenAsyncTask extends AsyncTask<AsyncTaskParams, Void, Void> {

        @Override
        protected Void doInBackground(AsyncTaskParams... params) {
            String parentId = params[0].parentId;
            Result<List<MediaBrowserCompat.MediaItem>> result = params[0].result;
            List<MediaBrowserCompat.MediaItem> mediaItems = new ArrayList<>();
            Set<Long> keySet;
            List<MediaMetadataCompat> mediaMetadataCompatList;
            switch (MediaIdUtil.getHierarchyLevel(parentId)) {
                case ROOT_TIER:
                    switch (parentId) {
                        case MediaIdUtil.ROOT_MEDIA_ARTISTS:
                            for (long eachArtistId : EncoreApplication.getmMusicProvider().getmMusicListByArtist().keySet()) {
                                List<MediaMetadataCompat> listItems = EncoreApplication.getmMusicProvider().getmMusicListByArtist().get(eachArtistId);
                                MediaBrowserCompat.MediaItem item = new MediaBrowserCompat.MediaItem(
                                        new MediaDescriptionCompat.Builder()
                                                .setMediaId(MediaIdUtil.buildMediaId(MediaIdUtil.ROOT_MEDIA_ARTISTS, String.valueOf(eachArtistId)))
                                                .setTitle(listItems.get(0).getString(MediaMetadataCompat.METADATA_KEY_ARTIST))
                                                .setSubtitle(EncoreApplication.getmContext().getResources().getQuantityString(R.plurals.albums, EncoreApplication.getmMusicProvider().getAlbumListsByArtist(eachArtistId).size(), EncoreApplication.getmMusicProvider().getAlbumListsByArtist(eachArtistId).size()) + " - " +
                                                        EncoreApplication.getmContext().getResources().getQuantityString(R.plurals.songs, listItems.size(), listItems.size()))
                                                .build(), MediaBrowserCompat.MediaItem.FLAG_BROWSABLE
                                );
                                mediaItems.add(item);
                            }
                            break;
                        case MediaIdUtil.ROOT_MEDIA_ALBUMS:
                            for (long eachAlbumId : EncoreApplication.getmMusicProvider().getmMusicListByAlbum().keySet()) {
                                List<MediaMetadataCompat> listItems = EncoreApplication.getmMusicProvider().getmMusicListByAlbum().get(eachAlbumId);
                                MediaBrowserCompat.MediaItem item = new MediaBrowserCompat.MediaItem(
                                        new MediaDescriptionCompat.Builder()
                                                .setMediaId(MediaIdUtil.buildMediaId(MediaIdUtil.ROOT_MEDIA_ALBUMS, String.valueOf(eachAlbumId)))
                                                .setTitle(listItems.get(0).getString(MediaMetadataCompat.METADATA_KEY_ALBUM))
                                                .setSubtitle(listItems.get(0).getString(MediaMetadataCompat.METADATA_KEY_ARTIST))
                                                .setIconUri(ProviderUtil.getAlbumArtworkUri(eachAlbumId))
                                                .build(), MediaBrowserCompat.MediaItem.FLAG_BROWSABLE
                                );
                                mediaItems.add(item);
                            }

                            break;
                        case MediaIdUtil.ROOT_MEDIA_SONGS:
                            mediaItems = EncoreApplication.getmMusicProvider().getmMediaItemListBySongs();
                            break;
                        case MediaIdUtil.ROOT_MEDIA_PLAYLISTS:
                            for (Long eachPlaylistId : EncoreApplication.getmMusicProvider().getmMusicPlaylistDetails().keySet()) {
                                MediaBrowserCompat.MediaItem item = new MediaBrowserCompat.MediaItem(
                                        new MediaDescriptionCompat.Builder()
                                                .setMediaId(MediaIdUtil.buildMediaId(MediaIdUtil.ROOT_MEDIA_PLAYLISTS, String.valueOf(eachPlaylistId)))
                                                .setTitle(EncoreApplication.getmMusicProvider().getmMusicPlaylistDetails().get(eachPlaylistId).getPlaylistName())
                                                .setSubtitle(EncoreApplication.getmContext().getResources().getQuantityString(R.plurals.songs, EncoreApplication.getmMusicProvider().getmMusicPlaylistDetails().get(eachPlaylistId).getNumberOfSongs(), EncoreApplication.getmMusicProvider().getmMusicPlaylistDetails().get(eachPlaylistId).getNumberOfSongs()))
                                                .build(), MediaBrowserCompat.MediaItem.FLAG_BROWSABLE
                                );
                                mediaItems.add(item);
                            }
                            break;
                    }
                    break;
                case FIRST_TIER:
                    switch (MediaIdUtil.getBaseMediaType(parentId)) {
                        case MediaIdUtil.ROOT_MEDIA_ARTISTS:
                            ConcurrentMap<Long, List<MediaMetadataCompat>> artistData = EncoreApplication.getmMusicProvider().getAlbumListsByArtist(MediaIdUtil.getBaseMediaValue(parentId));
                            if (artistData != null)
                                for (long eachAlbumId : artistData.keySet()) {
                                    List<MediaMetadataCompat> listItems = EncoreApplication.getmMusicProvider().getmMusicListByAlbum().get(eachAlbumId);
                                    if (listItems != null) {
                                        MediaBrowserCompat.MediaItem item = new MediaBrowserCompat.MediaItem(
                                                new MediaDescriptionCompat.Builder()
                                                        .setMediaId(MediaIdUtil.buildMediaIdWithNextHierarchy(parentId, MediaIdUtil.BRANCH_MEDIA_ALBUM, String.valueOf(eachAlbumId)))
                                                        .setTitle(listItems.get(0).getString(MediaMetadataCompat.METADATA_KEY_ALBUM))
                                                        .setSubtitle(listItems.get(0).getString(MediaMetadataCompat.METADATA_KEY_ARTIST))
                                                        .setIconUri(ProviderUtil.getAlbumArtworkUri(eachAlbumId))
                                                        .build(), MediaBrowserCompat.MediaItem.FLAG_BROWSABLE
                                        );
                                        mediaItems.add(item);
                                    }

                                }
                            break;
                        case MediaIdUtil.ROOT_MEDIA_ALBUMS:
                            mediaMetadataCompatList = EncoreApplication.getmMusicProvider().getmMusicListByAlbum().get(MediaIdUtil.getBaseMediaValue(parentId));
                            if (mediaMetadataCompatList != null) {
                                for (MediaMetadataCompat mediaMetadataCompat : mediaMetadataCompatList) {
                                    MediaBrowserCompat.MediaItem item = new MediaBrowserCompat.MediaItem(
                                            new MediaDescriptionCompat.Builder()
                                                    .setMediaId(MediaIdUtil.buildMediaIdWithNextHierarchy(parentId, MediaIdUtil.BRANCH_MEDIA_SONG, mediaMetadataCompat.getString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID)))
                                                    .setTitle(mediaMetadataCompat.getString(MediaMetadataCompat.METADATA_KEY_TITLE))
                                                    .setSubtitle(mediaMetadataCompat.getString(MediaMetadataCompat.METADATA_KEY_ARTIST) + " - " + mediaMetadataCompat.getString(MediaMetadataCompat.METADATA_KEY_ALBUM))
                                                    .setIconUri(ProviderUtil.getAlbumArtworkUri(mediaMetadataCompat.getLong(CatalogueConstants.GeneralConstants.CUSTOM_METADATA_ALBUM_ID)))
                                                    .build(), MediaBrowserCompat.MediaItem.FLAG_PLAYABLE
                                    );
                                    mediaItems.add(item);
                                }
                            }
                            break;
                        case MediaIdUtil.ROOT_MEDIA_PLAYLISTS:
                            mediaMetadataCompatList = EncoreApplication.getmMusicProvider().getmMusicListByPlaylist().get(MediaIdUtil.getBaseMediaValue(parentId));
                            if (mediaMetadataCompatList != null) {
                                for (MediaMetadataCompat mediaMetadataCompat : mediaMetadataCompatList) {
                                    MediaBrowserCompat.MediaItem item = new MediaBrowserCompat.MediaItem(
                                            new MediaDescriptionCompat.Builder()
                                                    .setMediaId(MediaIdUtil.buildMediaIdWithNextHierarchy(parentId, MediaIdUtil.BRANCH_MEDIA_SONG, mediaMetadataCompat.getString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID)))
                                                    .setTitle(mediaMetadataCompat.getString(MediaMetadataCompat.METADATA_KEY_TITLE))
                                                    .setSubtitle(mediaMetadataCompat.getString(MediaMetadataCompat.METADATA_KEY_ARTIST) + " - " + mediaMetadataCompat.getString(MediaMetadataCompat.METADATA_KEY_ALBUM))
                                                    .setIconUri(ProviderUtil.getAlbumArtworkUri(mediaMetadataCompat.getLong(CatalogueConstants.GeneralConstants.CUSTOM_METADATA_ALBUM_ID)))
                                                    .build(), MediaBrowserCompat.MediaItem.FLAG_PLAYABLE
                                    );
                                    mediaItems.add(item);
                                }
                            }

                            break;
                    }
                    break;
                case SECOND_TIER:
                    switch (MediaIdUtil.getBaseMediaType(parentId)) {
                        case MediaIdUtil.ROOT_MEDIA_ARTISTS:
                            mediaMetadataCompatList = EncoreApplication.getmMusicProvider().getmMusicListByAlbum().get(Long.parseLong(MediaIdUtil.getMediaHierarchyMap(parentId).get(MediaIdUtil.BRANCH_MEDIA_ALBUM)));
                            if (mediaMetadataCompatList != null) {
                                for (MediaMetadataCompat mediaMetadataCompat : mediaMetadataCompatList) {
                                    MediaBrowserCompat.MediaItem item = new MediaBrowserCompat.MediaItem(
                                            new MediaDescriptionCompat.Builder()
                                                    .setMediaId(MediaIdUtil.buildMediaIdWithNextHierarchy(parentId, MediaIdUtil.BRANCH_MEDIA_SONG, mediaMetadataCompat.getString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID)))
                                                    .setTitle(mediaMetadataCompat.getString(MediaMetadataCompat.METADATA_KEY_TITLE))
                                                    .setSubtitle(mediaMetadataCompat.getString(MediaMetadataCompat.METADATA_KEY_ARTIST) + " - " + mediaMetadataCompat.getString(MediaMetadataCompat.METADATA_KEY_ALBUM))
                                                    .setIconUri(ProviderUtil.getAlbumArtworkUri(mediaMetadataCompat.getLong(CatalogueConstants.GeneralConstants.CUSTOM_METADATA_ALBUM_ID)))
                                                    .build(), MediaBrowserCompat.MediaItem.FLAG_PLAYABLE
                                    );

                                    mediaItems.add(item);
                                }
                            }
                            break;
                    }
                    break;
            }
            result.sendResult(mediaItems);
            return null;
        }
    }


    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        mPlaybackManager.handleStopRequest();
        this.stopSelf();
    }

    public class AsyncTaskParams {
        public String parentId;
        public Result<List<MediaBrowserCompat.MediaItem>> result;

        public AsyncTaskParams(String parentId, Result<List<MediaBrowserCompat.MediaItem>> result) {
            this.parentId = parentId;
            this.result = result;
        }
    }
}
