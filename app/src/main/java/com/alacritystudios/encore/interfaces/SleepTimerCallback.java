package com.alacritystudios.encore.interfaces;

import android.os.CountDownTimer;

/**
 * Callback for when user presses a digit on sleep time dialogue number pad.
 */

public interface SleepTimerCallback {

    CountDownTimer fetchCountDownTimer();
    void setCountDownTimer(CountDownTimer countDownTimer);
    void onCountDownFinish();
}
