package com.alacritystudios.encore.interfaces;

/**
 * Created by anuj on 11/5/17.
 */

public interface LocalPlaybackCallback {
    void onCompletion();

    void onPlaybackStatusChanged(int exoPlayState, boolean playWhenReady);
}
