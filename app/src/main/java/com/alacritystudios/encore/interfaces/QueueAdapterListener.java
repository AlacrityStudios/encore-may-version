package com.alacritystudios.encore.interfaces;

import android.support.v4.media.MediaDescriptionCompat;

/**
 * Listens to Queue item clicks
 */

public interface QueueAdapterListener {
    void onQueueItemClicked(int position);

    void onQueueItemRemoved(MediaDescriptionCompat mediaDescriptionCompat);

    void onQueueItemMoved(int fromPosition, int toPosition);
}
