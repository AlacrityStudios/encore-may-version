package com.alacritystudios.encore.interfaces;

/**
 * Listens for clicks on the drawer layout items
 */

public interface DrawerItemClickListener {
    void onItemClicked(int position);
}
