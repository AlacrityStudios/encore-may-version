package com.alacritystudios.encore.interfaces;

/**
 * Created by anuj on 5/5/17.
 */

public interface ColorPickerCallback {
    void onColorSelected(int selectedColor);
}
