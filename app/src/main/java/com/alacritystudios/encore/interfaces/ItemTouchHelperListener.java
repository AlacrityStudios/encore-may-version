package com.alacritystudios.encore.interfaces;

/**
 * Interface to ensure move and swipe functionality is being implemented in adapters.
 */

public interface ItemTouchHelperListener {

    void onItemMove(int fromPosition, int toPosition);

    void onItemDismiss(int position);
}
