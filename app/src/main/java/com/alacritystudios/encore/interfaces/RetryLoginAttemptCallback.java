package com.alacritystudios.encore.interfaces;

/**
 * Callback to initiate login.
 */

public interface RetryLoginAttemptCallback {
    void retryLoginAttempt();
}
