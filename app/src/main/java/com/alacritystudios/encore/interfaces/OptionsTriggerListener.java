package com.alacritystudios.encore.interfaces;

import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.session.MediaSessionCompat;

/**
 * Listens to event of options request.
 */

public interface OptionsTriggerListener {
    void clickedMediaId(MediaBrowserCompat.MediaItem mediaItem);
    void clickedQueueId(MediaSessionCompat.QueueItem queueItem);
    void clickedMediaInfo(MediaBrowserCompat.MediaItem mediaItem);
}
