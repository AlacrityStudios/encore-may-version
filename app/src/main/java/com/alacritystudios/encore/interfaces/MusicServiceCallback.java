package com.alacritystudios.encore.interfaces;

import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;

/**
 * Created by anuj on 10/5/17.
 */

public interface MusicServiceCallback {
    void onNotificationRequired();

    void onPlaybackStateUpdated(PlaybackStateCompat newState);

    MediaSessionCompat getMediaSession();
}
