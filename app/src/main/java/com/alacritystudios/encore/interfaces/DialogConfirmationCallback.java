package com.alacritystudios.encore.interfaces;

/**
 * Created by anuj on 4/5/17.
 */

public interface DialogConfirmationCallback {
    void onDialogConfirm(int buttonReference);
}
