package com.alacritystudios.encore.interfaces;

import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaSessionCompat;

import java.util.List;

/**
 * Created by anuj on 11/5/17.
 */

public interface QueueManagerCallback {

    void onMetaDataUpdate(MediaMetadataCompat mediaMetadataCompat);

    void onQueueUpdated(List<MediaSessionCompat.QueueItem> queueItems);
}
