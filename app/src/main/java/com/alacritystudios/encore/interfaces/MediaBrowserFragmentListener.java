package com.alacritystudios.encore.interfaces;

import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.session.MediaControllerCompat;

import com.alacritystudios.encore.enums.SortOrder;

/**
 * Used to communicate values between browser fragments and activity
 */

public interface MediaBrowserFragmentListener {

    String getmSearchQueryString();

    MediaBrowserCompat getmMediaBrowserCompat();

    MediaControllerCompat getmMediaControllerCompat();

    void onMediaItemClicked(MediaBrowserCompat.MediaItem mediaItem);
}
