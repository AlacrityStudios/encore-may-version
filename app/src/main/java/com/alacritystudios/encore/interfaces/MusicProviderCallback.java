package com.alacritystudios.encore.interfaces;

/**
 * Callback for the music provider to inform that the music catalogue is ready for use.
 */

public interface MusicProviderCallback {
    void onMusicCatalogReady(boolean success);
}
