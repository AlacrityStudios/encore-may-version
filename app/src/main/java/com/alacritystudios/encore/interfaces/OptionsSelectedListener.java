package com.alacritystudios.encore.interfaces;

/**
 * Listens to click on options.
 */

public interface OptionsSelectedListener {

    void onSelectOption(int position, int callbackId);
}
