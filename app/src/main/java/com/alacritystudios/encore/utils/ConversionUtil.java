package com.alacritystudios.encore.utils;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;

/**
 * Utility for commonly used conversions.
 */

public class ConversionUtil {

    private static final String TAG = ConversionUtil.class.getSimpleName();

    public static int convertDpToPx(Context mContext, int dp)
    {
        Display display = ((Activity) mContext).getWindowManager().getDefaultDisplay();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        display.getMetrics(displayMetrics);
        return dp * (displayMetrics.densityDpi / 160);
    }
}
