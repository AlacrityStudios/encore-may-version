package com.alacritystudios.encore.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.media.session.PlaybackStateCompat;

/**
 * Stores the user preferences
 */

public class PreferenceUtil {

    public static final String FAVORITE_PLAYLIST_ID = "pref_favorite_playlist_id";
    public static final String LAST_VERSION_CODE = "pref_last_version_code";
    public static final String CURRENT_SORT_ORDER_SETTING = "pref_current_sort_order_setting";
    public static final String CURRENT_REPEAT_SETTING = "pref_current_repeat_setting";
    public static final String CURRENT_SHUFFLE_SETTING = "pref_current_shuffle_setting";
    public static final String CURRENT_BASS_BOOST_SETTING = "pref_current_bass_boost_setting";
    public static final String CURRENT_PRE_AMP_SETTING = "pref_current_pre_amp_setting";
    public static final String DEFAULT_EQUALIZER_CUSTOM_PRESET = "pref_default_equalizer_custom_preset";
    public static final String CURRENT_EQUALIZER_PRESET = "pref_current_equalizer_preset";
    public static final String IS_CURRENT_PRESET_CUSTOM = "pref_is_current_equalizer_preset_cutom";
    public static final String FCMTOKEN = "pref_fcm_token";
    public static final String COLOR_SCHEME = "pref_color_scheme";
    public static final String SHOW_NOTIFICATION_FOR_FRIENDS_LIKES = "pref_show_notifications_friends_likes";
    public static final String NOW_PLAYING_ON_LIKE_SCREEN = "pref_now_playing_on_lock_screen";
    public static final String SHOW_ONLINE_STATUS = "pref_online_status";
    public static final String CURRENT_SONG_ID = "pref_current_song_id";
    public static final String SIGN_INTO_FACEBOOK = "pref_account";
    public static final String FACEBOOK_IMAGE_URL = "pref_facebook_url";
    public static final String SHOW_ANIMATIONS = "pref_show_animations";
    public static final String SHOW_ALBUM_ART = "pref_show_album_art";

    public static boolean getCurrentShowAnimationPreference(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getBoolean(SHOW_ANIMATIONS, false);
    }

    public static boolean getCurrentShowAlbumArtPreference(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getBoolean(SHOW_ALBUM_ART, true);
    }

    public static String getFacebookImagePreference(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getString(FACEBOOK_IMAGE_URL, "");
    }

    public static void setFacebookImagePreference(Context mContext, String imageUrl) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        sharedPreferences.edit().putString(FACEBOOK_IMAGE_URL, imageUrl).commit();
    }

    public static int getCurrentSortOrderPreference(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getInt(CURRENT_SORT_ORDER_SETTING, 0);
    }

    public static void setCurrentSortOrderPreference(Context mContext, int sortOrder) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        sharedPreferences.edit().putInt(CURRENT_SORT_ORDER_SETTING, sortOrder).apply();
    }

    public static int getCurrentRepeatPreference(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getInt(CURRENT_REPEAT_SETTING, PlaybackStateCompat.REPEAT_MODE_NONE);
    }

    public static void setCurrentRepeatPreference(Context mContext, int repeatMode) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        sharedPreferences.edit().putInt(CURRENT_REPEAT_SETTING, repeatMode).apply();
    }

    public static boolean getCurrentShufflePreference(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getBoolean(CURRENT_SHUFFLE_SETTING, false);
    }

    public static void setCurrentShufflePreference(Context mContext, boolean enabled) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        sharedPreferences.edit().putBoolean(CURRENT_SHUFFLE_SETTING, enabled).apply();
    }

    public static boolean getCurrentFacebookAccountPreference(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getBoolean(SIGN_INTO_FACEBOOK, true);
    }

    public static void setCurrentFacebookAccountPreference(Context mContext, boolean enabled) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        sharedPreferences.edit().putBoolean(SIGN_INTO_FACEBOOK, enabled).apply();
    }

    public static boolean getShowNowPlayingOnLockScreenPreference(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getBoolean(NOW_PLAYING_ON_LIKE_SCREEN, true);
    }


    public static void setVersionCodePreference(Context mContext, String mediaId) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        sharedPreferences.edit().putString(CURRENT_SONG_ID, mediaId).apply();
    }

    public static String getCurrentSongPreference(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getString(CURRENT_SONG_ID, "");
    }

    public static void setCurrentSongPreference(Context mContext, String mediaId) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        sharedPreferences.edit().putString(CURRENT_SONG_ID, mediaId).apply();
    }

    public static void setFavoritePlaylistId(Context mContext, long playlistId) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        sharedPreferences.edit().putLong(FAVORITE_PLAYLIST_ID, playlistId).apply();
    }

    public static long getFavoritePlaylistId(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getLong(FAVORITE_PLAYLIST_ID, 0);
    }

    public static void setBassBoost(Context mContext, int bassBoost) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        sharedPreferences.edit().putInt(CURRENT_BASS_BOOST_SETTING, bassBoost).apply();
    }

    public static int getBassBoost(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getInt(CURRENT_BASS_BOOST_SETTING, 0);
    }

    public static void setAmpBoost(Context mContext, int ampBoost) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        sharedPreferences.edit().putInt(CURRENT_PRE_AMP_SETTING, ampBoost).apply();
    }

    public static int getAmpBoost(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getInt(CURRENT_PRE_AMP_SETTING, 0);
    }

    public static void setCustomPreset(Context mContext, int presetId) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        sharedPreferences.edit().putInt(DEFAULT_EQUALIZER_CUSTOM_PRESET, presetId).apply();
    }

    public static int getCustomPreset(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getInt(DEFAULT_EQUALIZER_CUSTOM_PRESET, -1);
    }

    public static void setCurrentPreset(Context mContext, int presetId, boolean isCustomPreset) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        sharedPreferences.edit().putInt(CURRENT_EQUALIZER_PRESET, presetId).apply();
        sharedPreferences.edit().putBoolean(IS_CURRENT_PRESET_CUSTOM, isCustomPreset).apply();
    }

    public static int getCurrentPreset(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getInt(CURRENT_EQUALIZER_PRESET, -1);
    }

    public static boolean isCurrentPresetCustom(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getBoolean(IS_CURRENT_PRESET_CUSTOM, false);
    }

    public static void setFCMToken(Context mContext, String token) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        sharedPreferences.edit().putString(FCMTOKEN, token).apply();
    }

    public static String getFCMToken(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getString(FCMTOKEN, "");
    }

    public static int getCurrentColorScheme(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getInt(COLOR_SCHEME, 0);
    }

    public static void setCurrentColorScheme(Context mContext, int selectedColor) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        sharedPreferences.edit().putInt(COLOR_SCHEME, selectedColor).apply();
    }

    public static boolean getOnlineStatusPreference(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getBoolean(SHOW_ONLINE_STATUS, true);
    }

    public static boolean getReceiveNotificationsPreference(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getBoolean(SHOW_NOTIFICATION_FOR_FRIENDS_LIKES, true);
    }
}
