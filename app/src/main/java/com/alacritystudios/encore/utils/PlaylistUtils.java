package com.alacritystudios.encore.utils;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.util.Log;

import com.alacritystudios.encore.R;
import com.alacritystudios.encore.application.EncoreApplication;
import com.alacritystudios.encore.models.PlaylistModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Util class for commonly used playlist operations.
 */

public class PlaylistUtils {

    public static final String TAG = "PlaylistUtils";

    public static long addNewPlaylist(ContentResolver contentResolver, PlaylistModel playlistModel, @Nullable String mediaId) {
        Log.i(TAG, "addNewPlaylist");
        Uri playlists = MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI;
        Log.d(TAG, "Checking for existing playlist for " + playlistModel.getPlaylistName());
        Cursor playlistCursor = contentResolver.query(playlists, new String[]{"*"}, null, null, null);
        long playlistId = 0;
        if (playlistCursor != null && playlistCursor.moveToFirst()) {
            do {
                String plname = playlistCursor.getString(playlistCursor.getColumnIndex(MediaStore.Audio.Playlists.NAME));
                if (plname.equalsIgnoreCase(playlistModel.getPlaylistName())) {
                    playlistId = playlistCursor.getLong(playlistCursor.getColumnIndex(MediaStore.Audio.Playlists._ID));
                    break;
                }
            } while (playlistCursor.moveToNext());
            playlistCursor.close();
        }
        if (playlistId != 0 && mediaId != null) {
            addSongToPlaylist(contentResolver, mediaId, playlistId);
            return playlistId;
        } else if (playlistId != 0 && mediaId == null) {
            EncoreApplication.updateMusicProvider();
            return playlistId;
        } else {
            Log.d(TAG, "CREATING PLAYLIST: " + playlistModel.getPlaylistName());
            ContentValues contentValues = new ContentValues();
            contentValues.put(MediaStore.Audio.Playlists.NAME, playlistModel.getPlaylistName());
            contentValues.put(MediaStore.Audio.Playlists.DATE_MODIFIED, System.currentTimeMillis());
            Uri newpl = null;
            try {
                newpl = contentResolver.insert(playlists, contentValues);
            } catch (Exception ex)
            {
                ex.printStackTrace();
            }
            Log.d(TAG, "Added PlayLIst: " + newpl);

            playlistCursor = contentResolver.query(newpl, new String[]{"*"}, null, null, null);
            if (playlistCursor != null && playlistCursor.moveToFirst()) {
                playlistId = playlistCursor.getLong(playlistCursor.getColumnIndex(MediaStore.Audio.Playlists._ID));
                if (mediaId != null) {
                    addSongToPlaylist(contentResolver, mediaId, playlistId);
                }
                playlistCursor.close();
            }
            return playlistId;
        }
    }

    public static void addSongToPlaylist(ContentResolver contentResolver, String mediaId, long playlistId) {
        Log.i(TAG, "addSongToPlaylist");
        Uri playlistMembers = MediaStore.Audio.Playlists.Members.getContentUri("external", playlistId);
        Cursor playlistMemberCursor = contentResolver.query(playlistMembers, new String[]{"*"}, null, null, null);
        ContentValues contentValues = new ContentValues();
        int playOrder = 0;
        if (playlistMemberCursor != null && playlistMemberCursor.moveToFirst()) {
            do {
                playOrder++;
                if (playlistMemberCursor.getLong(playlistMemberCursor.getColumnIndex(MediaStore.Audio.Playlists.Members.AUDIO_ID)) == MediaIdUtil.extractMusicIdFromMediaId(mediaId)) {
                    playlistMemberCursor.close();
                    return;
                }
            } while (playlistMemberCursor.moveToNext());
            playlistMemberCursor.close();
        }

        contentValues.put(MediaStore.Audio.Playlists.Members.PLAYLIST_ID, playlistId);
        contentValues.put(MediaStore.Audio.Playlists.Members.AUDIO_ID, MediaIdUtil.extractMusicIdFromMediaId(mediaId));
        contentValues.put(MediaStore.Audio.Playlists.Members.PLAY_ORDER, ++playOrder);
        contentValues.put(MediaStore.Audio.Playlists.Members.DEFAULT_SORT_ORDER, "play_order");
        Uri newpl = contentResolver.insert(playlistMembers, contentValues);
        EncoreApplication.updateMusicProvider();
    }

    public static void deleteSongFromPlaylist(ContentResolver contentResolver, String mediaId, long playlistId) {
        Log.i(TAG, "deleteSongFromPlaylist");
        Uri playlistMembers = MediaStore.Audio.Playlists.Members.getContentUri("external", playlistId);
        contentResolver.delete(playlistMembers, MediaStore.Audio.Playlists.Members.AUDIO_ID + "= ?" , new String[] { String.valueOf(MediaIdUtil.extractMusicIdFromMediaId(mediaId)) });
        EncoreApplication.updateMusicProvider();
    }

    public static void deletePlaylist(ContentResolver contentResolver, long playlistId) {
        Log.i(TAG, "deletePlaylist");
        if (playlistId != 0) {
            Uri playlists = MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI;
            Uri deleteUri = ContentUris.withAppendedId(playlists, playlistId);
            Log.d(TAG, "REMOVING Existing Playlist: " + playlistId);
            contentResolver.delete(deleteUri, null, null);
            EncoreApplication.updateMusicProvider();
        }
    }

    public static List<PlaylistModel> getAllPlaylists(ContentResolver contentResolver) {
        Log.i(TAG, "getAllPlaylists");
        List<PlaylistModel> mPlaylistModel = new ArrayList<>();
        Uri playlists = MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI;
        Cursor playlistCursor = contentResolver.query(playlists, new String[]{"*"}, null, null, null);
        long playlistId = 0;
        if (playlistCursor != null && playlistCursor.moveToFirst()) {
            do {
                PlaylistModel playlistModel = new PlaylistModel();
                playlistModel.setPlaylistName(playlistCursor.getString(playlistCursor.getColumnIndex(MediaStore.Audio.Playlists.NAME)));
                playlistModel.setPlaylistId(playlistCursor.getLong(playlistCursor.getColumnIndex(MediaStore.Audio.Playlists._ID)));
                mPlaylistModel.add(playlistModel);
            } while (playlistCursor.moveToNext());
            playlistCursor.close();
        }
        return mPlaylistModel;
    }

    public static void addToFavorites(ContentResolver contentResolver, String mediaId) {
        Log.i(TAG, "addToFavorites");
        long playlistId = PreferenceUtil.getFavoritePlaylistId(EncoreApplication.getmContext());
        if (playlistId != 0) {
            addSongToPlaylist(contentResolver, mediaId, playlistId);
        }
    }

    public static void deleteFromFavorites(ContentResolver contentResolver, String mediaId) {
        Log.i(TAG, "deleteFromFavorites");
        long playlistId = PreferenceUtil.getFavoritePlaylistId(EncoreApplication.getmContext());
        if (playlistId != 0) {
            deleteSongFromPlaylist(contentResolver, mediaId, playlistId);
        }
    }

    public static boolean isFavorite(ContentResolver contentResolver, String mediaId) {
        Log.i(TAG, "addToFavorites");
        long playlistId = PreferenceUtil.getFavoritePlaylistId(EncoreApplication.getmContext());
        Uri playlistMembers = MediaStore.Audio.Playlists.Members.getContentUri("external", playlistId);
        Cursor playlistMemberCursor = contentResolver.query(playlistMembers, new String[]{"*"}, MediaStore.Audio.Playlists.Members.AUDIO_ID + "=" + String.valueOf(MediaIdUtil.extractMusicIdFromMediaId(mediaId)), null, null);
        if (playlistMemberCursor != null && playlistMemberCursor.moveToFirst()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean findFavoritePlaylist(Context mContext) {
        Cursor playlistCursor = mContext.getContentResolver().query(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, new String[]{"*"}, MediaStore.Audio.Playlists.NAME + "= '" + mContext.getString(R.string.favorites) + "'", null, null);
        if (playlistCursor != null && playlistCursor.moveToFirst()) {
            playlistCursor.close();
            return true;
        } else {
            return false;
        }
    }
}
