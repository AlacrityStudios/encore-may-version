package com.alacritystudios.encore.utils;

import android.util.Log;

import com.alacritystudios.encore.enums.MediaIdHierarchy;
import com.alacritystudios.encore.models.MediaIdDetailsModel;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Util class to find out all data about media Ids
 */

public class MediaIdUtil {

    private static final String TAG = "MediaIdUtil";
    public static final String BASE_MEDIA_ID = "BASE_MEDIA_ID";
    public static final String ROOT_MEDIA_ID = "ROOT_MEDIA_ID";
    public static final String MEDIA_ID = "MEDIA_ID";
    public static final String ROOT_MEDIA_SONGS = "__SONGS__";
    public static final String ROOT_MEDIA_ARTISTS = "__ARTISTS__";
    public static final String ROOT_MEDIA_ALBUMS = "__ALBUMS__";
    public static final String ROOT_MEDIA_PLAYLISTS = "__PLAYLISTS__";
    public static final String BRANCH_MEDIA_ALBUM = "__ALBUMBRANCH__";
    public static final String BRANCH_MEDIA_SONG = "__SONGBRANCH__";

    //Help us to identify hierarchy
    public static final String VALUE_SEPARATOR = "|";
    public static final String TYPE_SEPARATOR = "/";

    public static MediaIdDetailsModel getAllMediaItemDetails(String inputMediaId) {
        MediaIdDetailsModel mediaIdDetailsModel = new MediaIdDetailsModel();
        mediaIdDetailsModel.setmRootHierarchy(getBaseMediaType(inputMediaId));
        mediaIdDetailsModel.setmMediaIdHierarchy(getHierarchyLevel(inputMediaId));
        mediaIdDetailsModel.setmMediaHierarchyMap(getMediaHierarchyMap(inputMediaId));
        return mediaIdDetailsModel;
    }

    public static String getBaseMediaType(String mediaId) {
        Log.i(TAG, "getBaseMediaType " + mediaId);
        if (mediaId.contains(VALUE_SEPARATOR)) {
            return mediaId.substring(0, mediaId.indexOf(VALUE_SEPARATOR));
        } else {
            return mediaId;
        }
    }

    public static Map<String, String> getMediaHierarchyMap(String mediaId) {
        Log.i(TAG, "getMediaHierarchy. MediaId: " + mediaId);
        Map<String, String> mHierarchyMap = new LinkedHashMap<>();

        while(true) {
            if (mediaId.contains(TYPE_SEPARATOR)) {
                String subString = mediaId.substring(0, mediaId.indexOf(TYPE_SEPARATOR));
                mHierarchyMap.put(subString.substring(0, mediaId.indexOf(VALUE_SEPARATOR)), subString.substring(mediaId.indexOf(VALUE_SEPARATOR) + 1));
                mediaId = mediaId.substring(mediaId.indexOf(TYPE_SEPARATOR) + 1);
            } else {
                mHierarchyMap.put(mediaId.substring(0, mediaId.indexOf(VALUE_SEPARATOR)), mediaId.substring(mediaId.indexOf(VALUE_SEPARATOR) + 1));
                break;
            }
        }
        return mHierarchyMap;
    }

    public static MediaIdHierarchy getHierarchyLevel(String mediaId) {
        Log.i(TAG, "getHierarchyLevel. MediaId: " + mediaId);
        int count = mediaId.length() - mediaId.replace(TYPE_SEPARATOR, "").length();
        switch (count) {
            case 0:
                if (mediaId.contains(VALUE_SEPARATOR)) {
                    return MediaIdHierarchy.FIRST_TIER;
                } else {
                    return MediaIdHierarchy.ROOT_TIER;
                }
            case 1:
                return MediaIdHierarchy.SECOND_TIER;
            case 2:
                return MediaIdHierarchy.THIRD_TIER;
            default:
                return MediaIdHierarchy.UNKNOWN_TIER;
        }
    }

    public static long extractMusicIdFromMediaId(String mediaId) {
        Log.i(TAG, "extractMusicIdFromMediaId. MediaId: " + mediaId);
        if (mediaId.contains(TYPE_SEPARATOR)) {
            mediaId = mediaId.substring(mediaId.lastIndexOf(VALUE_SEPARATOR + 1));
        }
        return Long.parseLong(mediaId.substring(mediaId.indexOf(VALUE_SEPARATOR) + 1));
    }

    public static String buildMediaId(String mediaId, String valueToBeAppended) {
        Log.i(TAG, "buildMediaId()");
        return mediaId + VALUE_SEPARATOR + valueToBeAppended;
    }

    public static String buildMediaIdWithNextHierarchy(String mediaId, String typeToBeAppended, String valueToBeAppended) {
        Log.i(TAG, "buildMediaIdWithNextHierarchy()");
        return mediaId + TYPE_SEPARATOR + typeToBeAppended + VALUE_SEPARATOR + valueToBeAppended;
    }

    public static Long getBaseMediaValue(String mediaId) {
        Log.i(TAG, "getBaseMediaValue " + mediaId);
        if (mediaId.contains(TYPE_SEPARATOR)) {
            return Long.parseLong(mediaId.substring(mediaId.indexOf(VALUE_SEPARATOR) + 1, mediaId.indexOf(TYPE_SEPARATOR)));
        } else {
            return Long.parseLong(mediaId.substring(mediaId.indexOf(VALUE_SEPARATOR) + 1, mediaId.length()));
        }
    }

}
