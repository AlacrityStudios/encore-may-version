package com.alacritystudios.encore.utils;

import android.util.Log;

import java.util.concurrent.TimeUnit;

/**
 * Class for commonly used utils
 */

public class CustomUtils {

    public static final String TAG = "CustomUtils";

    public static String getFormattedSongDuration(long duration) {
        return String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(duration),
                TimeUnit.MILLISECONDS.toSeconds(duration) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration)));
    }
}
