package com.alacritystudios.encore.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.TypedValue;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.alacritystudios.encore.R;
import com.alacritystudios.encore.interfaces.DialogConfirmationCallback;

import java.util.List;

/**
 * Util class to create commonly used components
 */

public class ComponentUtil {

    /**
     * Construct an AlertDialog to show retry events.
     *
     * @param context - Context to be used.
     * @param message - Error message to be shown.
     * @param intent  - Intent of the activity to be reloaded.
     * @return - AlertDialog inflated.
     */

    public static AlertDialog retryAlertDialog(final Context context, String message, final Intent intent) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setCancelable(true);
        builder.setPositiveButton(context.getString(R.string.retry), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((Activity) context).finish();
                context.startActivity(intent);
            }
        });
        final AlertDialog alertDialog = builder.create();
        final TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.colorAccent, typedValue, true);
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(typedValue.data);
            }
        });
        alertDialog.show();
        return alertDialog;
    }

    /**
     * Construct an AlertDialog to show error events.
     *
     * @param context          - Context to be used.
     * @param message          - Error message to be shown.
     * @param toFinishActivity - If current activity should be restarted or not.
     * @return - AlertDialog inflated.
     */

    public static AlertDialog errorAlertDialog(final Context context, String message, final boolean toFinishActivity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialogTheme);
        builder.setMessage(message);
        builder.setCancelable(true);
        builder.setPositiveButton(context.getString(R.string.okay), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (toFinishActivity) {
                    dialog.cancel();
                    ((Activity) context).finish();
                } else
                    dialog.cancel();
            }
        });
        final AlertDialog alertDialog = builder.create();
        final TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.colorAccent, typedValue, true);
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(typedValue.data);
            }
        });
        alertDialog.show();
        return alertDialog;
    }

    /**
     * Construct an AlertDialog to show lists.
     *
     * @param context - Context to be used.
     * @param message - List message to be shown.
     * @param title   - Title to be shown.
     * @return - AlertDialog inflated.
     */

    public static AlertDialog listAlertDialog(final Context context, List<String> message, String title) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = ((Activity) context).getLayoutInflater().inflate(R.layout.dialog_listview, null);
        ListView listView = (ListView) view.findViewById(R.id.dialog_listview);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, R.layout.recycler_view_dialog_item, message);
        listView.setAdapter(arrayAdapter);
        builder.setView(view);
        builder.setTitle(title);
        builder.setCancelable(true);
        builder.setPositiveButton(context.getString(R.string.okay), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        final AlertDialog alertDialog = builder.create();
        final TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.colorAccent, typedValue, true);
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(typedValue.data);
            }
        });
        alertDialog.show();
        return alertDialog;
    }

    /**
     * @param context                    - Context to be used.
     * @param message                    - Message to be shown.
     * @param title                      - Title to be shown.
     * @param dialogConfirmationCallback - The callback to be called on confirmation
     * @param buttonReference            - Reference of the calling button, useful for backstracing
     * @return - AlertDialog inflated.
     */
    public static AlertDialog confirmationAlertDialog(final Context context, String message, String title, final DialogConfirmationCallback dialogConfirmationCallback, final int buttonReference) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = ((Activity) context).getLayoutInflater().inflate(R.layout.dialog_confirm_delete, null);
        ((TextView) view.findViewById(R.id.confirmation_message)).setText(message);
        builder.setTitle(title);
        builder.setView(view);
        builder.setCancelable(true);
        builder.setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                dialogConfirmationCallback.onDialogConfirm(buttonReference);
            }
        });
        builder.setNegativeButton(context.getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        final AlertDialog alertDialog = builder.create();
        final TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.colorAccent, typedValue, true);
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(typedValue.data);
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(typedValue.data);
            }
        });
        alertDialog.show();
        return alertDialog;
    }

    /**
     * @param context  - Context to be used.
     * @param rootView - Root view
     * @param message  - Message to be shown.
     * @param duration - Duration to be shown.
     */
    public static void createAndShowSnackbar(final Context context, View rootView, String message, int duration) {
        Snackbar snackbar = Snackbar.make(rootView, message, duration);
        View view = snackbar.getView();
        TextView textView = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ActivityCompat.getColor(context, R.color.colorWhite));
        snackbar.show();
    }
}
