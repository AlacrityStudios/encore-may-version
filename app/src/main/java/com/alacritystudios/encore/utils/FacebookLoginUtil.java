package com.alacritystudios.encore.utils;

import com.facebook.login.LoginResult;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FacebookAuthProvider;

/**
 * Helper class to handle facebook related functionalities.
 */

public class FacebookLoginUtil {
    public static void handleFacebookLogin(LoginResult loginResult) {
        AuthCredential credential = FacebookAuthProvider.getCredential(loginResult.getAccessToken().getToken());
    }
}
