package com.alacritystudios.encore.utils;

import android.net.Uri;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.util.Log;

import com.alacritystudios.encore.constants.CatalogueConstants;
import com.alacritystudios.encore.enums.SortOrder;
import com.alacritystudios.encore.providers.MusicProvider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * Util for commonly used Queue operations.
 */

public class QueueUtil {
    private String TAG = getClass().getSimpleName();
    private MusicProvider mMusicProvider;
    private int mCurrentQueueIndex;
    private SortOrder mSortOrder;
    private Map<String, String> mHierarchyMap;

    public Map<String, String> getmHierarchyMap() {
        return mHierarchyMap;
    }

    public QueueUtil(MusicProvider mMusicProvider, SortOrder sortOrder) {
        this.mMusicProvider = mMusicProvider;
        this.mSortOrder = sortOrder;
    }

    public void setmSortOrder(SortOrder mSortOrder) {
        this.mSortOrder = mSortOrder;
    }

    public int getmCurrentQueueIndex() {
        return mCurrentQueueIndex;
    }

    public void setmCurrentQueueIndex(int mCurrentQueueIndex) {
        this.mCurrentQueueIndex = mCurrentQueueIndex;
    }

    public int skipCurrentQueueIndex(int index) {
        mCurrentQueueIndex += index;
        return mCurrentQueueIndex;
    }

    public int alterCurrentQueueIndex(int amount) {
        mCurrentQueueIndex += amount;
        return mCurrentQueueIndex;
    }

    public int alterCurrentQueueIndexRandomly(int queueSize) {
        int newIndex = 0;
        do {
            newIndex = (int) (Math.random() * queueSize);
        }
        while (newIndex < 0 || newIndex == mCurrentQueueIndex || newIndex > queueSize);
        mCurrentQueueIndex = newIndex;
        return mCurrentQueueIndex;
    }

    public List<MediaSessionCompat.QueueItem> getQueueBasedOnMediaId(String mediaId) {
        Log.i(TAG, "getQueueBasedOnMediaId. Media Id: " + mediaId);
        List<MediaMetadataCompat> tracks = null;
        mHierarchyMap = MediaIdUtil.getMediaHierarchyMap(mediaId);
        String[] mMediaHierarchyTypesArray = mHierarchyMap.keySet().toArray(new String[mHierarchyMap.size()]);
        String[] mMediaHierarchyValuesArray = mHierarchyMap.values().toArray(new String[mHierarchyMap.size()]);
        switch (MediaIdUtil.getHierarchyLevel(mediaId)) {
            case FIRST_TIER:
                Log.d(TAG, "The song is from the root directory 'SONGS'");
                tracks = new ArrayList<>(mMusicProvider.getmMusicListBySongs().values());
                break;
            case SECOND_TIER:
                switch (mMediaHierarchyTypesArray[0]) {
                    case MediaIdUtil.ROOT_MEDIA_ALBUMS:
                        Log.d(TAG, "The song is from the root directory 'ALBUMS'");
                        tracks = new ArrayList<>(mMusicProvider.getmMusicListByAlbum().get(Long.parseLong(mMediaHierarchyValuesArray[0])));
                        break;
                    case MediaIdUtil.ROOT_MEDIA_PLAYLISTS:
                        Log.d(TAG, "The song is from the root directory 'PLAYLISTS'");
                        tracks = new ArrayList<>(mMusicProvider.getmMusicListByPlaylist().get(Long.parseLong(mMediaHierarchyValuesArray[0])));
                        break;
                }
                break;
            case THIRD_TIER:
                switch (mMediaHierarchyTypesArray[0]) {
                    case MediaIdUtil.ROOT_MEDIA_ARTISTS:
                        Log.d(TAG, "The song is from the root directory 'ARTISTS'");
                        switch (mMediaHierarchyTypesArray[1]) {
                            case MediaIdUtil.BRANCH_MEDIA_ALBUM:
                                Log.d(TAG, "The song is from the directory 'ARTISTS > ALBUMS'");
                                tracks = new ArrayList<>(mMusicProvider.getmMusicListByAlbum().get(Long.parseLong(mMediaHierarchyValuesArray[1])));
                                break;
                        }
                        break;
                }
        }
        if (mSortOrder == SortOrder.ASCENDING) {
            Collections.sort(tracks, new Comparator<MediaMetadataCompat>() {
                @Override
                public int compare(MediaMetadataCompat o1, MediaMetadataCompat o2) {
                    return o1.getString(MediaMetadataCompat.METADATA_KEY_TITLE).compareTo(o2.getString(MediaMetadataCompat.METADATA_KEY_TITLE));
                }
            });
        } else {
            Collections.sort(tracks, new Comparator<MediaMetadataCompat>() {
                @Override
                public int compare(MediaMetadataCompat o1, MediaMetadataCompat o2) {
                    return o2.getString(MediaMetadataCompat.METADATA_KEY_TITLE).compareTo(o1.getString(MediaMetadataCompat.METADATA_KEY_TITLE));
                }
            });
        }

        return convertToQueue(tracks, Long.parseLong(mMediaHierarchyValuesArray[mMediaHierarchyValuesArray.length - 1]));
    }

    private List<MediaSessionCompat.QueueItem> convertToQueue(Iterable<MediaMetadataCompat> tracks, long musicId) {
        List<MediaSessionCompat.QueueItem> queue = new ArrayList<>();
        int count = 0;
        String hierarchyAwareMediaID;

        for (MediaMetadataCompat track : tracks) {
            if (musicId == Long.parseLong(track.getString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID))) {
                mCurrentQueueIndex = count;
            }
//            switch (MediaIdUtil.getHierarchyLevel(mediaId)) {
//                case FIRST_TIER:
//                    hierarchyAwareMediaID = MediaIdUtil.ROOT_MEDIA_SONGS + MediaIdUtil.VALUE_SEPARATOR + Long.parseLong(track.getDescription().getMediaId());
//                    break;
//                case SECOND_TIER:
//                    switch (mMediaHierarchyTypesArray[0]) {
//                        case MediaIdUtil.ROOT_MEDIA_ALBUMS:
//                            hierarchyAwareMediaID = MediaIdUtil.ROOT_MEDIA_ALBUMS + MediaIdUtil.VALUE_SEPARATOR + mMediaHierarchyValuesArray[0]
//                                    + MediaIdUtil.TYPE_SEPARATOR + MediaIdUtil.BRANCH_MEDIA_SONG + MediaIdUtil.VALUE_SEPARATOR + Long.parseLong(track.getDescription().getMediaId());
//                            break;
//                        case MediaIdUtil.ROOT_MEDIA_PLAYLISTS:
//                            hierarchyAwareMediaID = MediaIdUtil.ROOT_MEDIA_PLAYLISTS + MediaIdUtil.VALUE_SEPARATOR + mMediaHierarchyValuesArray[0]
//                                    + MediaIdUtil.TYPE_SEPARATOR + MediaIdUtil.BRANCH_MEDIA_SONG + MediaIdUtil.VALUE_SEPARATOR + Long.parseLong(track.getDescription().getMediaId());
//                            break;
//                        default:
//                            hierarchyAwareMediaID = "Some error came up.";
//                    }
//                    break;
//                case THIRD_TIER:
//                    switch (mMediaHierarchyTypesArray[0]) {
//                        case MediaIdUtil.ROOT_MEDIA_ARTISTS:
//                            switch (mMediaHierarchyTypesArray[1]) {
//                                case MediaIdUtil.BRANCH_MEDIA_ALBUM:
//                                    hierarchyAwareMediaID = MediaIdUtil.ROOT_MEDIA_ARTISTS + MediaIdUtil.VALUE_SEPARATOR + mMediaHierarchyValuesArray[0]
//                                            + MediaIdUtil.TYPE_SEPARATOR + MediaIdUtil.BRANCH_MEDIA_ALBUM + MediaIdUtil.VALUE_SEPARATOR + mMediaHierarchyValuesArray[1]
//                                            + MediaIdUtil.TYPE_SEPARATOR + MediaIdUtil.BRANCH_MEDIA_SONG + MediaIdUtil.VALUE_SEPARATOR + Long.parseLong(track.getDescription().getMediaId());
//                                    break;
//                                default:
//                                    hierarchyAwareMediaID = "Some error came up.";
//                            }
//                            break;
//                        default:
//                            hierarchyAwareMediaID = "Some error came up.";
//                    }
//                    break;
//                default:
//                    hierarchyAwareMediaID = "Some error came up.";
//            }
//            Log.d(TAG, "hierarchy aware mediaID: " + hierarchyAwareMediaID);

            MediaDescriptionCompat.Builder builder = new MediaDescriptionCompat.Builder();
            builder.setMediaId(track.getDescription().getMediaId());
            builder.setIconUri(ProviderUtil.getAlbumArtworkUri(track.getLong(CatalogueConstants.GeneralConstants.CUSTOM_METADATA_ALBUM_ID)));
            builder.setTitle(track.getString(MediaMetadataCompat.METADATA_KEY_TITLE));
            builder.setSubtitle(track.getString(MediaMetadataCompat.METADATA_KEY_ARTIST) + " - " + track.getString(MediaMetadataCompat.METADATA_KEY_ALBUM));
            builder.setMediaUri(Uri.parse(Uri.encode(track.getString(CatalogueConstants.GeneralConstants.CUSTOM_METADATA_TRACK_SOURCE))));
            MediaSessionCompat.QueueItem item = new MediaSessionCompat.QueueItem(builder.build(), count);
            queue.add(item);
            count++;
        }
        return queue;
    }

    public int updateCurrentQueueIndexBasedOnMediaId(String mediaId, List<MediaSessionCompat.QueueItem> queueItemList) {
        int count = 0;
        for (MediaSessionCompat.QueueItem queueItem : queueItemList) {
            if (MediaIdUtil.extractMusicIdFromMediaId(mediaId) == Long.parseLong(queueItem.getDescription().getMediaId())) {
                mCurrentQueueIndex = count;
                break;
            }
            count += 1;
        }
        return mCurrentQueueIndex;
    }

}
