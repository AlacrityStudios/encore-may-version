package com.alacritystudios.encore.utils;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.media.MediaBrowserCompat;
import android.util.Log;

import com.alacritystudios.encore.constants.CatalogueConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * Util class for commonly used MediaStore operations.
 */

public class ProviderUtil {

    public static final String TAG = "ProviderUtil";

    /**
     * Fetches the album artwork Uri
     *
     * @param albumId - Album Id for which the art is required.
     * @return
     */
    public static Uri getAlbumArtworkUri(long albumId) {
        return ContentUris.withAppendedId(Uri.parse(CatalogueConstants.UriConstants.mAlbumArtBaseUri), albumId);
    }

    /**
     * Returns cursor for the media database
     *
     * @param context - The context to be used
     * @return - All songs cursor
     */
    public static Cursor getAllSongsCursor(Context context) {
        Log.i(TAG, "getAllSongsCursor");
        return context.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                new String[]{
                        MediaStore.Audio.Media.TITLE,
                        MediaStore.Audio.Media.ARTIST,
                        MediaStore.Audio.Media.ALBUM,
                        MediaStore.Audio.Media.DURATION,
                        MediaStore.Audio.Media.DATA,
                        MediaStore.Audio.Media._ID,
                        MediaStore.Audio.Media.ALBUM_ID,
                        MediaStore.Audio.Media.ARTIST_ID
                }, MediaStore.Audio.Media.IS_MUSIC + "=1", null, null);
    }

    /**
     * Returns cursor for the playlist database
     *
     * @param context - The context to be used
     * @return - playlistId cursor
     */
    public static Cursor getPlaylistCursor(Context context) {
        Log.i(TAG, "getPlaylistCursor");
        return context.getContentResolver().query(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI,
                new String[]{
                        MediaStore.Audio.Playlists._ID,
                        MediaStore.Audio.Playlists.NAME,
                }, null, null, null);
    }

    /**
     * Returns cursor for the playlist media database
     *
     * @param context    - The context to be used
     * @param playlistId - The playlistId for which memebers are to be populated
     * @return - playlist media cursor
     */
    public static Cursor getPlaylistMediaCursor(Context context, long playlistId) {
        Log.i(TAG, "getPlaylistMediaCursor");
        return context.getContentResolver().query(MediaStore.Audio.Playlists.Members.getContentUri("external", playlistId),
                new String[]{
                        MediaStore.Audio.Playlists.Members.TITLE,
                        MediaStore.Audio.Playlists.Members.ARTIST,
                        MediaStore.Audio.Playlists.Members.ALBUM,
                        MediaStore.Audio.Playlists.Members.DURATION,
                        MediaStore.Audio.Playlists.Members.DATA,
                        MediaStore.Audio.Playlists.Members.AUDIO_ID,
                        MediaStore.Audio.Playlists.Members.ALBUM_ID,
                        MediaStore.Audio.Playlists.Members.ARTIST_ID
                }, MediaStore.Audio.Playlists.Members.IS_MUSIC + "=1", null, null);
    }

    /**
     * Get sublist based on search
     *
     * @param mMediaItemList - Input list
     * @param searchValue    -Value to search against
     * @return - Result list
     */
    public static List<MediaBrowserCompat.MediaItem> searchMediaItemByName(List<MediaBrowserCompat.MediaItem> mMediaItemList, String searchValue) {
        Log.i(TAG, "searchMediaItemByName()");
        List<MediaBrowserCompat.MediaItem> mResultList = new ArrayList<>();
        for (MediaBrowserCompat.MediaItem mediaItem : mMediaItemList) {
            if (mediaItem.getDescription().getTitle().toString().toLowerCase().contains(searchValue.toLowerCase())) {
                mResultList.add(mediaItem);
            }
        }
        return mResultList;
    }
}
