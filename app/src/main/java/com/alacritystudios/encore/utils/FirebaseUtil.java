package com.alacritystudios.encore.utils;

import android.util.Log;

import com.alacritystudios.encore.R;
import com.alacritystudios.encore.application.EncoreApplication;
import com.alacritystudios.encore.models.FirebaseUserGeneralDetails;
import com.alacritystudios.encore.models.FirebaseUserLibraryDetails;
import com.facebook.AccessToken;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Util class for commonly used database operations.
 */

public class FirebaseUtil {

    public static final String TAG = "FirebaseUtil";
    // User general details
    public static final String USER_GENERAL_DETAILS = "userGeneralDetails";
    public static final String USER_GENERAL_DETAILS_NAME = "userName";
    public static final String USER_GENERAL_DETAILS_EMAIL = "userEmail";
    public static final String USER_GENERAL_DETAILS_PHOTO_URL = "userPhotoUrl";
    public static final String USER_GENERAL_DETAILS_NUMBER_OF_FRIENDS = "numberOfFriends";


    //User library details
    public static final String USER_LIBRARY_DETAILS = "userLibraryDetails";
    public static final String USER_LIBRARY_DETAILS_NUMBER_OF_SONGS = "numberOfSongs";
    public static final String USER_LIBRARY_DETAILS_KARMA = "karmaScore";
    public static final String USER_LIBRARY_DETAILS_SONGS_LISTENED = "numberOfSongsListened";
    public static final String USER_LIBRARY_DETAILS_CURRENT_SONG = "currentSong";
    public static final String USER_LIBRARY_DETAILS_ONLINE_STATUS = "onlineStatus";
    public static final String USER_LIBRARY_DETAILS_IS_LISTENING = "isListening";


    //User account details
    public static final String USER_ACCOUNT_DETAILS = "userAccountDetails";
    public static final String USER_ACCOUNT_DETAILS_IS_PAID_ACCOUNT = "isPaidAccount";
    public static final String USER_ACCOUNT_DETAILS_NUMBER_OF_LIKES_AVAILABLE = "numberOfAvailableLikes";


    //User token details
    public static final String USER_TOKEN_DETAILS = "userTokenDetails";

    public static void loginNewUser() {
        Log.i(TAG, "loginNewUser");
        final AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null) {
            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            final FirebaseUserGeneralDetails firebaseUserGeneralDetails = new FirebaseUserGeneralDetails();
            try {
                if (firebaseUser.getDisplayName() != null) {
                    firebaseUserGeneralDetails.setUserName(firebaseUser.getDisplayName());
                } else {
                    firebaseUserGeneralDetails.setUserName(EncoreApplication.getmContext().getString(R.string.unknown_user_name));
                }
            } catch (NullPointerException ex) {
                firebaseUserGeneralDetails.setUserName(EncoreApplication.getmContext().getString(R.string.unknown_user_name));
            }
            try {
                if (firebaseUser.getEmail() != null) {
                    firebaseUserGeneralDetails.setUserEmail(firebaseUser.getEmail());
                } else {
                    firebaseUserGeneralDetails.setUserEmail(EncoreApplication.getmContext().getString(R.string.unknown_email));
                }

            } catch (NullPointerException ex) {
                firebaseUserGeneralDetails.setUserEmail(EncoreApplication.getmContext().getString(R.string.unknown_email));
            }
            try {
                if (firebaseUser.getPhotoUrl() != null) {
                    firebaseUserGeneralDetails.setUserPhotoUrl(firebaseUser.getPhotoUrl().toString());
                } else {
                    firebaseUserGeneralDetails.setUserPhotoUrl("");
                }
            } catch (NullPointerException ex) {
                firebaseUserGeneralDetails.setUserPhotoUrl("");
            }
            firebaseUserGeneralDetails.setNoOfFriends(0l);

            firebaseDatabase.getReference(accessToken.getUserId()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    dataSnapshot.child(USER_GENERAL_DETAILS).getRef().setValue(firebaseUserGeneralDetails);
                    dataSnapshot.child(USER_ACCOUNT_DETAILS).child(USER_ACCOUNT_DETAILS_IS_PAID_ACCOUNT).getRef().setValue(false);
                    dataSnapshot.child(USER_ACCOUNT_DETAILS).child(USER_ACCOUNT_DETAILS_NUMBER_OF_LIKES_AVAILABLE).getRef().setValue(1);
                    dataSnapshot.child(USER_ACCOUNT_DETAILS).child(USER_TOKEN_DETAILS).getRef().setValue(accessToken.getToken());
                    if (dataSnapshot.hasChildren()) {
                        dataSnapshot.child(USER_LIBRARY_DETAILS).child(USER_LIBRARY_DETAILS_CURRENT_SONG).getRef().setValue(EncoreApplication.getmContext().getString(R.string.currently_not_listening_to_anything));
                        dataSnapshot.child(USER_LIBRARY_DETAILS).child(USER_LIBRARY_DETAILS_ONLINE_STATUS).getRef().setValue(true);
                        dataSnapshot.child(USER_LIBRARY_DETAILS).child(USER_LIBRARY_DETAILS_IS_LISTENING).getRef().setValue(false);
                    } else {
                        FirebaseUserLibraryDetails firebaseUserLibraryDetails =
                                new FirebaseUserLibraryDetails(EncoreApplication.getmContext().getString(R.string.currently_not_listening_to_anything), 0, 0, 0, true, false);
                        dataSnapshot.child(USER_LIBRARY_DETAILS).getRef().setValue(firebaseUserLibraryDetails);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }
}