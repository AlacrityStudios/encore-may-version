package com.alacritystudios.encore.utils;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.alacritystudios.encore.R;
import com.alacritystudios.encore.activities.LandingActivity;
import com.alacritystudios.encore.application.EncoreApplication;
import com.alacritystudios.encore.models.PlaylistModel;
import com.facebook.AccessToken;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Util class for commonly used application level utilities.
 **/

public class AppStartUtil {

    public static void setDefaultColorScheme(Context mContext) {
        int selectedColor = PreferenceUtil.getCurrentColorScheme(mContext);
        if (selectedColor == 0) {
            PreferenceUtil.setCurrentColorScheme(mContext, ActivityCompat.getColor(mContext, R.color.pink_color_primary));
        }
    }

    public static int getCurrentColorSchemeTheme(Context mContext, boolean shouldShowToolbar) {
        int selectedColor = PreferenceUtil.getCurrentColorScheme(mContext);
        if (selectedColor != 0) {
            if (selectedColor == ActivityCompat.getColor(mContext, R.color.pink_color_primary)) {
                if (shouldShowToolbar) {
                    return R.style.AppThemePinkActionBar;
                } else {
                    return R.style.AppThemePink;
                }
            } else if (selectedColor == ActivityCompat.getColor(mContext, R.color.red_color_primary)) {
                if (shouldShowToolbar) {
                    return R.style.AppThemeRedActionBar;
                } else {
                    return R.style.AppThemeRed;
                }
            } else if (selectedColor == ActivityCompat.getColor(mContext, R.color.purple_color_primary)) {
                if (shouldShowToolbar) {
                    return R.style.AppThemePurpleActionBar;
                } else {
                    return R.style.AppThemePurple;
                }
            } else if (selectedColor == ActivityCompat.getColor(mContext, R.color.deep_purple_color_primary)) {
                if (shouldShowToolbar) {
                    return R.style.AppThemeDeepPurpleActionBar;
                } else {
                    return R.style.AppThemeDeepPurple;
                }
            } else if (selectedColor == ActivityCompat.getColor(mContext, R.color.indigo_color_primary)) {
                if (shouldShowToolbar) {
                    return R.style.AppThemeIndigoActionBar;
                } else {
                    return R.style.AppThemeIndigo;
                }
            } else if (selectedColor == ActivityCompat.getColor(mContext, R.color.blue_color_primary)) {
                if (shouldShowToolbar) {
                    return R.style.AppThemeBlueActionBar;
                } else {
                    return R.style.AppThemeBlue;
                }
            } else if (selectedColor == ActivityCompat.getColor(mContext, R.color.teal_color_primary)) {
                if (shouldShowToolbar) {
                    return R.style.AppThemeTealActionBar;
                } else {
                    return R.style.AppThemeTeal;
                }
            } else if (selectedColor == ActivityCompat.getColor(mContext, R.color.light_green_color_primary)) {
                if (shouldShowToolbar) {
                    return R.style.AppThemeLightGreenActionBar;
                } else {
                    return R.style.AppThemeLightGreen;
                }
            } else if (selectedColor == ActivityCompat.getColor(mContext, R.color.deep_orange_color_primary)) {
                if (shouldShowToolbar) {
                    return R.style.AppThemeDeepOrangeActionBar;
                } else {
                    return R.style.AppThemeDeepOrange;
                }
            } else if (selectedColor == ActivityCompat.getColor(mContext, R.color.brown_color_primary)) {
                if (shouldShowToolbar) {
                    return R.style.AppThemeBrownActionBar;
                } else {
                    return R.style.AppThemeBrown;
                }
            } else if (selectedColor == ActivityCompat.getColor(mContext, R.color.black_color_primary)) {
                if (shouldShowToolbar) {
                    return R.style.AppThemeBlackActionBar;
                } else {
                    return R.style.AppThemeBlack;
                }
            } else if (selectedColor == ActivityCompat.getColor(mContext, R.color.light_blue_color_primary)) {
                if (shouldShowToolbar) {
                    return R.style.AppThemeLightBlueActionBar;
                } else {
                    return R.style.AppThemeLightBlue;
                }
            } else if (selectedColor == ActivityCompat.getColor(mContext, R.color.orange_color_primary)) {
                if (shouldShowToolbar) {
                    return R.style.AppThemeOrangeActionBar;
                } else {
                    return R.style.AppThemeOrange;
                }
            } else {
                if (shouldShowToolbar) {
                    return R.style.AppThemeDefaultActionBar;
                } else {
                    return R.style.AppThemeDefault;
                }
            }
        } else {
            if (shouldShowToolbar) {
                return R.style.AppThemeDefaultActionBar;
            } else {
                return R.style.AppThemeDefault;
            }
        }
    }

    public static int getCurrentAccentColor(Context mContext) {
        int selectedColor = PreferenceUtil.getCurrentColorScheme(mContext);
        if (selectedColor != 0) {
            if (selectedColor == ActivityCompat.getColor(mContext, R.color.pink_color_primary)) {
                return ActivityCompat.getColor(mContext, R.color.pink_color_accent);
            } else if (selectedColor == ActivityCompat.getColor(mContext, R.color.red_color_primary)) {
                return ActivityCompat.getColor(mContext, R.color.red_color_accent);
            } else if (selectedColor == ActivityCompat.getColor(mContext, R.color.purple_color_primary)) {
                return ActivityCompat.getColor(mContext, R.color.purple_color_accent);
            } else if (selectedColor == ActivityCompat.getColor(mContext, R.color.deep_purple_color_primary)) {
                return ActivityCompat.getColor(mContext, R.color.deep_purple_color_accent);
            } else if (selectedColor == ActivityCompat.getColor(mContext, R.color.indigo_color_primary)) {
                return ActivityCompat.getColor(mContext, R.color.indigo_color_accent);
            } else if (selectedColor == ActivityCompat.getColor(mContext, R.color.blue_color_primary)) {
                return ActivityCompat.getColor(mContext, R.color.blue_color_accent);
            } else if (selectedColor == ActivityCompat.getColor(mContext, R.color.teal_color_primary)) {
                return ActivityCompat.getColor(mContext, R.color.teal_color_accent);
            } else if (selectedColor == ActivityCompat.getColor(mContext, R.color.light_green_color_primary)) {
                return ActivityCompat.getColor(mContext, R.color.light_green_color_accent);
            } else if (selectedColor == ActivityCompat.getColor(mContext, R.color.deep_orange_color_primary)) {
                return ActivityCompat.getColor(mContext, R.color.deep_orange_color_accent);
            } else if (selectedColor == ActivityCompat.getColor(mContext, R.color.brown_color_primary)) {
                return ActivityCompat.getColor(mContext, R.color.brown_color_accent);
            } else if (selectedColor == ActivityCompat.getColor(mContext, R.color.black_color_primary)) {
                return ActivityCompat.getColor(mContext, R.color.black_color_accent);
            } else if (selectedColor == ActivityCompat.getColor(mContext, R.color.light_blue_color_primary)) {
                return ActivityCompat.getColor(mContext, R.color.light_blue_color_accent);
            } else if (selectedColor == ActivityCompat.getColor(mContext, R.color.orange_color_primary)) {
                return ActivityCompat.getColor(mContext, R.color.orange_color_accent);
            } else {
                return ActivityCompat.getColor(mContext, R.color.pink_color_accent);
            }
        } else {
            return ActivityCompat.getColor(mContext, R.color.pink_color_accent);
        }
    }

    public static void sendReloadLibraryLocalBroadcast() {
        Intent intent = new Intent(EncoreApplication.getmContext(), LandingActivity.class);
        intent.setAction("com.alacritystudios.encore.RELOAD_MUSIC_LIBRARY");
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(EncoreApplication.getmContext());
        localBroadcastManager.sendBroadcast(intent);
    }

    public static void sendReloadFriendsDataLocalBroadcast() {
        Intent intent = new Intent(EncoreApplication.getmContext(), LandingActivity.class);
        intent.setAction("com.alacritystudios.encore.RELOAD_FRIENDS_DATA");
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(EncoreApplication.getmContext());
        localBroadcastManager.sendBroadcast(intent);
    }

    public static void sendReloadMusicListLocalBroadcast() {
        Intent intent = new Intent(EncoreApplication.getmContext(), LandingActivity.class);
        intent.setAction("com.alacritystudios.encore.RELOAD_MUSIC_LIST");
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(EncoreApplication.getmContext());
        localBroadcastManager.sendBroadcast(intent);
    }

//    public static void createCustomEqualizerPreset(Context mContext) {
//        if (EncoreApplication.getmEqualizer() != null) {
//            if (PreferenceUtil.getCustomPreset(mContext) == -1) {
//                EqualizerPreset equalizerPreset = SQLite.select()
//                        .from(EqualizerPreset.class)
//                        .where(EqualizerPreset_Table.presetName.eq(EqualizerUtils.CUSTOM))
//                        .querySingle();
//                if (equalizerPreset != null) {
//                    PreferenceUtil.setCustomPreset(mContext, equalizerPreset.getId());
//                } else {
//                    equalizerPreset = new EqualizerPreset();
//                    equalizerPreset.setCustomPreset(true);
//                    equalizerPreset.setPresetName(EqualizerUtils.CUSTOM);
//                    equalizerPreset.setNumberOfBands(EqualizerUtils.getNumberOfEqualizerBands());
//                    if (equalizerPreset.save()) {
//                        equalizerPreset = SQLite.select()
//                                .from(EqualizerPreset.class)
//                                .where(EqualizerPreset_Table.presetName.eq(EqualizerUtils.CUSTOM))
//                                .querySingle();
//                        if (equalizerPreset != null) {
//                            PreferenceUtil.setCustomPreset(mContext, equalizerPreset.getId());
//                            for (int i = 0; i < EqualizerUtils.getNumberOfEqualizerBands(); i++) {
//                                EqualizerBands equalizerBands = new EqualizerBands();
//                                equalizerBands.setRefPresetId(equalizerPreset.getId());
//                                equalizerBands.setBandFrequency(EqualizerUtils.getEqualizerBandFrequency(EqualizerUtils.getNumberOfEqualizerBands()).get(i));
//                                equalizerBands.setBandValue(0);
//                                equalizerBands.setOrder(i);
//                                equalizerBands.save();
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }
//
    public static void createFavoritePlaylist(Context mContext) {
        PlaylistModel playlistDetails = new PlaylistModel();
        playlistDetails.setPlaylistName(mContext.getString(R.string.favorites));
        long playlistId = PlaylistUtils.addNewPlaylist(mContext.getContentResolver(), playlistDetails, null);
        PreferenceUtil.setFavoritePlaylistId(mContext, playlistId);
    }

    public static void showUserAsOnline(final Context mContext) {
        if (PreferenceUtil.getOnlineStatusPreference(mContext)) {
            if (NetworkUtil.isNetworkAvailable(mContext) && FirebaseAuth.getInstance().getCurrentUser() != null) {
                final DatabaseReference libraryReference = FirebaseDatabase.getInstance().getReference(AccessToken.getCurrentAccessToken().getUserId()).child(FirebaseUtil.USER_LIBRARY_DETAILS);
                libraryReference.child(FirebaseUtil.USER_LIBRARY_DETAILS_ONLINE_STATUS).setValue(true);
                libraryReference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        libraryReference.child(FirebaseUtil.USER_LIBRARY_DETAILS_ONLINE_STATUS).onDisconnect().setValue(false);
                        libraryReference.child(FirebaseUtil.USER_LIBRARY_DETAILS_IS_LISTENING).onDisconnect().setValue(false);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }
    }
}
