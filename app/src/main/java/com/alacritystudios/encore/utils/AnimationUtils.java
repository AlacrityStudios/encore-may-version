package com.alacritystudios.encore.utils;

import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;

/**
 * Provides common animations.
 */

public class AnimationUtils {

    /**
     * Returns a scale animation object
     * @param duration - Duration of the animation.
     * @return - Scale animation object.
     */
    public static ScaleAnimation setScaleAnimation(int duration) {
        ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setDuration(duration);
        return anim;
    }
}
