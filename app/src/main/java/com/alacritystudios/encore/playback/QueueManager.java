package com.alacritystudios.encore.playback;

import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;

import com.alacritystudios.encore.enums.SortOrder;
import com.alacritystudios.encore.interfaces.QueueManagerCallback;
import com.alacritystudios.encore.providers.MusicProvider;
import com.alacritystudios.encore.utils.MediaIdUtil;
import com.alacritystudios.encore.utils.QueueUtil;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Class to manage the queue.
 */

public class QueueManager {
    private String TAG = getClass().getSimpleName();
    private List<MediaSessionCompat.QueueItem> mQueueItems;
    private MusicProvider mMusicProvider;
    private QueueUtil mQueueUtil;
    private int mRepeatMode;
    private boolean isShuffleEnabled;
    private MediaSessionCompat.QueueItem mCurrentQueueItem;
    private QueueManagerCallback mQueueManagerCallback;
    private SortOrder mSortOrder;
    private int mCurrentIndex;
    private Map<String, String> mHierarchyMap;

    public void setmSortOrder(SortOrder mSortOrder) {
        this.mSortOrder = mSortOrder;
        mQueueUtil.setmSortOrder(mSortOrder);
    }

    public void setmRepeatMode(int mRepeatMode) {
        this.mRepeatMode = mRepeatMode;
    }

    public void setShuffleEnabled(boolean shuffleEnabled) {
        isShuffleEnabled = shuffleEnabled;
    }

    public List<MediaSessionCompat.QueueItem> getmQueueItems() {
        return mQueueItems;
    }

    public QueueManager(MusicProvider mMusicProvider, QueueManagerCallback queueManagerCallback, SortOrder sortOrder) {
        this.mMusicProvider = mMusicProvider;
        this.mSortOrder = sortOrder;
        this.mQueueUtil = new QueueUtil(mMusicProvider, mSortOrder);
        this.mQueueManagerCallback = queueManagerCallback;
        this.mRepeatMode = PlaybackStateCompat.REPEAT_MODE_NONE;
        this.isShuffleEnabled = false;
    }

    public List<MediaSessionCompat.QueueItem> createQueueFromMediaId(String mediaId) {
        Log.i(TAG, "createQueueFromMediaId. Media Id: " + mediaId);
        if (isSameBrowsingHierarchy(mediaId)) {
            mCurrentIndex = mQueueUtil.updateCurrentQueueIndexBasedOnMediaId(mediaId, mQueueItems);
            mCurrentQueueItem = mQueueItems.get(mCurrentIndex);
            mQueueManagerCallback.onQueueUpdated(mQueueItems);
            mHierarchyMap = mQueueUtil.getmHierarchyMap();
        } else {
            mQueueItems = mQueueUtil.getQueueBasedOnMediaId(mediaId);
            mCurrentIndex = mQueueUtil.getmCurrentQueueIndex();
            mCurrentQueueItem = mQueueItems.get(mCurrentIndex);
            mQueueManagerCallback.onQueueUpdated(mQueueItems);
            mHierarchyMap = mQueueUtil.getmHierarchyMap();
        }

        return mQueueItems;
    }

    public MediaSessionCompat.QueueItem getmCurrentQueueItem() {
        return mCurrentQueueItem;
    }

    public boolean skipQueuePosition(int amount) {
        Log.i(TAG, "skipQueuePosition. amount: " + amount);
        mCurrentQueueItem = mQueueItems.get(mQueueUtil.skipCurrentQueueIndex(amount));
        return true;
    }

    public boolean incrementToNextSong() {
        Log.i(TAG, "incrementToNextSong");
        int newIndex = 0;
        if (mRepeatMode == PlaybackStateCompat.REPEAT_MODE_ONE) {
            return true;
        } else {
            if (isShuffleEnabled) {
                mCurrentIndex = mQueueUtil.alterCurrentQueueIndexRandomly(mQueueItems.size());
            } else {
                newIndex = mQueueUtil.alterCurrentQueueIndex(1);
                if (mRepeatMode == PlaybackStateCompat.REPEAT_MODE_NONE) {
                    if (newIndex >= mQueueItems.size()) {
                        mQueueUtil.setmCurrentQueueIndex(mCurrentIndex);
                        return false;
                    }
                    mCurrentIndex = newIndex;
                } else {
                    if (newIndex >= mQueueItems.size()) {
                        newIndex = 0;
                        mQueueUtil.setmCurrentQueueIndex(newIndex);
                        mCurrentIndex = mQueueUtil.getmCurrentQueueIndex();
                    } else {
                        mCurrentIndex = newIndex;
                    }
                }
            }
            mCurrentQueueItem = mQueueItems.get(mCurrentIndex);
            return true;
        }
    }

    public boolean decrementToPreviousSong() {
        Log.i(TAG, "decrementToPreviousSong()");
        int newIndex = 0;
        if (mRepeatMode == PlaybackStateCompat.REPEAT_MODE_ONE) {
            return true;
        } else {
            if (isShuffleEnabled) {
                mCurrentIndex = mQueueUtil.alterCurrentQueueIndexRandomly(mQueueItems.size());
            } else {
                newIndex = mQueueUtil.alterCurrentQueueIndex(-1);
                if (mRepeatMode == PlaybackStateCompat.REPEAT_MODE_NONE) {
                    if (newIndex < 0) {
                        mQueueUtil.setmCurrentQueueIndex(mCurrentIndex);
                        return false;
                    }
                    mCurrentIndex = newIndex;
                } else {
                    if (newIndex < 0) {
                        newIndex = mQueueItems.size() - 1;
                        mQueueUtil.setmCurrentQueueIndex(newIndex);
                        mCurrentIndex = mQueueUtil.getmCurrentQueueIndex();
                    } else {
                        mCurrentIndex = newIndex;
                    }
                }
            }
            mCurrentQueueItem = mQueueItems.get(mCurrentIndex);
            return true;
        }
    }

    public boolean playItemAtPosition(int position)
    {
        Log.i(TAG, "playItemAtPosition()");
        if(position >= 0 && position < mQueueItems.size())
        {
            mCurrentIndex = position;
            mCurrentQueueItem = mQueueItems.get(position);
            mQueueUtil.setmCurrentQueueIndex(mCurrentIndex);
            return true;
        }
        return false;
    }

    public void removeQueueItem(MediaDescriptionCompat mediaDescriptionCompat) {
        Log.i(TAG, "removeQueueItem()");
        for (MediaSessionCompat.QueueItem queueItem : mQueueItems) {
            if (queueItem.getDescription().getMediaId().equals(mediaDescriptionCompat.getMediaId())) {
                mQueueItems.remove(queueItem);
                break;
            }
        }
    }

    public void moveItemsInQueue(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(mQueueItems, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(mQueueItems, i, i - 1);
            }
        }
        if (fromPosition < mCurrentIndex && toPosition >= mCurrentIndex) {
            mCurrentIndex--;
        }
        if (fromPosition > mCurrentIndex && toPosition <= mCurrentIndex) {
            mCurrentIndex++;
        }
        mQueueUtil.setmCurrentQueueIndex(mCurrentIndex);
    }

    public void updateMetadata() {
        if (mCurrentQueueItem == null) {
            //TODO : Implemnent this.
            //mListener.onMetadataRetrieveError();
            return;
        }
        final long musicId = MediaIdUtil.extractMusicIdFromMediaId(mCurrentQueueItem.getDescription().getMediaId());
        MediaMetadataCompat metadata = mMusicProvider.getMusic(musicId);

        if (metadata == null) {
            throw new IllegalArgumentException("Invalid musicId " + musicId);
        }
        mQueueManagerCallback.onMetaDataUpdate(metadata);
    }

    public boolean isSameBrowsingHierarchy(String mediaId) {
        if (mHierarchyMap == null) {
            return false;
        }
        Map<String, String> mNewHierarchy = MediaIdUtil.getMediaHierarchyMap(mediaId);
        if (mHierarchyMap.size() != mNewHierarchy.size()) {
            return false;
        }

        String[] mMediaHierarchyTypesArray = mHierarchyMap.keySet().toArray(new String[mHierarchyMap.size()]);
        String[] mNewMediaHierarchyTypesArray = mNewHierarchy.keySet().toArray(new String[mNewHierarchy.size()]);

        for (int i = 0; i < mHierarchyMap.size(); i++) {
            if (i == mHierarchyMap.size() - 1) {
                if (!mMediaHierarchyTypesArray[i].equals(mNewMediaHierarchyTypesArray[i])) {
                    return false;
                }
            } else {
                if (!(mHierarchyMap.get(mMediaHierarchyTypesArray[i]).equals(mNewHierarchy.get(mNewMediaHierarchyTypesArray[i])) && mMediaHierarchyTypesArray[i].equals(mNewMediaHierarchyTypesArray[i]))) {
                    return false;
                }
            }
        }
        return true;
    }
}
