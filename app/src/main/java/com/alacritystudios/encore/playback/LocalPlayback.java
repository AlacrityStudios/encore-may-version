package com.alacritystudios.encore.playback;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.net.Uri;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;

import com.alacritystudios.encore.application.EncoreApplication;
import com.alacritystudios.encore.interfaces.LocalPlaybackCallback;
import com.alacritystudios.encore.models.FirebaseUserLibraryDetails;
import com.alacritystudios.encore.services.MusicService;
import com.alacritystudios.encore.utils.FirebaseUtil;
import com.facebook.AccessToken;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Class to define media player
 */

public class LocalPlayback implements SimpleExoPlayer.EventListener, AudioManager.OnAudioFocusChangeListener {

    private String TAG = getClass().getSimpleName();
    private SimpleExoPlayer mSimpleExoPlayer;
    private Context mContext;
    private PlaybackParameters mPlaybackParameters;
    private LocalPlaybackCallback mLocalPlaybackCallback;
    private final AudioManager mAudioManager;
    private boolean mPlayOnFocusGain;
    private volatile boolean mAudioNoisyReceiverRegistered;
    private int mAudioFocus = AUDIO_NO_FOCUS_NO_DUCK;

    // The volume we set the media player to when we lose audio focus, but are
    // allowed to reduce the volume instead of stopping playback.
    public static final float VOLUME_DUCK = 0.2f;
    // The volume we set the media player when we have audio focus.
    public static final float VOLUME_NORMAL = 1.0f;
    // we don't have audio focus, and can't duck (play at a low volume)
    private static final int AUDIO_NO_FOCUS_NO_DUCK = 0;
    // we don't have focus, but can duck (play at a low volume)
    private static final int AUDIO_NO_FOCUS_CAN_DUCK = 1;
    // we have full audio focus
    private static final int AUDIO_FOCUSED  = 2;

    private final IntentFilter mAudioNoisyIntentFilter =
            new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY);

    private final BroadcastReceiver mAudioNoisyReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent.getAction())) {
                Log.d(TAG, "Headphones disconnected.");
                if (isPlaying()) {
                    Intent i = new Intent(context, MusicService.class);
                    i.setAction(MusicService.ACTION_CMD);
                    i.putExtra(MusicService.CMD_NAME, MusicService.CMD_PAUSE);
                    mContext.startService(i);
                }
            }
        }
    };

    /**
     * Try to get the system audio focus.
     */
    private void tryToGetAudioFocus() {
        Log.d(TAG, "tryToGetAudioFocus");
        int result = mAudioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC,
                AudioManager.AUDIOFOCUS_GAIN);
        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            mAudioFocus = AUDIO_FOCUSED;
        } else {
            mAudioFocus = AUDIO_NO_FOCUS_NO_DUCK;
        }
    }

    /**
     * Give up the audio focus.
     */
    private void giveUpAudioFocus() {
        Log.d(TAG, "giveUpAudioFocus");
        if (mAudioManager.abandonAudioFocus(this) == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            mAudioFocus = AUDIO_NO_FOCUS_NO_DUCK;
        }
    }

    private void registerAudioNoisyReceiver() {
        if (!mAudioNoisyReceiverRegistered) {
            mContext.registerReceiver(mAudioNoisyReceiver, mAudioNoisyIntentFilter);
            mAudioNoisyReceiverRegistered = true;
        }
    }

    private void unregisterAudioNoisyReceiver() {
        if (mAudioNoisyReceiverRegistered) {
            mContext.unregisterReceiver(mAudioNoisyReceiver);
            mAudioNoisyReceiverRegistered = false;
        }
    }

    public LocalPlayback(Context mContext, LocalPlaybackCallback mLocalPlaybackCallback) {
        this.mContext = mContext;
        this.mLocalPlaybackCallback = mLocalPlaybackCallback;
        this.mPlaybackParameters = new PlaybackParameters(1.0f, 1.0f);
        this.mAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        createMediaPlayer();
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
            // We have gained focus:
            mAudioFocus = AUDIO_FOCUSED;

        } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS ||
                focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT ||
                focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
            // We have lost focus. If we can duck (low playback volume), we can keep playing.
            // Otherwise, we need to pause the playback.
            boolean canDuck = focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK;
            mAudioFocus = canDuck ? AUDIO_NO_FOCUS_CAN_DUCK : AUDIO_NO_FOCUS_NO_DUCK;

            // If we are playing, we need to reset media player by calling configMediaPlayerState
            // with mAudioFocus properly set.
            if (isPlaying() && !canDuck) {
                // If we don't have audio focus and can't duck, we save the information that
                // we were playing, so that we can resume playback once we get the focus back.
                mPlayOnFocusGain = true;
            }
        }
        configMediaPlayerState();
    }

    /**
     * Reconfigures MediaPlayer according to audio focus settings and
     * starts/restarts it. This method starts/restarts the MediaPlayer
     * respecting the current audio focus state. So if we have focus, it will
     * play normally; if we don't have focus, it will either leave the
     * MediaPlayer paused or set it to a low volume, depending on what is
     * allowed by the current focus settings. This method assumes mPlayer !=
     * null, so if you are calling it, you have to do so from a context where
     * you are sure this is the case.
     */
    private void configMediaPlayerState() {
        Log.d(TAG, "configMediaPlayerState. mAudioFocus=" + mAudioFocus);
        if (mAudioFocus == AUDIO_NO_FOCUS_NO_DUCK) {
            // If we don't have audio focus and can't duck, we have to pause,
            if (isPlaying()) {
                pauseMusicPlayer();
            }
        } else {  // we have audio focus:
            registerAudioNoisyReceiver();
            if (mAudioFocus == AUDIO_NO_FOCUS_CAN_DUCK) {
                mSimpleExoPlayer.setVolume(VOLUME_DUCK); // we'll be relatively quiet
            } else {
                if (mSimpleExoPlayer != null) {
                    mSimpleExoPlayer.setVolume(VOLUME_NORMAL); // we can be loud again
                } // else do something for remote client.
            }
            // If we were playing when we lost focus, we need to resume playing.
            if (mPlayOnFocusGain) {
                if (mSimpleExoPlayer != null && isPaused()) {
                    resumeMusicPlayer();
                }
                mPlayOnFocusGain = false;
            }
        }
    }

    public void createMediaPlayer() {
        Log.i(TAG, "createMediaPlayer");
        if (mSimpleExoPlayer == null) {
            TrackSelector trackSelector = new DefaultTrackSelector();
            mSimpleExoPlayer = ExoPlayerFactory.newSimpleInstance(EncoreApplication.getmContext(), trackSelector);
        }
    }

    public void prepareExoPlayer(final MediaSessionCompat.QueueItem queueItem) {
        Log.i(TAG, "prepareExoPlayer");
        Uri mediaUri = queueItem.getDescription().getMediaUri();
        if (mSimpleExoPlayer == null) {
            createMediaPlayer();
        }
        tryToGetAudioFocus();
        registerAudioNoisyReceiver();
        mSimpleExoPlayer.addListener(this);
        DataSource.Factory factory = new DefaultDataSourceFactory(mContext, Util.getUserAgent(mContext, "Encore"));
        MediaSource mediaSource = new ExtractorMediaSource(mediaUri, factory, new DefaultExtractorsFactory(), null, null);
        mSimpleExoPlayer.setPlaybackParameters(mPlaybackParameters);
        mSimpleExoPlayer.prepare(mediaSource);
        mSimpleExoPlayer.setPlayWhenReady(true);
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null) {
            FirebaseDatabase.getInstance().getReference(accessToken.getUserId()).child(FirebaseUtil.USER_LIBRARY_DETAILS).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    FirebaseUserLibraryDetails firebaseUserLibraryDetails = dataSnapshot.getValue(FirebaseUserLibraryDetails.class);
                    dataSnapshot.getRef().child(FirebaseUtil.USER_LIBRARY_DETAILS_CURRENT_SONG).setValue(queueItem.getDescription().getTitle());
                    dataSnapshot.getRef().child(FirebaseUtil.USER_LIBRARY_DETAILS_SONGS_LISTENED).setValue(firebaseUserLibraryDetails.getNumberOfSongsListened() + 1);
                    dataSnapshot.getRef().child(FirebaseUtil.USER_LIBRARY_DETAILS_IS_LISTENING).setValue(true);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    public long getPlayerCurrentPosition() {
        Log.i(TAG, "prepareExoPlayer");
        return mSimpleExoPlayer.getCurrentPosition();
    }

    public boolean isPlayerConnected() {
        Log.i(TAG, "prepareExoPlayer");
        if (mSimpleExoPlayer == null) {
            return false;
        } else {
            return true;
        }
    }

    public boolean isPlaying() {
        Log.i(TAG, "isPlaying");
        //TODO: Add focus stuff here.
        return (mSimpleExoPlayer != null && mSimpleExoPlayer.getPlayWhenReady() && mSimpleExoPlayer.getPlaybackState() == SimpleExoPlayer.STATE_READY);
    }

    public boolean isPaused() {
        Log.i(TAG, "isPaused");
        //TODO: Add focus stuff here.
        return (mSimpleExoPlayer != null && !mSimpleExoPlayer.getPlayWhenReady() && mSimpleExoPlayer.getPlaybackState() == SimpleExoPlayer.STATE_READY);
    }

    /**
     * Releases resources used by the service for playback. This includes the
     * "foreground service" status, the wake locks and possibly the MediaPlayer.
     */
    public void releaseMediaPlayer() {
        Log.d(TAG, "releaseMediaPlayer");
        mSimpleExoPlayer.release();
        mSimpleExoPlayer = null;
        unregisterAudioNoisyReceiver();
        giveUpAudioFocus();
    }

    public void pauseMusicPlayer() {
        Log.i(TAG, "pauseMusicPlayer");
        mSimpleExoPlayer.setPlayWhenReady(false);
        mLocalPlaybackCallback.onPlaybackStatusChanged(mSimpleExoPlayer.getPlaybackState(), mSimpleExoPlayer.getPlayWhenReady());
    }

    public void resumeMusicPlayer() {
        Log.i(TAG, "resumeMusicPlayer");
        mSimpleExoPlayer.setPlayWhenReady(true);
        mLocalPlaybackCallback.onPlaybackStatusChanged(mSimpleExoPlayer.getPlaybackState(), mSimpleExoPlayer.getPlayWhenReady());
    }

    public void seekTo(int position) {
        Log.i(TAG, "seekTo");
        if (mSimpleExoPlayer != null && mSimpleExoPlayer.getPlaybackState() == SimpleExoPlayer.STATE_READY) {
            mSimpleExoPlayer.seekTo(position);
        }
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest) {
        Log.i(TAG, "onTimelineChanged");
    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
        Log.i(TAG, "onTracksChanged");
    }

    @Override
    public void onLoadingChanged(boolean isLoading) {
        Log.i(TAG, "onLoadingChanged");
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        Log.i(TAG, "onPlayerStateChanged. playWhenReady: " + playWhenReady + " playbackState: " + playbackState);
        mLocalPlaybackCallback.onPlaybackStatusChanged(playbackState, playWhenReady);
    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        Log.i(TAG, "onPlayerError");
        mLocalPlaybackCallback.onPlaybackStatusChanged(mSimpleExoPlayer.getPlaybackState(), mSimpleExoPlayer.getPlayWhenReady());
    }

    @Override
    public void onPositionDiscontinuity() {
        Log.i(TAG, "onPositionDiscontinuity");
    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
        Log.i(TAG, "onPlaybackParametersChanged");
    }
}