package com.alacritystudios.encore.playback;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;

import com.alacritystudios.encore.enums.SortOrder;
import com.alacritystudios.encore.interfaces.LocalPlaybackCallback;
import com.alacritystudios.encore.interfaces.MediaSessionServiceCallback;
import com.alacritystudios.encore.interfaces.QueueManagerCallback;
import com.alacritystudios.encore.providers.MusicProvider;
import com.alacritystudios.encore.utils.PreferenceUtil;
import com.google.android.exoplayer2.ExoPlayer;

import java.util.List;

/**
 * Class to handle playback
 */

public class PlaybackManager implements LocalPlaybackCallback, QueueManagerCallback {

    private String TAG = getClass().getSimpleName();
    private Context mContext;
    private MediaSessionCallback mMediaSessionCallback;
    private List<MediaSessionCompat.QueueItem> mQueueItems;
    private MusicProvider mMusicProvider;
    private QueueManager mQueueManager;
    private LocalPlayback mLocalPlayback;
    private MediaSessionServiceCallback mMediaSessionServiceCallback;
    private SortOrder mSortOrder;
    private final String ACTION_INVERT_SORT_ORDER = "ACTION_INVERT_SORT_ORDER";
    private final String ACTION_RE_ORDER_QUEUE = "ACTION_RE_ORDER_QUEUE";
    private final String ACTION_RE_ORDER_QUEUE_FROM_POSITION = "ACTION_RE_ORDER_QUEUE_FROM_POSITION";
    private final String ACTION_RE_ORDER_QUEUE_TO_POSITION = "ACTION_RE_ORDER_QUEUE_TO_POSITION";
    private final String ACTION_PLAY_QUEUE_ITEM = "ACTION_PLAY_QUEUE_ITEM";
    private final String ACTION_PLAY_QUEUE_ITEM_POSITION = "ACTION_PLAY_QUEUE_ITEM_POSITION";

    public MediaSessionCallback getmMediaSessionCallback() {
        return mMediaSessionCallback;
    }

    public PlaybackManager(Context context, MusicProvider musicProvider, MediaSessionServiceCallback mediaSessionServiceCallback) {
        this.mContext = context;
        this.mMediaSessionCallback = new MediaSessionCallback();
        this.mMusicProvider = musicProvider;
        if (PreferenceUtil.getCurrentSortOrderPreference(mContext) == 0) {
            this.mSortOrder = SortOrder.ASCENDING;
        } else {
            this.mSortOrder = SortOrder.DESCENDING;
        }
        this.mQueueManager = new QueueManager(mMusicProvider, this, mSortOrder);
        this.mLocalPlayback = new LocalPlayback(mContext, this);
        this.mMediaSessionServiceCallback = mediaSessionServiceCallback;
    }

    @Override
    public void onCompletion() {
        Log.i(TAG, "onCompletion");
        if (mQueueManager.incrementToNextSong()) {
            handlePlayRequest();
            mQueueManager.updateMetadata();
        } else {
            handleStopRequest();
        }
    }

    @Override
    public void onPlaybackStatusChanged(int exoPlayState, boolean playWhenReady) {
        Log.i(TAG, "onPlaybackStatusChanged");
        switch (exoPlayState) {
            case ExoPlayer.STATE_READY:
                if (playWhenReady) {
                    updatePlaybackState(PlaybackStateCompat.STATE_PLAYING, "");
                } else {
                    updatePlaybackState(PlaybackStateCompat.STATE_PAUSED, "");
                }
                break;
            case ExoPlayer.STATE_ENDED:
                if (playWhenReady) {
                    onCompletion();
                } else {
                    updatePlaybackState(PlaybackStateCompat.STATE_STOPPED, "");
                }
                break;
        }
    }

    @Override
    public void onMetaDataUpdate(MediaMetadataCompat mediaMetadataCompat) {
        Log.i(TAG, "onMetaDataUpdate");
        mMediaSessionServiceCallback.onMetadataUpdate(mediaMetadataCompat);
    }

    @Override
    public void onQueueUpdated(List<MediaSessionCompat.QueueItem> queueItems) {
        Log.i(TAG, "onQueueUpdated");
        mMediaSessionServiceCallback.onQueueUpdated(queueItems);
    }

    public void handlePlayRequest() {
        Log.i(TAG, "handlePlayRequest");
        mLocalPlayback.prepareExoPlayer(mQueueManager.getmCurrentQueueItem());
    }

    public void handleResumeRequest() {
        Log.i(TAG, "handleResumeRequest");
        if (mLocalPlayback.isPaused()) {
            mLocalPlayback.resumeMusicPlayer();
        }
    }

    public void handlePauseRequest() {
        Log.i(TAG, "handlePauseRequest");
        if (mLocalPlayback.isPlaying()) {
            mLocalPlayback.pauseMusicPlayer();
        }
    }

    public void handleStopRequest() {
        Log.i(TAG, "handleStopRequest");
        mLocalPlayback.releaseMediaPlayer();
        updatePlaybackState(PlaybackStateCompat.STATE_STOPPED, "");
    }

    private long getAvailableActions() {
        long actions =
                PlaybackStateCompat.ACTION_PLAY_PAUSE |
                        PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID |
                        PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS |
                        PlaybackStateCompat.ACTION_SKIP_TO_NEXT |
                        PlaybackStateCompat.ACTION_SET_SHUFFLE_MODE_ENABLED |
                        PlaybackStateCompat.ACTION_SET_REPEAT_MODE;
        if (mLocalPlayback.isPlaying()) {
            actions |= PlaybackStateCompat.ACTION_PAUSE;
        } else {
            actions |= PlaybackStateCompat.ACTION_PLAY;
        }
        return actions;
    }


    /**
     * Update the current media player state, optionally showing an error message.
     */
    public void updatePlaybackState(int playbackState, String error) {
        Log.d(TAG, "updatePlaybackState, playback state=" + playbackState);
        long position = PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN;
        if (mLocalPlayback != null && mLocalPlayback.isPlayerConnected()) {
            position = mLocalPlayback.getPlayerCurrentPosition();
        }

        PlaybackStateCompat.Builder stateBuilder = new PlaybackStateCompat.Builder()
                .setActions(getAvailableActions());

        //TODO: Error case
//        if (error != null) {
//            // Error states are really only supposed to be used for errors that cause playback to
//            // stop unexpectedly and persist until the user takes action to fix it.
//            stateBuilder.setErrorMessage(error);
//            state = PlaybackStateCompat.STATE_ERROR;
//        }
        //noinspection ResourceType
        stateBuilder.setState(playbackState, position, 1.0f, SystemClock.elapsedRealtime());

        // Set the activeQueueItemId if the current index is valid.
        MediaSessionCompat.QueueItem currentMusic = mQueueManager.getmCurrentQueueItem();
        if (currentMusic != null) {
            stateBuilder.setActiveQueueItemId(currentMusic.getQueueId());
        }

        mMediaSessionServiceCallback.onPlaybackStateChanged(stateBuilder.build());
        if (playbackState == PlaybackStateCompat.STATE_PLAYING || playbackState == PlaybackStateCompat.STATE_PAUSED) {
            mMediaSessionServiceCallback.onNotificationRequired();
        }
    }

    public class MediaSessionCallback extends MediaSessionCompat.Callback {
        private String TAG = getClass().getSimpleName();

        @Override
        public void onPlayFromMediaId(String mediaId, Bundle extras) {
            Log.i(TAG, "onPlayFromMediaId. Media ID: " + mediaId);
            mQueueItems = mQueueManager.createQueueFromMediaId(mediaId);
            handlePlayRequest();
            mQueueManager.updateMetadata();
        }

        @Override
        public void onPlay() {
            Log.i(TAG, "onPlay");
            handleResumeRequest();
        }

        @Override
        public void onPause() {
            Log.i(TAG, "onPause");
            handlePauseRequest();
        }

        @Override
        public void onSkipToNext() {
            Log.i(TAG, "onSkipToNext");
            if (mQueueManager.incrementToNextSong()) {
                handlePlayRequest();
                mQueueManager.updateMetadata();
            } else {
                handlePlayRequest();
            }
        }

        @Override
        public void onSkipToPrevious() {
            Log.i(TAG, "onSkipToPrevious");
            if (mQueueManager.decrementToPreviousSong()) {
                handlePlayRequest();
                mQueueManager.updateMetadata();
            } else {
                handlePlayRequest();
            }
        }

        @Override
        public void onSetRepeatMode(int repeatMode) {
            super.onSetRepeatMode(repeatMode);
            Log.i(TAG, "onSetRepeatMode. Repeat mode: " + repeatMode);
            mQueueManager.setmRepeatMode(repeatMode);
            mMediaSessionServiceCallback.getMediaSession().setRepeatMode(repeatMode);
        }

        @Override
        public void onSetShuffleModeEnabled(boolean enabled) {
            super.onSetShuffleModeEnabled(enabled);
            Log.i(TAG, "onSetShuffleModeEnabled. Enabled: " + enabled);
            mQueueManager.setShuffleEnabled(enabled);
            mMediaSessionServiceCallback.getMediaSession().setShuffleModeEnabled(enabled);
        }

        @Override
        public void onSeekTo(long pos) {
            Log.i(TAG, "onSeekTo. Position: " + pos);
            mLocalPlayback.seekTo((int) pos);
        }

        @Override
        public void onRemoveQueueItem(MediaDescriptionCompat description) {
            Log.i(TAG, "onRemoveQueueItem()");
            mQueueManager.removeQueueItem(description);
        }



        @Override
        public void onCustomAction(String action, Bundle extras) {
            if (action.equals(ACTION_INVERT_SORT_ORDER)) {
                if (PreferenceUtil.getCurrentSortOrderPreference(mContext) == 0) {
                    mSortOrder = SortOrder.ASCENDING;
                } else {
                    mSortOrder = SortOrder.DESCENDING;
                }
                mQueueManager.setmSortOrder(mSortOrder);
            }
            if (action.equals(ACTION_RE_ORDER_QUEUE)) {
                int fromPosition = extras.getInt(ACTION_RE_ORDER_QUEUE_FROM_POSITION, 0);
                int toPosition = extras.getInt(ACTION_RE_ORDER_QUEUE_TO_POSITION, 0);
                mQueueManager.moveItemsInQueue(fromPosition, toPosition);
                mQueueItems = mQueueManager.getmQueueItems();
            }
            if (action.equals(ACTION_PLAY_QUEUE_ITEM)) {
                int position = extras.getInt(ACTION_PLAY_QUEUE_ITEM_POSITION, 0);
                if(mQueueManager.playItemAtPosition(position))
                {
                    handlePlayRequest();
                    mQueueManager.updateMetadata();
                } else {
                    handleStopRequest();
                }
            }
        }
    }
}
