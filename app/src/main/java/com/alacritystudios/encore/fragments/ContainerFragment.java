package com.alacritystudios.encore.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.media.MediaBrowserCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alacritystudios.encore.R;
import com.alacritystudios.encore.fragments.base.BaseFragment;
import com.alacritystudios.encore.utils.MediaIdUtil;

import java.util.Map;

/**
 * Container fragment for media browser fragments.
 */

public class ContainerFragment extends BaseFragment implements BaseFragment.BaseFragmentImpl {

    private Fragment mBrowseFragment;
    private String mediaId;
    private FragmentTransaction mFragmentTransaction;

    @Override
    public void initialiseUI() {
        navigateToBrowseFragment(mediaId);
    }

    @Override
    public void setListeners() {

    }

    @Override
    public void setContextAndTags() {
        TAG = getClass().getSimpleName();
        mContext = getActivity();
    }

    @Override
    public void initialiseOtherComponents() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_container, container, false);
        mediaId = getArguments().getString(MediaIdUtil.MEDIA_ID);
        initialiseOtherComponents();
        initialiseUI();
        return mFragmentView;
    }

    /**
     * Returns a new instance of this fragment.
     *
     * @param mediaId - The media Id to be associated with the fragment.
     * @return
     */
    public static ContainerFragment newInstance(String mediaId) {
        ContainerFragment containerFragment = new ContainerFragment();
        Bundle args = new Bundle();
        args.putString(MediaIdUtil.MEDIA_ID, mediaId);
        containerFragment.setArguments(args);
        return containerFragment;
    }

    /**
     * Loads the relevant fragment
     *
     * @param mediaId - ID of item clicked
     */
    private void navigateToBrowseFragment(String mediaId) {
        Log.i(TAG, "navigateToBrowseFragment" + mediaId);
        mBrowseFragment = getBrowseFragment(mediaId);
        mFragmentTransaction = getChildFragmentManager().beginTransaction();
        if (getChildFragmentManager().getBackStackEntryCount() != 0) {
            mFragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        } else {
            mFragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.exit_to_left);
        }
        mFragmentTransaction.addToBackStack(null);
        mFragmentTransaction.add(R.id.container_layout, mBrowseFragment);
        mFragmentTransaction.commit();
    }

    /**
     * Returns the relevant fragment
     *
     * @param mediaId - ID of item clicked
     * @return - Relevant fragment
     */
    public Fragment getBrowseFragment(String mediaId) {
        Log.i(TAG, "getBrowseFragment " + mediaId);
        Map<String, String> mHierarchyMap;
        if (mediaId.contains(MediaIdUtil.VALUE_SEPARATOR)) {
            mHierarchyMap = MediaIdUtil.getMediaHierarchyMap(mediaId);
        } else {
            mHierarchyMap = null;
        }
        MediaBrowserFragment mediaBrowserFragment = new MediaBrowserFragment();
        Bundle bundle = new Bundle();
        switch (MediaIdUtil.getHierarchyLevel(mediaId)) {
            case ROOT_TIER:
                switch (mediaId) {
                    case MediaIdUtil.ROOT_MEDIA_ARTISTS:
                        bundle.putString(MediaIdUtil.MEDIA_ID, MediaIdUtil.ROOT_MEDIA_ARTISTS);
                        bundle.putString(MediaIdUtil.ROOT_MEDIA_ID, MediaIdUtil.ROOT_MEDIA_ARTISTS);
                        bundle.putString(MediaIdUtil.BASE_MEDIA_ID, MediaIdUtil.ROOT_MEDIA_ARTISTS);
                        mediaBrowserFragment.setArguments(bundle);
                        return mediaBrowserFragment;
                    case MediaIdUtil.ROOT_MEDIA_ALBUMS:
                        bundle.putString(MediaIdUtil.MEDIA_ID, MediaIdUtil.ROOT_MEDIA_ALBUMS);
                        bundle.putString(MediaIdUtil.ROOT_MEDIA_ID, MediaIdUtil.ROOT_MEDIA_ALBUMS);
                        bundle.putString(MediaIdUtil.BASE_MEDIA_ID, MediaIdUtil.ROOT_MEDIA_ALBUMS);
                        mediaBrowserFragment.setArguments(bundle);
                        return mediaBrowserFragment;
                    case MediaIdUtil.ROOT_MEDIA_PLAYLISTS:
                        bundle.putString(MediaIdUtil.MEDIA_ID, MediaIdUtil.ROOT_MEDIA_PLAYLISTS);
                        bundle.putString(MediaIdUtil.ROOT_MEDIA_ID, MediaIdUtil.ROOT_MEDIA_PLAYLISTS);
                        bundle.putString(MediaIdUtil.BASE_MEDIA_ID, MediaIdUtil.ROOT_MEDIA_PLAYLISTS);
                        mediaBrowserFragment.setArguments(bundle);
                        return mediaBrowserFragment;
                    case MediaIdUtil.ROOT_MEDIA_SONGS:
                        bundle.putString(MediaIdUtil.MEDIA_ID, MediaIdUtil.ROOT_MEDIA_SONGS);
                        bundle.putString(MediaIdUtil.ROOT_MEDIA_ID, MediaIdUtil.ROOT_MEDIA_SONGS);
                        bundle.putString(MediaIdUtil.BASE_MEDIA_ID, MediaIdUtil.ROOT_MEDIA_SONGS);
                        mediaBrowserFragment.setArguments(bundle);
                        return mediaBrowserFragment;
                }
                break;
            case FIRST_TIER:
                switch (mHierarchyMap.keySet().toArray(new String[mHierarchyMap.size()])[0]) {
                    case MediaIdUtil.ROOT_MEDIA_ARTISTS:
                        bundle.putString(MediaIdUtil.MEDIA_ID, mediaId);
                        bundle.putString(MediaIdUtil.ROOT_MEDIA_ID, MediaIdUtil.ROOT_MEDIA_ALBUMS);
                        bundle.putString(MediaIdUtil.BASE_MEDIA_ID, MediaIdUtil.ROOT_MEDIA_ARTISTS);
                        mediaBrowserFragment.setArguments(bundle);
                        return mediaBrowserFragment;
                    default:
                        bundle.putString(MediaIdUtil.MEDIA_ID, mediaId);
                        bundle.putString(MediaIdUtil.ROOT_MEDIA_ID, MediaIdUtil.ROOT_MEDIA_SONGS);
                        bundle.putString(MediaIdUtil.BASE_MEDIA_ID, mHierarchyMap.keySet().toArray(new String[mHierarchyMap.size()])[0]);
                        mediaBrowserFragment.setArguments(bundle);
                        return mediaBrowserFragment;
                }
            case SECOND_TIER:
                bundle.putString(MediaIdUtil.MEDIA_ID, mediaId);
                bundle.putString(MediaIdUtil.ROOT_MEDIA_ID, MediaIdUtil.ROOT_MEDIA_SONGS);
                bundle.putString(MediaIdUtil.BASE_MEDIA_ID, mHierarchyMap.keySet().toArray(new String[mHierarchyMap.size()])[0]);
                mediaBrowserFragment.setArguments(bundle);
                return mediaBrowserFragment;
        }
        return null;
    }

    public void onBackKeyPress() {
        if (getChildFragmentManager().getBackStackEntryCount() > 1) {
            getChildFragmentManager().popBackStack();
            getChildFragmentManager().executePendingTransactions();

        } else {
            getActivity().moveTaskToBack(true);
        }
    }

    public void clearBackStack() {
        if (getChildFragmentManager().getBackStackEntryCount() > 1) {
            for (int i = 0; i < getChildFragmentManager().getBackStackEntryCount() - 1; i++)
                getChildFragmentManager().popBackStack();
            getChildFragmentManager().executePendingTransactions();
        }
    }

    public void onMediaItemSelected(MediaBrowserCompat.MediaItem item) {
        Log.i(TAG, "onMediaItemSelected");
        if (item.isPlayable()) {
            Log.d(TAG, "Item is playable");
            // mMediaFragmentListener.onMediaItemSelected(item);
        } else if (item.isBrowsable()) {
            Log.d(TAG, "Item is browsable");
            navigateToBrowseFragment(item.getMediaId());
        } else {
            Log.w(TAG, "Ignoring MediaItem that is neither browsable nor playable: MediaId= " + item.getMediaId());
        }
    }
}
