package com.alacritystudios.encore.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alacritystudios.encore.R;
import com.alacritystudios.encore.adapters.AddToPlaylistAdapter;
import com.alacritystudios.encore.adapters.MediaItemAdapter;
import com.alacritystudios.encore.adapters.OptionsAdapter;
import com.alacritystudios.encore.application.EncoreApplication;
import com.alacritystudios.encore.constants.CatalogueConstants;
import com.alacritystudios.encore.enums.MediaBrowserItemState;
import com.alacritystudios.encore.enums.SortOrder;
import com.alacritystudios.encore.fragments.base.BaseFragment;
import com.alacritystudios.encore.interfaces.DialogConfirmationCallback;
import com.alacritystudios.encore.interfaces.MediaBrowserFragmentListener;
import com.alacritystudios.encore.interfaces.OptionsSelectedListener;
import com.alacritystudios.encore.interfaces.OptionsTriggerListener;
import com.alacritystudios.encore.models.PlaylistModel;
import com.alacritystudios.encore.utils.ComponentUtil;
import com.alacritystudios.encore.utils.MediaIdUtil;
import com.alacritystudios.encore.utils.PlaylistUtils;
import com.alacritystudios.encore.utils.PreferenceUtil;
import com.alacritystudios.encore.utils.ProviderUtil;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Container fragment for browsing through media items in the library.
 */

public class MediaBrowserFragment extends BaseFragment implements BaseFragment.BaseFragmentImpl, OptionsSelectedListener,
        OptionsTriggerListener, DialogConfirmationCallback, SharedPreferences.OnSharedPreferenceChangeListener {

    private static final int DIALOG_SONGS_DEFAULT_OPTIONS = 322;
    private static final int DIALOG_SONGS_PLAYLIST_OPTIONS = 323;
    private static final int DIALOG_ADD_TO_PLAYLIST_OPTIONS = 324;
    private static final int DIALOG_PLAYLIST_OPTIONS = 325;
    private static final int DELETE_SONG_FROM_DEVICE_REFERENCE = 7000;
    private static final int DELETE_SONG_FROM_PLAYLIST_REFERENCE = 7001;
    private static final int DELETE_PLAYLIST_FROM_DEVICE_REFERENCE = 7002;

    private String mRootMediaType;
    private String mBaseMediaType;
    private String mMediaId;
    private String mSearchQueryString;
    private SortOrder mSortOrder;
    private String[] mOptionsList;
    private boolean mShouldShowOptions;
    private boolean mShouldShowAnimations;
    private boolean mShouldShowAlbumArt;
    private Drawable mPlaceholderDrawable;
    private int mPrimaryColor;
    private int mDialogOptionsReference;
    private List<PlaylistModel> mPlaylistList;
    private MediaBrowserItemState mMediaBrowserItemState;

    private MediaBrowserCompat mMediaBrowserCompat;
    private List<MediaBrowserCompat.MediaItem> mFetchedMediaItems;
    private List<MediaBrowserCompat.MediaItem> mFilteredMediaItems;
    private MediaBrowserFragmentListener mMediaBrowserFragmentListener;
    private MediaBrowserCompat.MediaItem mSelectedMediaItem;

    private LinearLayout mPlaceholderLayout;
    private ImageView mRetryImageview;
    private AlertDialog mOptionsAlertDialog;
    private RecyclerView mOptionsRecyclerview;
    private View mOptionsView;
    private RecyclerView mMediaItemsRecyclerView;
    private AlertDialog mAddToPlaylistAlertDialog;
    private View mAddToPlaylistView;
    private RecyclerView mAddToPlaylistsRecyclerview;
    private AlertDialog mNewPlaylistAlertDialog;
    private View mNewPlaylistView;

    private MediaItemAdapter mMediaItemAdapter;
    private OptionsAdapter mOptionsAdapter;
    private AddToPlaylistAdapter mAddToPlaylistAdapter;
    private LinearLayoutManager mMediaItemsLayoutManager;
    private LinearLayoutManager mOptionsLayoutManager;
    private LinearLayoutManager mAddToPlaylistsLayoutManager;
    private BroadcastReceiver mReRenderListReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG, "onReceiveReRenderListBroadcastReceiver");
            mSearchQueryString = mMediaBrowserFragmentListener.getmSearchQueryString();
            setRecyclerViewData(mFetchedMediaItems);
        }
    };
    private MediaBrowserCompat.SubscriptionCallback subscriptionCallback = new MediaBrowserCompat.SubscriptionCallback() {
        @Override
        public void onChildrenLoaded(@NonNull String parentId, List<MediaBrowserCompat.MediaItem> children) {
            super.onChildrenLoaded(parentId, children);
            Log.i(TAG, "SubscripttionCallbackRecieved. Number of items: " + children.size());
            mFetchedMediaItems = children;
            setRecyclerViewData(mFetchedMediaItems);
        }

        @Override
        public void onChildrenLoaded(@NonNull String parentId, List<MediaBrowserCompat.MediaItem> children, @NonNull Bundle options) {
            super.onChildrenLoaded(parentId, children, options);
        }

        @Override
        public void onError(@NonNull String parentId) {
            super.onError(parentId);
        }

        @Override
        public void onError(@NonNull String parentId, @NonNull Bundle options) {
            super.onError(parentId, options);
        }
    };
    private BroadcastReceiver mReloadLibraryReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG, "onReceiveReloadLibraryBroadcastReceiver");
            reloadItems();
        }
    };

    @Override
    public void initialiseUI() {
        mPlaceholderLayout = (LinearLayout) mFragmentView.findViewById(R.id.placeholder_layout);
        mMediaItemsRecyclerView = (RecyclerView) mFragmentView.findViewById(R.id.recycler_view_media_items);
        mMediaItemsRecyclerView.setHasFixedSize(true);
        mMediaItemsLayoutManager = new LinearLayoutManager(mContext);
        mMediaItemsLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRetryImageview = (ImageView) mFragmentView.findViewById(R.id.retry_imageview);
        mMediaItemAdapter = new MediaItemAdapter(mContext, mFilteredMediaItems, mMediaBrowserFragmentListener, this, mPlaceholderDrawable, mMediaBrowserItemState);
        mMediaItemsRecyclerView.setLayoutManager(mMediaItemsLayoutManager);
        mMediaItemsRecyclerView.setAdapter(mMediaItemAdapter);
        reloadItems();
    }

    @Override
    public void setListeners() {
        mRetryImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reloadItems();
            }
        });
    }

    @Override
    public void setContextAndTags() {
        TAG = getClass().getSimpleName();
        mContext = getActivity();
    }

    @Override
    public void initialiseOtherComponents() {
        IntentFilter reloadLibraryFilter = new IntentFilter("com.alacritystudios.encore.RELOAD_MUSIC_LIBRARY");
        IntentFilter searchQueryFilter = new IntentFilter("com.alacritystudios.encore.RELOAD_MUSIC_LIST");
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(mContext);
        localBroadcastManager.registerReceiver(mReloadLibraryReceiver, reloadLibraryFilter);
        localBroadcastManager.registerReceiver(mReRenderListReceiver, searchQueryFilter);
        TypedValue typedValue = new TypedValue();
        mContext.getTheme().resolveAttribute(R.attr.colorPrimary, typedValue, true);
        mPrimaryColor = typedValue.data;
        switch (mRootMediaType) {
            case MediaIdUtil.ROOT_MEDIA_PLAYLISTS:
                mOptionsList = mContext.getResources().getStringArray(R.array.playlist_options);
                mShouldShowOptions = true;
                mPlaceholderDrawable = ActivityCompat.getDrawable(mContext, R.drawable.ic_music_playlist);
                DrawableCompat.wrap(mPlaceholderDrawable).mutate().setTint(mPrimaryColor);
                mDialogOptionsReference = DIALOG_PLAYLIST_OPTIONS;
                break;
            case MediaIdUtil.ROOT_MEDIA_SONGS:
                if (mBaseMediaType.equals(MediaIdUtil.ROOT_MEDIA_PLAYLISTS)) {
                    mOptionsList = mContext.getResources().getStringArray(R.array.songs_playlist_options);
                    mDialogOptionsReference = DIALOG_SONGS_PLAYLIST_OPTIONS;
                } else {mOptionsList = mContext.getResources().getStringArray(R.array.songs_options);
                    mDialogOptionsReference = DIALOG_SONGS_DEFAULT_OPTIONS;
                }

                if (mBaseMediaType.equals(MediaIdUtil.ROOT_MEDIA_PLAYLISTS)) {
                    mOptionsList = mContext.getResources().getStringArray(R.array.songs_playlist_options);
                    mDialogOptionsReference = DIALOG_SONGS_PLAYLIST_OPTIONS;
                }
                mShouldShowOptions = true;
                mPlaceholderDrawable = ActivityCompat.getDrawable(mContext, R.drawable.ic_music_now_playing);
                DrawableCompat.wrap(mPlaceholderDrawable).mutate().setTint(mPrimaryColor);
                break;
            case MediaIdUtil.ROOT_MEDIA_ARTISTS:
                mOptionsList = new String[0];
                mShouldShowOptions = false;
                mPlaceholderDrawable = ActivityCompat.getDrawable(mContext, R.drawable.ic_music_artist);
                DrawableCompat.wrap(mPlaceholderDrawable).mutate().setTint(mPrimaryColor);
                break;
            case MediaIdUtil.ROOT_MEDIA_ALBUMS:
                mOptionsList = new String[0];
                mShouldShowOptions = false;
                mPlaceholderDrawable = ActivityCompat.getDrawable(mContext, R.drawable.ic_music_album);
                DrawableCompat.wrap(mPlaceholderDrawable).mutate().setTint(mPrimaryColor);
                break;
        }
        PreferenceManager.getDefaultSharedPreferences(getActivity()).registerOnSharedPreferenceChangeListener(this);
        mFilteredMediaItems = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_media_browser, container, false);
        setContextAndTags();
        mMediaId = this.getArguments().getString(MediaIdUtil.MEDIA_ID);
        mBaseMediaType = this.getArguments().getString(MediaIdUtil.BASE_MEDIA_ID);
        mRootMediaType = this.getArguments().getString(MediaIdUtil.ROOT_MEDIA_ID);
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
        return mFragmentView;
    }

    @Override
    public void onAttach(Context context) {
        Log.i(TAG, "onAttach()");
        super.onAttach(context);
    }

    @Override
    public void onDialogConfirm(int buttonReference) {
        if (buttonReference == DELETE_SONG_FROM_DEVICE_REFERENCE) {
            MediaMetadataCompat mediaMetadataCompat = EncoreApplication.getmMusicProvider().getMusic(MediaIdUtil.extractMusicIdFromMediaId(mSelectedMediaItem.getDescription().getMediaId()));
            String source;
            if (mMediaBrowserFragmentListener.getmMediaControllerCompat() != null && mMediaBrowserFragmentListener.getmMediaControllerCompat().getMetadata() != null) {
                if (mediaMetadataCompat.getString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID).equals(String.valueOf(MediaIdUtil.extractMusicIdFromMediaId(mMediaBrowserFragmentListener.getmMediaControllerCompat().getMetadata().getDescription().getMediaId())))) {
                    ComponentUtil.createAndShowSnackbar(mContext, mFragmentView, mContext.getString(R.string.song_currently_in_use), 1000);
                }
            } else {
                source = mediaMetadataCompat.getBundle().getString(CatalogueConstants.GeneralConstants.CUSTOM_METADATA_TRACK_SOURCE);
                new AsyncTask<String, Void, Void>() {
                    @Override
                    protected Void doInBackground(String... params) {
                        File file = new File(params[0]);
                        if (file.delete()) {
                            deleteFileCallback(true, params[0]);
                        } else {
                            deleteFileCallback(false, params[0]);
                        }

                        return null;
                    }
                }.execute(source);
            }
        } else if (buttonReference == DELETE_SONG_FROM_PLAYLIST_REFERENCE) {
            PlaylistUtils.deleteSongFromPlaylist(mContext.getContentResolver(), mSelectedMediaItem.getDescription().getMediaId(), MediaIdUtil.getBaseMediaValue(mSelectedMediaItem.getMediaId()));
            ComponentUtil.createAndShowSnackbar(mContext, mFragmentView, mContext.getString(R.string.song_deleted_from_playlist), 1000);
        } else if (buttonReference == DELETE_PLAYLIST_FROM_DEVICE_REFERENCE) {
            PlaylistUtils.deletePlaylist(mContext.getContentResolver(), MediaIdUtil.getBaseMediaValue(mSelectedMediaItem.getMediaId()));
            ComponentUtil.createAndShowSnackbar(mContext, mFragmentView, mContext.getString(R.string.playlist_deleted_from_device), 1000);
        }
    }

    @Override
    public void onSelectOption(int position, int callbackId) {
        MediaMetadataCompat mediaMetadataCompat = EncoreApplication.getmMusicProvider().getMusic(MediaIdUtil.extractMusicIdFromMediaId(mSelectedMediaItem.getMediaId()));
        switch (callbackId) {
            case DIALOG_SONGS_DEFAULT_OPTIONS:
                switch (position) {
                    case 0:
                        mMediaBrowserFragmentListener.onMediaItemClicked(mSelectedMediaItem);
                        break;
                    case 1:
                        mAddToPlaylistView = ((Activity) mContext).getLayoutInflater().inflate(R.layout.dialog_add_to_playlist, null);
                        mAddToPlaylistView.findViewById(R.id.create_new_playlist).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mAddToPlaylistAlertDialog.cancel();
                                mNewPlaylistView = ((Activity) mContext).getLayoutInflater().inflate(R.layout.dialog_create_new_playlist, null);
                                mNewPlaylistAlertDialog = new AlertDialog.Builder(mContext)
                                        .setView(mNewPlaylistView)
                                        .setPositiveButton(R.string.create, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                EditText mPlaylistNameEdittext = (EditText) mNewPlaylistView.findViewById(R.id.playlist_name_edittext);
                                                PlaylistModel mPlaylistDetails = new PlaylistModel();
                                                mPlaylistDetails.setPlaylistName(mPlaylistNameEdittext.getText().toString());
                                                PlaylistUtils.addNewPlaylist(mContext.getContentResolver(), mPlaylistDetails, mSelectedMediaItem.getMediaId());
                                                mNewPlaylistAlertDialog.cancel();
                                            }
                                        })
                                        .create();
                                final TypedValue typedValue = new TypedValue();
                                mContext.getTheme().resolveAttribute(R.attr.colorAccent, typedValue, true);
                                mNewPlaylistAlertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                    @Override
                                    public void onShow(DialogInterface arg0) {
                                        mNewPlaylistAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(typedValue.data);
                                    }
                                });
                                mNewPlaylistAlertDialog.show();
                            }
                        });
                        mPlaylistList = PlaylistUtils.getAllPlaylists(mContext.getContentResolver());
                        mAddToPlaylistsRecyclerview = (RecyclerView) mAddToPlaylistView.findViewById(R.id.options_recyclerview);
                        mAddToPlaylistAdapter = new AddToPlaylistAdapter(mContext, this, mPlaylistList, DIALOG_ADD_TO_PLAYLIST_OPTIONS);
                        mAddToPlaylistsLayoutManager = new LinearLayoutManager(mContext);
                        mAddToPlaylistsLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        mAddToPlaylistsRecyclerview.setLayoutManager(mAddToPlaylistsLayoutManager);
                        mAddToPlaylistsRecyclerview.setAdapter(mAddToPlaylistAdapter);
                        mAddToPlaylistAlertDialog = new AlertDialog.Builder(mContext)
                                .setView(mAddToPlaylistView)
                                .create();
                        mAddToPlaylistAlertDialog.show();
                        break;
                    case 2:
                        Uri uri = Uri.parse(Uri.encode(mediaMetadataCompat.getString(CatalogueConstants.GeneralConstants.CUSTOM_METADATA_TRACK_SOURCE)));
                        Intent share = new Intent(Intent.ACTION_SEND);
                        share.setType("audio/*");
                        share.putExtra(Intent.EXTRA_STREAM, uri);
                        startActivity(Intent.createChooser(share, "Share"));
                        break;
                    case 3:
                        ComponentUtil.confirmationAlertDialog(mContext, mContext.getString(R.string.delete_song_confirmation), mContext.getString(R.string.confirm_delete_song), this, DELETE_SONG_FROM_DEVICE_REFERENCE).show();
                        break;
                }
                mOptionsAlertDialog.cancel();
                break;

            case DIALOG_SONGS_PLAYLIST_OPTIONS:
                switch (position) {
                    case 0:
                        mMediaBrowserFragmentListener.onMediaItemClicked(mSelectedMediaItem);
                        break;
                    case 1:
                        ComponentUtil.confirmationAlertDialog(mContext, mContext.getString(R.string.delete_song_from_playlist_confirmation), mContext.getString(R.string.confirm_delete_song), this, DELETE_SONG_FROM_PLAYLIST_REFERENCE).show();
                        break;
                    case 2:
                        Uri uri = Uri.parse(Uri.encode(mediaMetadataCompat.getString(CatalogueConstants.GeneralConstants.CUSTOM_METADATA_TRACK_SOURCE)));
                        Intent share = new Intent(Intent.ACTION_SEND);
                        share.setType("audio/*");
                        share.putExtra(Intent.EXTRA_STREAM, uri);
                        startActivity(Intent.createChooser(share, "Share"));
                        break;
                    case 3:
                        ComponentUtil.confirmationAlertDialog(mContext, mContext.getString(R.string.delete_song_confirmation), mContext.getString(R.string.confirm_delete_song), this, DELETE_SONG_FROM_DEVICE_REFERENCE).show();
                        break;
                }
                mOptionsAlertDialog.cancel();
                break;

            case DIALOG_ADD_TO_PLAYLIST_OPTIONS:
                PlaylistUtils.addSongToPlaylist(mContext.getContentResolver(), mSelectedMediaItem.getMediaId(), mPlaylistList.get(position).getPlaylistId());
                mAddToPlaylistAlertDialog.cancel();
                break;
            case DIALOG_PLAYLIST_OPTIONS:
                switch (position) {
                    case 0:
                        ComponentUtil.confirmationAlertDialog(mContext, mContext.getString(R.string.delete_playlist_confirmation), mContext.getString(R.string.confirm_delete_playlist), this, DELETE_PLAYLIST_FROM_DEVICE_REFERENCE).show();
                        mOptionsAlertDialog.cancel();
                }
                break;
        }
    }

    @Override
    public void clickedMediaId(MediaBrowserCompat.MediaItem mediaItem) {
        Log.i(TAG, "Clicked media options: " + mediaItem);
        mSelectedMediaItem = mediaItem;
        mOptionsView = ((Activity) mContext).getLayoutInflater().inflate(R.layout.dialog_options, null);
        mOptionsRecyclerview = (RecyclerView) mOptionsView.findViewById(R.id.options_recyclerview);
        mOptionsAdapter = new OptionsAdapter(Arrays.asList(mOptionsList), mContext, this, mDialogOptionsReference);
        mOptionsLayoutManager = new LinearLayoutManager(mContext);
        mOptionsLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mOptionsRecyclerview.setLayoutManager(mOptionsLayoutManager);
        mOptionsRecyclerview.setAdapter(mOptionsAdapter);
        mOptionsAlertDialog = new AlertDialog.Builder(mContext)
                .setView(mOptionsView)
                .create();
        mOptionsAlertDialog.show();
    }

    @Override
    public void clickedQueueId(MediaSessionCompat.QueueItem queueItem) {

    }

    @Override
    public void clickedMediaInfo(MediaBrowserCompat.MediaItem mediaItem) {
        if (mRootMediaType.equals(MediaIdUtil.ROOT_MEDIA_SONGS)) {
            showSongDetails(mediaItem);
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(PreferenceUtil.CURRENT_SORT_ORDER_SETTING) || key.equals(PreferenceUtil.SHOW_ALBUM_ART) || key.equals(PreferenceUtil.SHOW_ANIMATIONS)) {
            setRecyclerViewData(mFetchedMediaItems);
        }
    }

    /**
     * Used to get mediaBrowser related data from the parent activity.
     */
    public void fetchMediaBrowserRelatedDataFromActivity() {
        Log.i(TAG, "fetchMediaBrowserRelatedDataFromActivity()");
        mMediaBrowserFragmentListener = (MediaBrowserFragmentListener) getActivity();
        if(mMediaBrowserFragmentListener != null) {
            mMediaBrowserCompat = mMediaBrowserFragmentListener.getmMediaBrowserCompat();
            mSearchQueryString = mMediaBrowserFragmentListener.getmSearchQueryString();
        }
    }

    public void setRecyclerViewData(List<MediaBrowserCompat.MediaItem> mMediaItems) {
        if (!mSearchQueryString.isEmpty()) {
            mFilteredMediaItems = ProviderUtil.searchMediaItemByName(mMediaItems, mSearchQueryString);
        } else {
            mFilteredMediaItems = mMediaItems;
        }
        if (PreferenceUtil.getCurrentSortOrderPreference(mContext) == 0) {
            mSortOrder = SortOrder.ASCENDING;
        } else {
            mSortOrder = SortOrder.DESCENDING;
        }
        if (mSortOrder == SortOrder.ASCENDING) {
            Collections.sort(mFilteredMediaItems, new Comparator<MediaBrowserCompat.MediaItem>() {
                @Override
                public int compare(MediaBrowserCompat.MediaItem o1, MediaBrowserCompat.MediaItem o2) {
                    return o1.getDescription().getTitle().toString().compareTo(o2.getDescription().getTitle().toString());
                }
            });
        } else {
            Collections.sort(mFilteredMediaItems, new Comparator<MediaBrowserCompat.MediaItem>() {
                @Override
                public int compare(MediaBrowserCompat.MediaItem o1, MediaBrowserCompat.MediaItem o2) {
                    return o2.getDescription().getTitle().toString().compareTo(o1.getDescription().getTitle().toString());
                }
            });
        }

        if (mFilteredMediaItems.size() == 0) {
            mPlaceholderLayout.setVisibility(View.VISIBLE);
            mMediaItemsRecyclerView.setVisibility(View.GONE);
        } else {
            mMediaBrowserItemState = fetchItemStates();
            mMediaItemsRecyclerView.setVisibility(View.VISIBLE);
            mPlaceholderLayout.setVisibility(View.GONE);
            mMediaItemAdapter.setmMediaBrowserItemState(mMediaBrowserItemState);
            mMediaItemAdapter.setmMediaItemList(mFilteredMediaItems);
            mMediaItemAdapter.setmMediaBrowserFragmentListener(mMediaBrowserFragmentListener);
            mMediaItemAdapter.notifyDataSetChanged();
        }
    }

    public void showSongDetails(MediaBrowserCompat.MediaItem mediaItem) {
        MediaMetadataCompat mediaMetadataCompat = EncoreApplication.getmMusicProvider().getMusic(MediaIdUtil.extractMusicIdFromMediaId(mediaItem.getMediaId()));
        String source = mediaMetadataCompat.getString(CatalogueConstants.GeneralConstants.CUSTOM_METADATA_TRACK_SOURCE);
        File file = new File(source);
        long size = file.length() / 1024;
        String value = "";
        if (size >= 1024) {
            value = size / 1024 + " " + mContext.getString(R.string.mb);
        } else {
            value = size + " " + mContext.getString(R.string.kb);
        }

        long durationLong = mediaMetadataCompat.getLong(MediaMetadataCompat.METADATA_KEY_DURATION);
        String duration = String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(durationLong),
                TimeUnit.MILLISECONDS.toSeconds(durationLong) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(durationLong)));
        MediaExtractor mediaExtractor = new MediaExtractor();
        try {
            mediaExtractor.setDataSource(mediaMetadataCompat.getString(CatalogueConstants.GeneralConstants.CUSTOM_METADATA_TRACK_SOURCE));// the adresss location of the sound on sdcard.
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        int bitRate = 0;
        int sampleRate = 0;
        int channelCount = 0;
        String mimeType = "";
        MediaFormat mediaFormat = mediaExtractor.getTrackFormat(0);
        try {
            bitRate = mediaFormat.getInteger(MediaFormat.KEY_BIT_RATE);
        } catch (NullPointerException ex) {
            bitRate = 0;
        }
        try {
            sampleRate = mediaFormat.getInteger(MediaFormat.KEY_SAMPLE_RATE);
        } catch (NullPointerException ex) {
            sampleRate = 0;
        }
        try {
            channelCount = mediaFormat.getInteger(MediaFormat.KEY_CHANNEL_COUNT);
        } catch (NullPointerException ex) {
            channelCount = 0;
        }
        try {
            mimeType = mediaFormat.getString(MediaFormat.KEY_MIME);
        } catch (NullPointerException ex) {
            mimeType = "";
        }
        View view = ((Activity) mContext).getLayoutInflater().inflate(R.layout.dialog_media_info, null);
        TextView nameTextView = (TextView) view.findViewById(R.id.song_info_name);
        nameTextView.setText(mediaMetadataCompat.getString(MediaMetadataCompat.METADATA_KEY_TITLE));
        TextView pathTextView = (TextView) view.findViewById(R.id.song_info_path);
        pathTextView.setText(mediaMetadataCompat.getString(CatalogueConstants.GeneralConstants.CUSTOM_METADATA_TRACK_SOURCE));
        TextView durationTextView = (TextView) view.findViewById(R.id.song_info_duration);
        durationTextView.setText(duration);
        TextView formatTextView = (TextView) view.findViewById(R.id.song_info_format);
        formatTextView.setText(mimeType);
        TextView sizeTextView = (TextView) view.findViewById(R.id.song_info_size);
        sizeTextView.setText(value);
        TextView bitrateTextView = (TextView) view.findViewById(R.id.song_info_bit_rate);
        bitrateTextView.setText(String.valueOf(bitRate / 1000) + " " + mContext.getString(R.string.kbps));
        TextView samplingrateTextView = (TextView) view.findViewById(R.id.song_info_sampling_rate);
        samplingrateTextView.setText(String.valueOf(sampleRate) + " " + mContext.getString(R.string.Hz));
        TextView channelcountTextView = (TextView) view.findViewById(R.id.song_info_channel_count);
        channelcountTextView.setText(String.valueOf(channelCount));
        final ImageView songInfoImageView = (ImageView) view.findViewById(R.id.song_info_imageview);
        if (mShouldShowAlbumArt) {
            Drawable placeholderImage = ActivityCompat.getDrawable(mContext, R.drawable.ic_music_now_playing);
            DrawableCompat.wrap(placeholderImage).mutate().setTint(mPrimaryColor);
            songInfoImageView.setPadding(24, 24, 24, 24);
            Glide.with((Activity) mContext)
                    .load(mediaItem.getDescription().getIconUri())
                    .centerCrop()
                    .placeholder(placeholderImage)
                    .priority(Priority.LOW)
                    .skipMemoryCache(true)
                    .thumbnail(0.1f)
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .dontAnimate().listener(new RequestListener<Uri, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, Uri model, Target<GlideDrawable> target, boolean isFirstResource) {
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, Uri model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                    songInfoImageView.setPadding(0, 0, 0, 0);
                    return false;
                }
            }).into(songInfoImageView);
            songInfoImageView.setClipToOutline(true);
        } else {
            Drawable placeholderImage = ActivityCompat.getDrawable(mContext, R.drawable.ic_music_now_playing);
            DrawableCompat.wrap(placeholderImage).mutate().setTint(mPrimaryColor);
            songInfoImageView.setPadding(24, 24, 24, 24);
            songInfoImageView.setImageDrawable(placeholderImage);
            songInfoImageView.setClipToOutline(true);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(view);
        builder.setPositiveButton(R.string.okay, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        final TypedValue typedValue = new TypedValue();
        mContext.getTheme().resolveAttribute(R.attr.colorAccent, typedValue, true);
        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(typedValue.data);
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(typedValue.data);
            }
        });
        alertDialog.show();
    }

    public void reloadItems() {
        fetchMediaBrowserRelatedDataFromActivity();
        if (mMediaBrowserCompat != null && mMediaBrowserCompat.isConnected()) {
            mMediaBrowserCompat.unsubscribe(mMediaId);
            mMediaBrowserCompat.subscribe(mMediaId, subscriptionCallback);
        }
    }

    /**
     * Helper method to find out item states for media item adapter.
     *
     * @return - Item state
     */
    public MediaBrowserItemState fetchItemStates() {
        mShouldShowAlbumArt = PreferenceUtil.getCurrentShowAlbumArtPreference(mContext);
        mShouldShowAnimations = PreferenceUtil.getCurrentShowAnimationPreference(mContext);
        if (mShouldShowAlbumArt && mShouldShowAnimations && mShouldShowOptions) {
            return MediaBrowserItemState.ALL;
        } else if (!(mShouldShowAlbumArt || mShouldShowAnimations || mShouldShowOptions)) {
            return MediaBrowserItemState.NONE;
        } else if (mShouldShowAlbumArt ^ mShouldShowAnimations ^ mShouldShowOptions) {
            if (mShouldShowAlbumArt) {
                return MediaBrowserItemState.ALBUM_ART_ONLY;
            } else if (mShouldShowAnimations) {
                return MediaBrowserItemState.ANIMATION_ONLY;
            } else {
                return MediaBrowserItemState.OPTIONS_ONLY;
            }
        } else {
            if (mShouldShowAlbumArt && mShouldShowAnimations) {
                return MediaBrowserItemState.ALBUM_ART_ANIMATION;
            } else if (mShouldShowAlbumArt && mShouldShowOptions) {
                return MediaBrowserItemState.ALBUM_ART_OPTIONS;
            } else {
                return MediaBrowserItemState.ANIMATION_OPTIONS;
            }
        }
    }

    /**
     * Function to handle the callback for deleting files from device.
     *
     * @param success - Delete successful?
     * @param source  - File source
     */
    public void deleteFileCallback(boolean success, String source) {
        Log.i(TAG, "deleteFileCallback");
        if (success) {
            mContext.getContentResolver().delete(
                    MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    MediaStore.MediaColumns.DATA + "='" + source + "'", null
            );
            EncoreApplication.updateMusicProvider();
            ComponentUtil.createAndShowSnackbar(mContext, mFragmentView, mContext.getString(R.string.song_deleted_from_device), 1000);
        } else {
            ComponentUtil.createAndShowSnackbar(mContext, mFragmentView, mContext.getString(R.string.failed_to_delete), 1000);
        }
    }
}
