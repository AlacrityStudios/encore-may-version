package com.alacritystudios.encore.fragments;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadata;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.alacritystudios.encore.R;
import com.alacritystudios.encore.adapters.AddToPlaylistAdapter;
import com.alacritystudios.encore.adapters.QueueAdapter;
import com.alacritystudios.encore.application.EncoreApplication;
import com.alacritystudios.encore.components.CustomFrameLayout;
import com.alacritystudios.encore.constants.CatalogueConstants;
import com.alacritystudios.encore.fragments.base.BaseFragment;
import com.alacritystudios.encore.helpers.MediaItemTouchHelperSimpleCallback;
import com.alacritystudios.encore.interfaces.MediaBrowserFragmentListener;
import com.alacritystudios.encore.interfaces.OptionsSelectedListener;
import com.alacritystudios.encore.interfaces.OptionsTriggerListener;
import com.alacritystudios.encore.interfaces.QueueAdapterListener;
import com.alacritystudios.encore.models.PlaylistModel;
import com.alacritystudios.encore.utils.CustomUtils;
import com.alacritystudios.encore.utils.PlaylistUtils;
import com.alacritystudios.encore.utils.PreferenceUtil;
import com.alacritystudios.encore.utils.ProviderUtil;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Fragment to show media controls.
 */

public class MediaPlayerFragment extends BaseFragment implements BaseFragment.BaseFragmentImpl, QueueAdapterListener, OptionsTriggerListener, OptionsSelectedListener {

    private MediaBrowserFragmentListener mMediaBrowserFragmentListener;
    private MediaControllerCompat mMediaControllerCompat;
    private String mSelectedMediaId;
    private ImageView mMiniPlayPauseImageview;
    private TextView mMiniCurrentSongNameTextview;

    private final String ACTION_RE_ORDER_QUEUE = "ACTION_RE_ORDER_QUEUE";
    private final String ACTION_RE_ORDER_QUEUE_FROM_POSITION = "ACTION_RE_ORDER_QUEUE_FROM_POSITION";
    private final String ACTION_RE_ORDER_QUEUE_TO_POSITION = "ACTION_RE_ORDER_QUEUE_TO_POSITION";
    private final String ACTION_PLAY_QUEUE_ITEM = "ACTION_PLAY_QUEUE_ITEM";
    private final String ACTION_PLAY_QUEUE_ITEM_POSITION = "ACTION_PLAY_QUEUE_ITEM_POSITION";
    private boolean mIsPlayerPlaying;

    private static final long PROGRESS_UPDATE_INTERNAL = 50;
    private static final long PROGRESS_UPDATE_INITIAL_INTERVAL = 100;
    private static final int DIALOG_ADD_TO_PLAYLIST_OPTIONS = 324;
    private final ScheduledExecutorService mExecutorService = Executors.newSingleThreadScheduledExecutor();
    private List<MediaSessionCompat.QueueItem> mQueueItems;
    private ContentResolver mContentResolver;
    private CustomFrameLayout mCustomFrameLayout;
    private CoordinatorLayout mMediaArtCoordinatorLayout;
    private View mBottomSheetLayoutView;
    private TextView mMusicBasicInfoTextview;
    private ImageView mMediaArtImageView;
    private ImageView mPlayPauseImageview;
    private ImageView mSkipToNextImageview;
    private ImageView mSkipToPreviousImageview;
    private ImageView mShareImageview;
    private ImageView mFavoriteImageview;
    private ImageView mRepeatImageview;
    private ImageView mShuffleImageview;
    private ImageView mShowPlayingListImageview;
    private ImageView mAddToPlaylistImageview;
    private SeekBar mSongPositionSeekbar;
    private AdView mAdView;
    private ScheduledFuture<?> mScheduleFuture;
    private Handler mHandler = new Handler();
    private final Runnable mUpdateProgressTask = new Runnable() {
        @Override
        public void run() {
            updateProgress();
        }
    };


    private QueueAdapter mQueueAdapter;
    private LinearLayoutManager mQueueLinearLayoutManager;
    private RecyclerView mQueueRecyclerView;
    private BottomSheetDialog mBottomSheetDialog;
    private AlertDialog mAddToPlaylistAlertDialog;
    private View mAddToPlaylistView;
    private RecyclerView mAddToPlaylistsRecyclerview;
    private AlertDialog mNewPlaylistAlertDialog;
    private View mNewPlaylistView;
    private List<PlaylistModel> mPlaylistList;
    private AddToPlaylistAdapter mAddToPlaylistAdapter;
    private LinearLayoutManager mAddToPlaylistsLayoutManager;


    private MediaControllerCompat.Callback mMediaControllerCallback = new MediaControllerCompat.Callback() {
        @Override
        public void onMetadataChanged(MediaMetadataCompat metadata) {
            super.onMetadataChanged(metadata);
            Log.d(TAG, "onMetadataChanged");
            mSelectedMediaId = metadata.getDescription().getMediaId();
            updateDescription(metadata);
            updateDurationOfSeekbar(metadata);
            updateQueue();
        }

        @Override
        public void onPlaybackStateChanged(PlaybackStateCompat state) {
            super.onPlaybackStateChanged(state);
            Log.d(TAG, "onPlaybackStateChanged");
            updatePlaybackState(state);
        }
    };


    @Override
    public void onQueueItemClicked(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt(ACTION_PLAY_QUEUE_ITEM_POSITION, position);
        mMediaControllerCompat.getTransportControls().sendCustomAction(ACTION_PLAY_QUEUE_ITEM, bundle);
        mBottomSheetDialog.cancel();
    }

    @Override
    public void onQueueItemRemoved(MediaDescriptionCompat mediaDescriptionCompat) {
        mMediaControllerCompat.removeQueueItem(mediaDescriptionCompat);
        mQueueItems = mMediaControllerCompat.getQueue();
    }

    @Override
    public void onQueueItemMoved(int fromPosition, int toPosition) {
        Bundle bundle = new Bundle();
        bundle.putInt(ACTION_RE_ORDER_QUEUE_FROM_POSITION, fromPosition);
        bundle.putInt(ACTION_RE_ORDER_QUEUE_TO_POSITION, toPosition);
        mMediaControllerCompat.getTransportControls().sendCustomAction(ACTION_RE_ORDER_QUEUE, bundle);
    }

    //
//    @Override
//    public void onSelectOption(int position, int callbackId) {
//        switch (callbackId) {
//            case DIALOG_PLAYLISTS_OPTIONS:
//                PlaylistUtils.addSongToPlaylist(mContentResolver, mSelectedMediaId, mPlaylists.get(position).getPlaylistId());
//                mPlaylistAlertDialog.cancel();
//                break;
//        }
//
//    }
//


    @Override
    public void clickedMediaId(MediaBrowserCompat.MediaItem mediaItem) {

    }

    @Override
    public void clickedQueueId(MediaSessionCompat.QueueItem queueItem) {

    }

    @Override
    public void clickedMediaInfo(MediaBrowserCompat.MediaItem mediaItem) {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_media_player, container, false);
        setContextAndTags();
        mContentResolver = mContext.getContentResolver();
        initialiseOtherComponents();
        initialiseUI();
        setListeners();
        return mFragmentView;
    }

    @Override
    public void initialiseUI() {
        Log.i(TAG, "initialiseUI()");
        //Mini player.
        mMiniPlayPauseImageview = (ImageView) mFragmentView.findViewById(R.id.play_pause_button);
        mMiniCurrentSongNameTextview = (TextView) mFragmentView.findViewById(R.id.current_music_name_textview);
        //Large player

        mMediaArtCoordinatorLayout = (CoordinatorLayout) mFragmentView.findViewById(R.id.media_album_art_coordinator_layout);
        mCustomFrameLayout = (CustomFrameLayout) mFragmentView.findViewById(R.id.custom_frame_layout);
        mMusicBasicInfoTextview = (TextView) mFragmentView.findViewById(R.id.textview_music_basic_info);
        mMediaArtImageView = (ImageView) mFragmentView.findViewById(R.id.imageview_music_art);
        mPlayPauseImageview = (ImageView) mFragmentView.findViewById(R.id.media_button_play_pause);
        mSkipToNextImageview = (ImageView) mFragmentView.findViewById(R.id.media_button_skip_to_next);
        mSkipToPreviousImageview = (ImageView) mFragmentView.findViewById(R.id.media_button_skip_to_previous);
        mShareImageview = (ImageView) mFragmentView.findViewById(R.id.media_button_share);
        mFavoriteImageview = (ImageView) mFragmentView.findViewById(R.id.media_button_favorite);

        mRepeatImageview = (ImageView) mFragmentView.findViewById(R.id.media_button_repeat);
        mShuffleImageview = (ImageView) mFragmentView.findViewById(R.id.media_button_shuffle);
        mShowPlayingListImageview = (ImageView) mFragmentView.findViewById(R.id.media_button_show_playlist);
        mAddToPlaylistImageview = (ImageView) mFragmentView.findViewById(R.id.media_button_add_to_playlist);
        mSongPositionSeekbar = (SeekBar) mFragmentView.findViewById(R.id.seekbar_music_position);
        mAdView = (AdView) mFragmentView.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("254A95527DBC0D8C10F9CC87F9B7FE47").build();
        mAdView.loadAd(adRequest);
        mBottomSheetDialog = new BottomSheetDialog(mContext);
        mBottomSheetLayoutView = getActivity().getLayoutInflater().inflate(R.layout.dialog_music_queue, null);
        mQueueRecyclerView = (RecyclerView) mBottomSheetLayoutView.findViewById(R.id.music_queue_recyclerview);
        mQueueRecyclerView.setHasFixedSize(true);
        mBottomSheetDialog.setContentView(mBottomSheetLayoutView);
        mSelectedMediaId = "";
        mQueueItems = new ArrayList<>();
        mIsPlayerPlaying = false;
        TypedValue typedValue = new TypedValue();
        mContext.getTheme().resolveAttribute(R.attr.colorPrimary, typedValue, true);
        mQueueAdapter = new QueueAdapter(mContext, mQueueItems, mSelectedMediaId, this, this, typedValue.data, mIsPlayerPlaying);
        mQueueLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
        int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
        ItemTouchHelper.Callback callback = new MediaItemTouchHelperSimpleCallback(dragFlags, swipeFlags, mQueueAdapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(mQueueRecyclerView);
        mQueueRecyclerView.setLayoutManager(mQueueLinearLayoutManager);
        mQueueRecyclerView.setAdapter(mQueueAdapter);
    }

    @Override
    public void setListeners() {
        Log.i(TAG, "setListeners()");
        //Mini player
        mMiniPlayPauseImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlaybackStateCompat state = mMediaControllerCompat.getPlaybackState();
                if (state != null) {
                    MediaControllerCompat.TransportControls controls =
                            mMediaControllerCompat.getTransportControls();
                    switch (state.getState()) {
                        case PlaybackStateCompat.STATE_PLAYING: // fall through
                        case PlaybackStateCompat.STATE_BUFFERING:
                            controls.pause();
                            break;
                        case PlaybackStateCompat.STATE_PAUSED:
                        case PlaybackStateCompat.STATE_STOPPED:
                            controls.play();
                            break;
                        default:
                            Log.d(TAG, "onClick with state " + state.getState());
                    }
                }
            }
        });

        //Large player
        mShareImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMediaControllerCompat != null) {
                    String source = mMediaControllerCompat.getMetadata().getBundle().getString(CatalogueConstants.GeneralConstants.CUSTOM_METADATA_TRACK_SOURCE);
                    Uri uri = Uri.parse(source);
                    Intent share = new Intent(Intent.ACTION_SEND);
                    share.setType("audio/*");
                    share.putExtra(Intent.EXTRA_STREAM, uri);
                    startActivity(Intent.createChooser(share, ""));
                }
            }
        });
        mAddToPlaylistImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAddToPlaylistView = ((Activity) mContext).getLayoutInflater().inflate(R.layout.dialog_add_to_playlist, null);
                mAddToPlaylistView.findViewById(R.id.create_new_playlist).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mAddToPlaylistAlertDialog.cancel();
                        mNewPlaylistView = ((Activity) mContext).getLayoutInflater().inflate(R.layout.dialog_create_new_playlist, null);
                        mNewPlaylistAlertDialog = new AlertDialog.Builder(mContext)
                                .setView(mNewPlaylistView)
                                .setPositiveButton(R.string.create, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        EditText mPlaylistNameEdittext = (EditText) mNewPlaylistView.findViewById(R.id.playlist_name_edittext);
                                        PlaylistModel mPlaylistDetails = new PlaylistModel();
                                        mPlaylistDetails.setPlaylistName(mPlaylistNameEdittext.getText().toString());
                                        PlaylistUtils.addNewPlaylist(mContext.getContentResolver(), mPlaylistDetails, mSelectedMediaId);
                                        mNewPlaylistAlertDialog.cancel();
                                    }
                                })
                                .create();
                        final TypedValue typedValue = new TypedValue();
                        mContext.getTheme().resolveAttribute(R.attr.colorAccent, typedValue, true);
                        mNewPlaylistAlertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface arg0) {
                                mNewPlaylistAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(typedValue.data);
                            }
                        });
                        mNewPlaylistAlertDialog.show();
                    }
                });
                mPlaylistList = PlaylistUtils.getAllPlaylists(mContext.getContentResolver());
                mAddToPlaylistsRecyclerview = (RecyclerView) mAddToPlaylistView.findViewById(R.id.options_recyclerview);
                mAddToPlaylistAdapter = new AddToPlaylistAdapter(mContext, MediaPlayerFragment.this, mPlaylistList, DIALOG_ADD_TO_PLAYLIST_OPTIONS);
                mAddToPlaylistsLayoutManager = new LinearLayoutManager(mContext);
                mAddToPlaylistsLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                mAddToPlaylistsRecyclerview.setLayoutManager(mAddToPlaylistsLayoutManager);
                mAddToPlaylistsRecyclerview.setAdapter(mAddToPlaylistAdapter);
                mAddToPlaylistAlertDialog = new AlertDialog.Builder(mContext)
                        .setView(mAddToPlaylistView)
                        .create();
                mAddToPlaylistAlertDialog.show();
            }
        });

        mSongPositionSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                ((TextView) mFragmentView.findViewById(R.id.current_song_time)).setText(CustomUtils.getFormattedSongDuration(seekBar.getProgress()));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                stopSeekbarUpdate();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (mMediaControllerCompat != null) {
                    mMediaControllerCompat.getTransportControls().seekTo(seekBar.getProgress());
                    scheduleSeekbarUpdate();
                }
            }
        });

        mPlayPauseImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlaybackStateCompat state = mMediaControllerCompat.getPlaybackState();
                if (state != null) {
                    MediaControllerCompat.TransportControls controls =
                            mMediaControllerCompat.getTransportControls();
                    switch (state.getState()) {
                        case PlaybackStateCompat.STATE_PLAYING: // fall through
                        case PlaybackStateCompat.STATE_BUFFERING:
                            controls.pause();
                            stopSeekbarUpdate();
                            break;
                        case PlaybackStateCompat.STATE_PAUSED:
                        case PlaybackStateCompat.STATE_STOPPED:
                            controls.play();
                            scheduleSeekbarUpdate();
                            break;
                        default:
                            Log.d(TAG, "onClick with state " + state.getState());
                    }
                }
            }
        });
        mSkipToNextImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMediaControllerCompat != null) {
                    MediaControllerCompat.TransportControls controls =
                            mMediaControllerCompat.getTransportControls();
                    controls.skipToNext();
                }
            }
        });

        mSkipToPreviousImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMediaControllerCompat != null) {
                    MediaControllerCompat.TransportControls controls =
                            mMediaControllerCompat.getTransportControls();
                    if (mMediaControllerCompat.getPlaybackState().getPosition() > 10000) {
                        controls.seekTo(0);
                        scheduleSeekbarUpdate();
                    } else {
                        controls.skipToPrevious();
                    }
                }
            }
        });

        mRepeatImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMediaControllerCompat != null) {
                    MediaControllerCompat.TransportControls controls =
                            mMediaControllerCompat.getTransportControls();
                    if (mMediaControllerCompat.getRepeatMode() == PlaybackStateCompat.REPEAT_MODE_NONE) {
                        Log.d(TAG, "Repeat mode none");
                        mRepeatImageview.setSelected(true);
                        controls.setRepeatMode(PlaybackStateCompat.REPEAT_MODE_ALL);
                        PreferenceUtil.setCurrentRepeatPreference(mContext, PlaybackStateCompat.REPEAT_MODE_ALL);
                    } else if (mMediaControllerCompat.getRepeatMode() == PlaybackStateCompat.REPEAT_MODE_ALL) {
                        Log.d(TAG, "Repeat mode all");
                        controls.setRepeatMode(PlaybackStateCompat.REPEAT_MODE_ONE);
                        PreferenceUtil.setCurrentRepeatPreference(mContext, PlaybackStateCompat.REPEAT_MODE_ONE);
                        mRepeatImageview.setImageDrawable(ActivityCompat.getDrawable(mContext, R.drawable.ic_music_repeat_one));
                        mRepeatImageview.setSelected(true);
                    } else {
                        Log.d(TAG, "Repeat mode one");
                        controls.setRepeatMode(PlaybackStateCompat.REPEAT_MODE_NONE);
                        PreferenceUtil.setCurrentRepeatPreference(mContext, PlaybackStateCompat.REPEAT_MODE_NONE);
                        mRepeatImageview.setImageDrawable(ActivityCompat.getDrawable(mContext, R.drawable.ic_music_repeat));
                        mRepeatImageview.setSelected(false);
                    }
                }
            }
        });
        mShowPlayingListImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.show();
            }
        });

        mFavoriteImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMediaControllerCompat != null) {
                    String mediaId = mMediaControllerCompat.getMetadata().getDescription().getMediaId();
                    if (PlaylistUtils.isFavorite(mContentResolver, mediaId)) {
                        PlaylistUtils.deleteFromFavorites(mContentResolver, mediaId);
                        mFavoriteImageview.setImageDrawable(ActivityCompat.getDrawable(mContext, R.drawable.ic_music_favorite_default));
                    } else {
                        PlaylistUtils.addToFavorites(mContentResolver, mediaId);
                        mFavoriteImageview.setImageDrawable(ActivityCompat.getDrawable(mContext, R.drawable.ic_music_favorite));
                    }
                }
            }
        });

        mShuffleImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMediaControllerCompat != null) {
                    MediaControllerCompat.TransportControls controls =
                            mMediaControllerCompat.getTransportControls();
                    if (mMediaControllerCompat.isShuffleModeEnabled()) {
                        controls.setShuffleModeEnabled(false);
                        PreferenceUtil.setCurrentShufflePreference(mContext, false);
                        mShuffleImageview.setSelected(false);
                    } else {
                        controls.setShuffleModeEnabled(true);
                        mShuffleImageview.setSelected(true);
                        PreferenceUtil.setCurrentShufflePreference(mContext, true);
                    }
                }
            }
        });
    }

    @Override
    public void setContextAndTags() {
        TAG = getClass().getSimpleName();
        mContext = getActivity();
    }

    @Override
    public void initialiseOtherComponents() {
        mIsPlayerPlaying = false;
        mQueueItems = new ArrayList<>();
    }

    @Override
    public void onSelectOption(int position, int callbackId) {
        switch (callbackId) {
            case DIALOG_ADD_TO_PLAYLIST_OPTIONS:
                PlaylistUtils.addSongToPlaylist(mContext.getContentResolver(), mSelectedMediaId, mPlaylistList.get(position).getPlaylistId());
                mAddToPlaylistAlertDialog.cancel();
                break;
        }
    }

    //
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        mMediaControlFragmentListener = (LandingActivity) context;
//        mMediaFragmentListener = (LandingActivity) context;
//    }
//
//    @Override
//    public void onStart() {
//        super.onStart();
//        Log.d(TAG, "onStart()");
//        mToken = mMediaControlFragmentListener.getSessionToken();
//        if (mToken != null) {
//            Log.d(TAG, "Token recieved, proceeding to connect to session.");
//            connectToSession(mToken);
//        }
//    }
//
//    private void connectToSession(MediaSessionCompat.Token token) {
//        Log.d(TAG, "connectToSession. Session token: " + token);
//        try {
//            mMediaControllerCompat = new MediaControllerCompat(mContext, token);
//            mMediaControllerCompat.registerCallback(mCallback);
//            mMediaControllerCompat.getTransportControls().setRepeatMode(PreferenceUtil.getCurrentRepeatPreference(mContext));
//            mMediaControllerCompat.getTransportControls().setShuffleModeEnabled(PreferenceUtil.getCurrentShufflePreference(mContext));
//            if (mMediaControllerCompat.getMetadata() != null) {
//                updateDescription(mMediaControllerCompat.getMetadata());
//                mSelectedMediaId = mMediaControllerCompat.getMetadata().getDescription().getMediaId();
//                updateQueue(mMediaControllerCompat.getMetadata());
//                updateDurationOfSeekarc(mMediaControllerCompat.getMetadata());
//            }
//            if (mMediaControllerCompat.getPlaybackState() != null) {
//                updatePlaybackState(mMediaControllerCompat.getPlaybackState());
//            }
//        } catch (RemoteException e) {
//            e.printStackTrace();
//        }
//    }
//
//

    /**
     * Method to update UI based on state of the playback.
     *
     * @param state - Playback state
     */
    private void updatePlaybackState(PlaybackStateCompat state) {
        Log.i(TAG, "updatePlaybackState() " + state);
        switch (state.getState()) {
            case PlaybackStateCompat.STATE_PLAYING:
                mMiniPlayPauseImageview.setImageDrawable(ActivityCompat.getDrawable(mContext, R.drawable.ic_music_pause));
                mPlayPauseImageview.setImageDrawable(ActivityCompat.getDrawable(mContext, R.drawable.ic_music_pause));
                mIsPlayerPlaying = true;
                scheduleSeekbarUpdate();
                break;
            case PlaybackStateCompat.STATE_PAUSED:
                mMiniPlayPauseImageview.setImageDrawable(ActivityCompat.getDrawable(mContext, R.drawable.ic_music_play));
                mPlayPauseImageview.setImageDrawable(ActivityCompat.getDrawable(mContext, R.drawable.ic_music_play));
                mIsPlayerPlaying = false;
                stopSeekbarUpdate();
                break;
            case PlaybackStateCompat.STATE_STOPPED:
                mMiniPlayPauseImageview.setImageDrawable(ActivityCompat.getDrawable(mContext, R.drawable.ic_music_play));
                mPlayPauseImageview.setImageDrawable(ActivityCompat.getDrawable(mContext, R.drawable.ic_music_play));
                mIsPlayerPlaying = false;
                stopSeekbarUpdate();
                break;
            case PlaybackStateCompat.ERROR_CODE_UNKNOWN_ERROR:
                mMiniPlayPauseImageview.setImageDrawable(ActivityCompat.getDrawable(mContext, R.drawable.ic_music_play));
                mPlayPauseImageview.setImageDrawable(ActivityCompat.getDrawable(mContext, R.drawable.ic_music_play));
                stopSeekbarUpdate();
                mIsPlayerPlaying = false;
                break;
        }
        if (mQueueAdapter != null) {
            mQueueAdapter.setmIsPlayerPlaying(mIsPlayerPlaying);
            mQueueAdapter.notifyDataSetChanged();
        }

    }

    /**
     * Set relevant media details on to the UI.
     *
     * @param mediaMetadataCompat - Details of the playing media
     */
    public void updateDescription(MediaMetadataCompat mediaMetadataCompat) {
        Log.i(TAG, "updateDescription() ");
        String songName = mediaMetadataCompat.getBundle().getString(MediaMetadataCompat.METADATA_KEY_TITLE);
        //Mini player
        mMiniCurrentSongNameTextview.setText(songName);
        mMiniCurrentSongNameTextview.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        mMiniCurrentSongNameTextview.setSingleLine(true);
        mMiniCurrentSongNameTextview.setMarqueeRepeatLimit(-1);
        mMiniCurrentSongNameTextview.setSelected(true);
        final TypedValue typedValue = new TypedValue();
        mContext.getTheme().resolveAttribute(R.attr.colorPrimary, typedValue, true);

        //Large player
        if (mMediaControllerCompat.getRepeatMode() == PlaybackStateCompat.REPEAT_MODE_NONE) {
            Log.d(TAG, "Repeat mode none");
            mRepeatImageview.setImageDrawable(ActivityCompat.getDrawable(mContext, R.drawable.ic_music_repeat));
            mRepeatImageview.setSelected(false);
        } else if (mMediaControllerCompat.getRepeatMode() == PlaybackStateCompat.REPEAT_MODE_ALL) {
            Log.d(TAG, "Repeat mode all");
            mRepeatImageview.setImageDrawable(ActivityCompat.getDrawable(mContext, R.drawable.ic_music_repeat));
            mRepeatImageview.setSelected(true);
        } else {
            Log.d(TAG, "Repeat mode one");
            mRepeatImageview.setImageDrawable(ActivityCompat.getDrawable(mContext, R.drawable.ic_music_repeat_one));
            mRepeatImageview.setSelected(true);
        }
        if (mMediaControllerCompat.isShuffleModeEnabled()) {
            mShuffleImageview.setSelected(true);
        } else {
            mShuffleImageview.setSelected(false);
        }
        mMusicBasicInfoTextview.setText(songName);
        mMusicBasicInfoTextview.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        mMusicBasicInfoTextview.setSingleLine(true);
        mMusicBasicInfoTextview.setMarqueeRepeatLimit(-1);
        mMusicBasicInfoTextview.setSelected(true);
        String mediaId = mediaMetadataCompat.getDescription().getMediaId();
        if (PlaylistUtils.isFavorite(mContentResolver, mediaId)) {
            mFavoriteImageview.setImageDrawable(ActivityCompat.getDrawable(mContext, R.drawable.ic_music_favorite));
        } else {
            mFavoriteImageview.setImageDrawable(ActivityCompat.getDrawable(mContext, R.drawable.ic_music_favorite_default));
        }
        Drawable biggerImage = ActivityCompat.getDrawable(mContext, R.drawable.ic_music_now_playing);
        DrawableCompat.wrap(biggerImage).mutate().setTint(typedValue.data);
        Uri uri = ProviderUtil.getAlbumArtworkUri(mediaMetadataCompat.getLong(CatalogueConstants.GeneralConstants.CUSTOM_METADATA_ALBUM_ID));
        Bitmap art = null;
        try {
            art = MediaStore.Images.Media.getBitmap(EncoreApplication.getmContext().getContentResolver(), uri);
            mMediaArtImageView.setImageBitmap(art);
            mMediaArtImageView.setPadding(0, 0, 0, 0);
        } catch (IOException ex) {
            ex.printStackTrace();
            mMediaArtImageView.setImageDrawable(biggerImage);
            mMediaArtImageView.setPadding(24, 24, 24, 24);
        }
        mMediaArtImageView.setClipToOutline(true);
    }

    /**
     * Method to update the queue.
     */
    private void updateQueue() {
        Log.i(TAG, "updateQueue()");
        mQueueItems = mMediaControllerCompat.getQueue();
        mIsPlayerPlaying = true;
        mQueueAdapter.setmIsPlayerPlaying(mIsPlayerPlaying);
        mQueueAdapter.setmQueueItemList(mQueueItems);
        mQueueAdapter.setmSelectedMediaId(mSelectedMediaId);
        mQueueAdapter.notifyDataSetChanged();
    }


    /**
     * Method to update the seekarc.
     *
     * @param metadata - Metadata of the song
     */
    private void updateDurationOfSeekbar(MediaMetadataCompat metadata) {
        Log.i(TAG, "updateDurationOfSeekbar()");
        long duration = metadata.getLong(MediaMetadataCompat.METADATA_KEY_DURATION);
        mSongPositionSeekbar.setMax((int) duration);
        ((TextView) mFragmentView.findViewById(R.id.song_duration)).setText(CustomUtils.getFormattedSongDuration(mMediaControllerCompat.getMetadata().getLong(MediaMetadataCompat.METADATA_KEY_DURATION)));
    }

    private void updateProgress() {
        if (mMediaControllerCompat.getPlaybackState() == null) {
            return;
        }
        long currentPosition = mMediaControllerCompat.getPlaybackState().getPosition();
        if (mMediaControllerCompat.getPlaybackState().getState() != PlaybackStateCompat.STATE_PAUSED) {
            // Calculate the elapsed time between the last position update and now and unless
            // paused, we can assume (delta * speed) + current position is approximately the
            // latest position. This ensure that we do not repeatedly call the getPlaybackState()
            // on MediaController.
            long timeDelta = SystemClock.elapsedRealtime() -
                    mMediaControllerCompat.getPlaybackState().getLastPositionUpdateTime();
            currentPosition += (int) timeDelta * mMediaControllerCompat.getPlaybackState().getPlaybackSpeed();
        }
        mSongPositionSeekbar.setProgress((int) currentPosition);
    }

    private void scheduleSeekbarUpdate() {
        stopSeekbarUpdate();
        if (!mExecutorService.isShutdown()) {
            mScheduleFuture = mExecutorService.scheduleAtFixedRate(
                    new Runnable() {
                        @Override
                        public void run() {
                            mHandler.post(mUpdateProgressTask);
                        }
                    }, PROGRESS_UPDATE_INITIAL_INTERVAL,
                    PROGRESS_UPDATE_INTERNAL, TimeUnit.MILLISECONDS);
        }
    }

    private void stopSeekbarUpdate() {
        if (mScheduleFuture != null) {
            mScheduleFuture.cancel(false);
        }
    }

    public void setOffsetOfCustomFrameLayout(float offset) {
        mCustomFrameLayout.setmContainerLayoutHeight(mMediaArtCoordinatorLayout.getHeight());
        int height = (int) (offset * mMediaArtCoordinatorLayout.getHeight());
        ViewGroup.LayoutParams frameLayoutParams = mCustomFrameLayout.getLayoutParams();
        frameLayoutParams.height = height;
        mCustomFrameLayout.setLayoutParams(frameLayoutParams);
        mCustomFrameLayout.setmSlidingOffset(offset);
    }

    @Override
    public void onAttach(Context context) {
        Log.i(TAG, "onAttach()");
        super.onAttach(context);
        mMediaBrowserFragmentListener = (MediaBrowserFragmentListener) context;
        mMediaControllerCompat = mMediaBrowserFragmentListener.getmMediaControllerCompat();
        mMediaControllerCompat.registerCallback(mMediaControllerCallback);
    }

    @Override
    public void onStop() {
        Log.i(TAG, "onStop()");
        super.onStop();
    }

    @Override
    public void onPause() {
        Log.i(TAG, "onPause()");
        super.onPause();
    }
}
