package com.alacritystudios.encore.fragments;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.alacritystudios.encore.R;
import com.alacritystudios.encore.adapters.SleepTimerNumberPadAdapter;
import com.alacritystudios.encore.interfaces.SleepTimerCallback;
import com.alacritystudios.encore.utils.AppStartUtil;

import java.sql.Time;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Shows the sleep timer dialog box
 */

public class SleepTimerFragment extends AppCompatDialogFragment {

    private String TAG = getClass().getSimpleName();
    private Button mPositiveButton;
    private Button mNegativeButton;
    private Button mResetButton;
    private SeekBar mTimerSeekbar;
    private TextView mMinutesTextView;
    private TextView mSecondsTextView;
    private TypedValue mPrimaryColorTypedValue;
    private SleepTimerCallback mSleepTimerCallback;
    private int mCountdownInterval = 1000;
    private int mDuration;


    public static SleepTimerFragment newInstance(SleepTimerCallback sleepTimerCallback) {
        SleepTimerFragment sleepTimerFragment = new SleepTimerFragment();
        sleepTimerFragment.setmSleepTimerCallback(sleepTimerCallback);
        return sleepTimerFragment;
    }

    public void setmSleepTimerCallback(SleepTimerCallback mSleepTimerCallback) {
        this.mSleepTimerCallback = mSleepTimerCallback;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTheme(AppStartUtil.getCurrentColorSchemeTheme(getActivity(), false));
        mPrimaryColorTypedValue = new TypedValue();
        getActivity().getTheme().resolveAttribute(R.attr.colorPrimary, mPrimaryColorTypedValue, true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_sleep_timer, container, false);
        mTimerSeekbar = (SeekBar) view.findViewById(R.id.select_timer_seekbar);
        mMinutesTextView = (TextView) view.findViewById(R.id.minutes_number_picker);
        mSecondsTextView = (TextView) view.findViewById(R.id.seconds_number_picker);
        mPositiveButton = (Button) view.findViewById(R.id.start_button);
        mResetButton = (Button) view.findViewById(R.id.reset_button);
        mNegativeButton = (Button) view.findViewById(R.id.cancel_button);
        initialiseUI();
        setListeners();
        return view;
    }

    private void setListeners() {
        Log.i(TAG, "setListeners()");
        mTimerSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mMinutesTextView.setText(String.format("%02d", progress));
                mSecondsTextView.setText(String.format("%02d", 0));
                mDuration = progress * 60000;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mPositiveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CountDownTimer countDownTimer = mSleepTimerCallback.fetchCountDownTimer();
                if(countDownTimer == null) {
                    countDownTimer = new CountDownTimer(mDuration, mCountdownInterval) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            mMinutesTextView.setText(String.format("%02d", TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));
                            mSecondsTextView.setText(String.format("%02d", millisUntilFinished % 60000));
                        }

                        @Override
                        public void onFinish() {
                            mSleepTimerCallback.onCountDownFinish();
                            mResetButton.performClick();
                        }
                    };
                    mSleepTimerCallback.setCountDownTimer(countDownTimer);
                    mSleepTimerCallback.fetchCountDownTimer().start();
                    mTimerSeekbar.setVisibility(View.GONE);
                    mPositiveButton.setVisibility(View.GONE);
                }
            }
        });

        mResetButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mSleepTimerCallback.fetchCountDownTimer().cancel();
                mSleepTimerCallback.setCountDownTimer(null);
                mTimerSeekbar.setProgress(0);
                mTimerSeekbar.setVisibility(View.VISIBLE);
                mPositiveButton.setVisibility(View.VISIBLE);
            }
        });


        mNegativeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getDialog().cancel();
            }
        });
    }

    private void initialiseUI()
    {
        Log.i(TAG, "initialiseUI()");
        if (mSleepTimerCallback.fetchCountDownTimer() == null)
        {
            mPositiveButton.setVisibility(View.VISIBLE);
            mTimerSeekbar.setVisibility(View.VISIBLE);
        } else {
            mPositiveButton.setVisibility(View.GONE);
            mTimerSeekbar.setVisibility(View.GONE);
        }
    }
}
