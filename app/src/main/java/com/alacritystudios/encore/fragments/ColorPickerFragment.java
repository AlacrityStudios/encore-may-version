package com.alacritystudios.encore.fragments;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.preference.DialogPreference;
import android.support.v7.preference.PreferenceDialogFragmentCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.alacritystudios.encore.R;
import com.alacritystudios.encore.adapters.ColorPickerAdapter;
import com.alacritystudios.encore.interfaces.ColorPickerCallback;
import com.alacritystudios.encore.preferences.ColorSchemePickerPreference;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment to pick color scheme.
 */

public class ColorPickerFragment extends PreferenceDialogFragmentCompat implements ColorPickerCallback {

    private RecyclerView mColorPickerRecyclerView;
    private ColorPickerAdapter mColorPickerAdapter;
    private GridLayoutManager mGridLayoutManager;
    private List<Integer> mColorList;
    private int mSelectedColor;

    public static ColorPickerFragment newInstance(String key) {
        final ColorPickerFragment fragment = new ColorPickerFragment();
        final Bundle b = new Bundle(1);
        b.putString(ARG_KEY, key);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onColorSelected(int selectedColor) {
        mSelectedColor = selectedColor;
        mColorPickerAdapter.setmSelectedColor(selectedColor);
        mColorPickerAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);
        if (PreferenceManager.getDefaultSharedPreferences(getActivity()).getInt("pref_color_scheme", 0) != 0) {
            mSelectedColor = PreferenceManager.getDefaultSharedPreferences(getActivity()).getInt("pref_color_scheme", 0);
        } else {
            mSelectedColor = R.color.pink_color_primary;
        }
        mColorPickerRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_color_picker);
        mGridLayoutManager = new GridLayoutManager(getActivity(), 4);
        mColorList = new ArrayList<>();
        mColorList.add(ActivityCompat.getColor(getActivity(), R.color.pink_color_primary));
        mColorList.add(ActivityCompat.getColor(getActivity(), R.color.red_color_primary));
        mColorList.add(ActivityCompat.getColor(getActivity(), R.color.light_blue_color_primary));
        mColorList.add(ActivityCompat.getColor(getActivity(), R.color.deep_purple_color_primary));
        mColorList.add(ActivityCompat.getColor(getActivity(), R.color.indigo_color_primary));
        mColorList.add(ActivityCompat.getColor(getActivity(), R.color.blue_color_primary));
        mColorList.add(ActivityCompat.getColor(getActivity(), R.color.teal_color_primary));
        mColorList.add(ActivityCompat.getColor(getActivity(), R.color.light_green_color_primary));
        mColorList.add(ActivityCompat.getColor(getActivity(), R.color.orange_color_primary));
        mColorList.add(ActivityCompat.getColor(getActivity(), R.color.deep_orange_color_primary));
        mColorList.add(ActivityCompat.getColor(getActivity(), R.color.brown_color_primary));
        mColorList.add(ActivityCompat.getColor(getActivity(), R.color.black_color_primary));
        mColorPickerAdapter = new ColorPickerAdapter(mColorList, getActivity(), mSelectedColor, this);
        mColorPickerRecyclerView.setLayoutManager(mGridLayoutManager);
        mColorPickerRecyclerView.setAdapter(mColorPickerAdapter);
    }

    @Override
    public void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            DialogPreference dialogPreference = getPreference();
            if (dialogPreference instanceof ColorSchemePickerPreference) {
                ColorSchemePickerPreference colorSchemePickerPreference = ((ColorSchemePickerPreference) dialogPreference);
                if (colorSchemePickerPreference.callChangeListener(mSelectedColor)) {
                    colorSchemePickerPreference.setmColorScheme(mSelectedColor);
                }
            }
        }
    }
}
