package com.alacritystudios.encore.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.alacritystudios.encore.R;
import com.alacritystudios.encore.models.DrawerModel;

import java.util.List;

/**
 * Created by Anuj Dutt on 23-02-2017.
 */

public class DrawerAdapter extends ArrayAdapter {

    private Context mContext = null;
    private int mResource;
    private List<DrawerModel> mDrawerModelList;
    private int mSelectedPosition;

    public DrawerAdapter(Context mContext, int mResource, List<DrawerModel> mDrawerModelList) {
        super(mContext, mResource, mDrawerModelList);
        this.mContext = mContext;
        this.mResource = mResource;
        this.mDrawerModelList = mDrawerModelList;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            Drawable image = ActivityCompat.getDrawable(mContext, mDrawerModelList.get(position).getDrawableResourceId());
            convertView = inflater.inflate(R.layout.recycler_view_drawer_item, parent, false);
            TextView itemNameTextView = ((TextView) convertView.findViewById(R.id.drawer_item_text));
            ImageView itemImageView = ((ImageView) convertView.findViewById(R.id.drawer_item_image));
            itemNameTextView.setText(mDrawerModelList.get(position).getName());
            itemImageView.setImageDrawable(image);
            if (position == mSelectedPosition) {
                itemNameTextView.setSelected(true);
                itemImageView.setSelected(true);
            } else {
                itemNameTextView.setSelected(false);
                itemImageView.setSelected(false);
            }
        } else {
            Drawable image = ActivityCompat.getDrawable(mContext, mDrawerModelList.get(position).getDrawableResourceId());
            TextView itemNameTextView = ((TextView) convertView.findViewById(R.id.drawer_item_text));
            ImageView itemImageView = ((ImageView) convertView.findViewById(R.id.drawer_item_image));
            itemNameTextView.setText(mDrawerModelList.get(position).getName());
            itemImageView.setImageDrawable(image);
            if (position == mSelectedPosition) {
                itemNameTextView.setSelected(true);
                itemImageView.setSelected(true);
            } else {
                itemNameTextView.setSelected(false);
                itemImageView.setSelected(false);
            }
        }
        return convertView;
    }

    public void setSelectedPosition(int position) {
        this.mSelectedPosition = position;
    }

    @Override
    public int getCount() {
        if (mDrawerModelList != null) {
            return mDrawerModelList.size();
        } else {
            return 0;
        }
    }
}
