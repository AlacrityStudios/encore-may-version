package com.alacritystudios.encore.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alacritystudios.encore.R;
import com.alacritystudios.encore.enums.MediaBrowserItemState;
import com.alacritystudios.encore.interfaces.MediaBrowserFragmentListener;
import com.alacritystudios.encore.interfaces.OptionsTriggerListener;
import com.alacritystudios.encore.utils.AnimationUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

/**
 * Adapter to render the media items.
 */

public class MediaItemAdapter extends RecyclerView.Adapter<MediaItemAdapter.MediaItemViewHolder> {

    private Context mContext;
    private int mCount;
    private List<MediaBrowserCompat.MediaItem> mMediaItemList;
    private MediaBrowserFragmentListener mMediaBrowserFragmentListener;
    private OptionsTriggerListener mOptionsTriggerListener;
    private Drawable mPlaceholderDrawable;
    private MediaBrowserItemState mMediaBrowserItemState;

    public MediaItemAdapter(Context mContext, List<MediaBrowserCompat.MediaItem> mMediaItemList, MediaBrowserFragmentListener mMediaBrowserFragmentListener,
                            OptionsTriggerListener optionsTriggerListener, Drawable placeholderDrawable, MediaBrowserItemState mediaBrowserItemState) {
        this.mContext = mContext;
        if (mMediaItemList != null) {
            this.mCount = mMediaItemList.size();
        } else {
            mCount = 0;
        }

        this.mMediaItemList = mMediaItemList;
        this.mMediaBrowserFragmentListener = mMediaBrowserFragmentListener;
        this.mOptionsTriggerListener = optionsTriggerListener;
        this.mPlaceholderDrawable = placeholderDrawable;
        this.mMediaBrowserItemState = mediaBrowserItemState;
    }

    public void setmMediaItemList(List<MediaBrowserCompat.MediaItem> mMediaItemList) {
        this.mMediaItemList = mMediaItemList;
        if (mMediaItemList != null) {
            this.mCount = mMediaItemList.size();
        } else {
            mCount = 0;
        }
    }

    public void setmMediaBrowserItemState(MediaBrowserItemState mMediaBrowserItemState) {
        this.mMediaBrowserItemState = mMediaBrowserItemState;
    }

    public void setmMediaBrowserFragmentListener(MediaBrowserFragmentListener mMediaBrowserFragmentListener) {
        this.mMediaBrowserFragmentListener = mMediaBrowserFragmentListener;
    }

    public class MediaItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mMyMusicTitle;
        private TextView mMyMusicSubtitle;
        private ImageView mMyMusicAlbumArt;
        private ImageView mMyMusicOptions;
        private LinearLayout mClickListenerLayout;
        private View mRootLayout;
        private Animation mAnimation;

        public MediaItemViewHolder(View itemView) {
            super(itemView);
            mRootLayout = itemView;
            mMyMusicTitle = (TextView) itemView.findViewById(R.id.my_music_title);
            mMyMusicSubtitle = (TextView) itemView.findViewById(R.id.my_music_subtitle);
            mMyMusicAlbumArt = (ImageView) itemView.findViewById(R.id.my_music_icon);
            mMyMusicOptions = (ImageView) itemView.findViewById(R.id.my_music_options);
            mClickListenerLayout = (LinearLayout) itemView.findViewById(R.id.my_music_click_listener_layout);
            mClickListenerLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mMediaBrowserFragmentListener.onMediaItemClicked(mMediaItemList.get(getAdapterPosition()));
        }

        public void clearAnimation() {
            mRootLayout.clearAnimation();
        }

        private void setScaleAnimation() {
            mRootLayout.setAnimation(mAnimation);
        }
    }


    @Override
    public MediaItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = ((Activity) mContext).getLayoutInflater().inflate(R.layout.recycler_view_media_browser_item, parent, false);
        return new MediaItemViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(final MediaItemViewHolder holder, final int position) {
        switch (mMediaBrowserItemState) {
            case ALL:
                holder.mMyMusicOptions.setVisibility(View.VISIBLE);
                holder.mMyMusicOptions.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mOptionsTriggerListener.clickedMediaId(mMediaItemList.get(position));
                    }
                });
                holder.mMyMusicAlbumArt.setPadding(24, 24, 24, 24);
                Glide.with((Activity) mContext)
                        .load(mMediaItemList.get(position).getDescription().getIconUri())
                        .centerCrop()
                        .placeholder(mPlaceholderDrawable)
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .dontAnimate().listener(new RequestListener<Uri, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, Uri model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, Uri model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        holder.mMyMusicAlbumArt.setPadding(0, 0, 0, 0);
                        return false;
                    }
                }).into(holder.mMyMusicAlbumArt);
                holder.mMyMusicAlbumArt.setClipToOutline(true);
                holder.mAnimation = AnimationUtils.setScaleAnimation(150);
                break;
            case NONE:
                holder.mMyMusicOptions.setVisibility(View.INVISIBLE);
                holder.mMyMusicAlbumArt.setPadding(24, 24, 24, 24);
                holder.mMyMusicAlbumArt.setImageDrawable(mPlaceholderDrawable);
                holder.mMyMusicAlbumArt.setClipToOutline(true);
                holder.mAnimation = null;
                break;
            case ALBUM_ART_ANIMATION:
                holder.mMyMusicOptions.setVisibility(View.INVISIBLE);
                holder.mMyMusicAlbumArt.setPadding(24, 24, 24, 24);
                Glide.with((Activity) mContext)
                        .load(mMediaItemList.get(position).getDescription().getIconUri())
                        .centerCrop()
                        .placeholder(mPlaceholderDrawable)
                        .priority(Priority.LOW)
                        .skipMemoryCache(true)
                        .thumbnail(0.1f)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .dontAnimate().listener(new RequestListener<Uri, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, Uri model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, Uri model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        holder.mMyMusicAlbumArt.setPadding(0, 0, 0, 0);
                        return false;
                    }
                }).into(holder.mMyMusicAlbumArt);
                holder.mMyMusicAlbumArt.setClipToOutline(true);
                holder.mAnimation = AnimationUtils.setScaleAnimation(150);
                break;
            case ALBUM_ART_OPTIONS:
                holder.mMyMusicOptions.setVisibility(View.VISIBLE);
                holder.mMyMusicOptions.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mOptionsTriggerListener.clickedMediaId(mMediaItemList.get(position));
                    }
                });
                holder.mMyMusicAlbumArt.setPadding(24, 24, 24, 24);
                Glide.with((Activity) mContext)
                        .load(mMediaItemList.get(position).getDescription().getIconUri())
                        .centerCrop()
                        .placeholder(mPlaceholderDrawable)
                        .priority(Priority.LOW)
                        .skipMemoryCache(true)
                        .thumbnail(0.1f)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .dontAnimate().listener(new RequestListener<Uri, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, Uri model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, Uri model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        holder.mMyMusicAlbumArt.setPadding(0, 0, 0, 0);
                        return false;
                    }
                }).into(holder.mMyMusicAlbumArt);
                holder.mMyMusicAlbumArt.setClipToOutline(true);
                holder.mAnimation = null;
                break;
            case ANIMATION_OPTIONS:
                holder.mMyMusicOptions.setVisibility(View.VISIBLE);
                holder.mMyMusicOptions.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mOptionsTriggerListener.clickedMediaId(mMediaItemList.get(position));
                    }
                });
                holder.mMyMusicAlbumArt.setPadding(24, 24, 24, 24);
                holder.mMyMusicAlbumArt.setImageDrawable(mPlaceholderDrawable);
                holder.mMyMusicAlbumArt.setClipToOutline(true);
                holder.mAnimation = AnimationUtils.setScaleAnimation(150);
                break;
            case ALBUM_ART_ONLY:
                holder.mMyMusicOptions.setVisibility(View.INVISIBLE);
                holder.mAnimation = null;
                holder.mMyMusicAlbumArt.setPadding(24, 24, 24, 24);
                Glide.with((Activity) mContext)
                        .load(mMediaItemList.get(position).getDescription().getIconUri())
                        .centerCrop()
                        .placeholder(mPlaceholderDrawable)
                        .priority(Priority.LOW)
                        .skipMemoryCache(true)
                        .thumbnail(0.1f)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .dontAnimate().listener(new RequestListener<Uri, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, Uri model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, Uri model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        holder.mMyMusicAlbumArt.setPadding(0, 0, 0, 0);
                        return false;
                    }
                }).into(holder.mMyMusicAlbumArt);
                holder.mMyMusicAlbumArt.setClipToOutline(true);
                break;
            case ANIMATION_ONLY:
                holder.mAnimation = AnimationUtils.setScaleAnimation(150);
                holder.mMyMusicOptions.setVisibility(View.INVISIBLE);
                holder.mMyMusicAlbumArt.setPadding(24, 24, 24, 24);
                holder.mMyMusicAlbumArt.setImageDrawable(mPlaceholderDrawable);
                holder.mMyMusicAlbumArt.setClipToOutline(true);
                break;
            case OPTIONS_ONLY:
                holder.mMyMusicOptions.setVisibility(View.VISIBLE);
                holder.mMyMusicOptions.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mOptionsTriggerListener.clickedMediaId(mMediaItemList.get(position));
                    }
                });
                holder.mAnimation = null;
                holder.mMyMusicAlbumArt.setPadding(24, 24, 24, 24);
                holder.mMyMusicAlbumArt.setImageDrawable(mPlaceholderDrawable);
                holder.mMyMusicAlbumArt.setClipToOutline(true);
                break;
        }
        holder.mMyMusicAlbumArt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOptionsTriggerListener.clickedMediaInfo(mMediaItemList.get(position));
            }
        });

        holder.mMyMusicTitle.setText(mMediaItemList.get(position).getDescription().getTitle());

        holder.mMyMusicSubtitle.setText(mMediaItemList.get(position).getDescription().getSubtitle());
    }

    @Override
    public int getItemCount() {
        return mCount;
    }

    @Override
    public void onViewDetachedFromWindow(MediaItemViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.clearAnimation();
    }

    @Override
    public void onViewAttachedToWindow(MediaItemViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.setScaleAnimation();
    }

}
