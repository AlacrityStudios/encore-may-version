package com.alacritystudios.encore.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alacritystudios.encore.R;
import com.alacritystudios.encore.interfaces.ItemTouchHelperListener;
import com.alacritystudios.encore.interfaces.OptionsTriggerListener;
import com.alacritystudios.encore.interfaces.QueueAdapterListener;
import com.alacritystudios.encore.utils.AnimationUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.Collections;
import java.util.List;

/**
 * Adapter to bind the queue details to the view.
 */

public class QueueAdapter extends RecyclerView.Adapter<QueueAdapter.QueueViewHolder> implements ItemTouchHelperListener {

    private Context mContext;
    private List<MediaSessionCompat.QueueItem> mQueueItemList;
    private String mSelectedMediaId;
    private QueueAdapterListener mQueueAdapterListener;
    private OptionsTriggerListener mOptionsTriggerListener;
    private int mPrimaryColor;
    private boolean mIsPlayerPlaying;

    public QueueAdapter(Context mContext, List<MediaSessionCompat.QueueItem> mQueueItemList, String mSelectedMediaId, QueueAdapterListener mQueueAdapterListener, OptionsTriggerListener optionsTriggerListener, int mPrimaryColor, boolean isPlayerPlaying) {
        this.mContext = mContext;
        this.mQueueItemList = mQueueItemList;
        this.mSelectedMediaId = mSelectedMediaId;
        this.mQueueAdapterListener = mQueueAdapterListener;
        this.mOptionsTriggerListener = optionsTriggerListener;
        this.mPrimaryColor = mPrimaryColor;
        this.mIsPlayerPlaying = isPlayerPlaying;
    }

    public void setmQueueItemList(List<MediaSessionCompat.QueueItem> mQueueItemList) {
        this.mQueueItemList = mQueueItemList;
    }

    public void setmIsPlayerPlaying(boolean mIsPlayerPlaying) {
        this.mIsPlayerPlaying = mIsPlayerPlaying;
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(mQueueItemList, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(mQueueItemList, i, i - 1);
            }
        }
        mQueueAdapterListener.onQueueItemMoved(fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public void onItemDismiss(int position) {
        mQueueAdapterListener.onQueueItemRemoved(mQueueItemList.get(position).getDescription());
        mQueueItemList.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public QueueViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new QueueViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_view_media_browser_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final QueueViewHolder holder, final int position) {
        if (mSelectedMediaId.equals(mQueueItemList.get(position).getDescription().getMediaId())) {
            holder.mIsCurrentlyPlayingItem = true;
            holder.mQueueItemOptions.setImageResource(R.drawable.ic_equalizer_animated);
            Drawable animation = holder.mQueueItemOptions.getDrawable();
            if (animation instanceof Animatable && holder.mIsCurrentlyPlayingItem) {
                if(mIsPlayerPlaying){
                    ((Animatable) animation).start();
                } else {
                    ((Animatable) animation).stop();
                }
            }
            holder.mQueueItemOptions.setVisibility(View.VISIBLE);
        } else {
            holder.mQueueItemOptions.setVisibility(View.INVISIBLE);
        }
        holder.mQueueItemOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOptionsTriggerListener.clickedQueueId(mQueueItemList.get(position));
            }
        });
        Drawable placeholderImage = ActivityCompat.getDrawable(mContext, R.drawable.ic_music_now_playing);
        DrawableCompat.wrap(placeholderImage).mutate().setTint(mPrimaryColor);
        holder.mQueueItemAlbumArt.setPadding(24, 24, 24, 24);
        Glide.with((Activity) mContext)
                .load(mQueueItemList.get(position).getDescription().getIconUri())
                .centerCrop()
                .placeholder(placeholderImage)
                .priority(Priority.LOW)
                .skipMemoryCache(true)
                .thumbnail(0.1f)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .dontAnimate().listener(new RequestListener<Uri, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, Uri model, Target<GlideDrawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, Uri model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                holder.mQueueItemAlbumArt.setPadding(0, 0, 0, 0);
                return false;
            }
        }).into(holder.mQueueItemAlbumArt);
        holder.mQueueItemAlbumArt.setClipToOutline(true);

        holder.mQueueItemTitle.setText(mQueueItemList.get(position).getDescription().getTitle());
        holder.mQueueItemTitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        holder.mQueueItemTitle.setSingleLine(true);

        holder.mQueueItemSubtitle.setText(mQueueItemList.get(position).getDescription().getSubtitle());
        holder.mQueueItemSubtitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        holder.mQueueItemSubtitle.setSingleLine(true);
    }

    @Override
    public int getItemCount() {
        if (mQueueItemList != null) {
            return mQueueItemList.size();
        } else {
            return 0;
        }
    }

    public void setmSelectedMediaId(String mSelectedMediaId) {
        this.mSelectedMediaId = mSelectedMediaId;
    }

    @Override
    public void onViewRecycled(QueueViewHolder holder) {
        super.onViewRecycled(holder);
        Glide.clear(holder.mQueueItemAlbumArt);
    }

    @Override
    public void onViewDetachedFromWindow(QueueViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.clearAnimation();
    }

    @Override
    public void onViewAttachedToWindow(QueueViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.setScaleAnimation();
    }

    public class QueueViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mQueueItemTitle;
        private TextView mQueueItemSubtitle;
        private ImageView mQueueItemAlbumArt;
        private ImageView mQueueItemOptions;
        private LinearLayout mClickListenerLayout;
        private View mRootLayout;
        private boolean mIsCurrentlyPlayingItem;

        public QueueViewHolder(View itemView) {
            super(itemView);
            mRootLayout = itemView;
            mQueueItemTitle = (TextView) itemView.findViewById(R.id.my_music_title);
            mQueueItemSubtitle = (TextView) itemView.findViewById(R.id.my_music_subtitle);
            mQueueItemAlbumArt = (ImageView) itemView.findViewById(R.id.my_music_icon);
            mQueueItemOptions = (ImageView) itemView.findViewById(R.id.my_music_options);
            mClickListenerLayout = (LinearLayout) itemView.findViewById(R.id.my_music_click_listener_layout);
            mIsCurrentlyPlayingItem = false;
            mClickListenerLayout.setOnClickListener(this);
        }

        public boolean ismIsCurrentlyPlayingItem() {
            return mIsCurrentlyPlayingItem;
        }

        public void clearAnimation() {
            mRootLayout.clearAnimation();
        }

        private void setScaleAnimation() {
            ScaleAnimation anim = AnimationUtils.setScaleAnimation(300);
            mRootLayout.setAnimation(anim);
        }

        @Override
        public void onClick(View v) {
            mQueueAdapterListener.onQueueItemClicked(getAdapterPosition());
        }
    }
}
