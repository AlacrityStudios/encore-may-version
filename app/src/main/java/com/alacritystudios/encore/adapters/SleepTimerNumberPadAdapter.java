package com.alacritystudios.encore.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alacritystudios.encore.R;
import com.alacritystudios.encore.interfaces.SleepTimerCallback;

import java.util.List;

/**
 * Adapter to render the number pad shown on sleep timer dialog fragment.
 */

public class SleepTimerNumberPadAdapter extends RecyclerView.Adapter<SleepTimerNumberPadAdapter.NumberPadViewHolder> {

    public List<String> mNumberList;
    public Context mContext;
    public SleepTimerCallback mSleepTimerCallback;

    public SleepTimerNumberPadAdapter(List<String> mNumberList, Context mContext, SleepTimerCallback mSleepTimerCallback) {
        this.mNumberList = mNumberList;
        this.mContext = mContext;
        this.mSleepTimerCallback = mSleepTimerCallback;
    }

    @Override
    public NumberPadViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NumberPadViewHolder(((Activity) mContext).getLayoutInflater().inflate(R.layout.recycler_view_number_pad_item, parent, false));
    }

    @Override
    public void onBindViewHolder(NumberPadViewHolder holder, final int position) {
        holder.mNumberTextView.setText(mNumberList.get(position));
        holder.mNumberTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // mSleepTimerCallback.onKeyPress(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mNumberList != null) {
            return mNumberList.size();
        } else {
            return 0;
        }
    }

    public class NumberPadViewHolder extends RecyclerView.ViewHolder {
        private TextView mNumberTextView;

        public NumberPadViewHolder(View itemView) {
            super(itemView);
            mNumberTextView = (TextView) itemView.findViewById(R.id.number_pad_number_textview);
        }
    }
}
