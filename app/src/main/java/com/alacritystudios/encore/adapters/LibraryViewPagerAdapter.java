package com.alacritystudios.encore.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.alacritystudios.encore.R;
import com.alacritystudios.encore.application.EncoreApplication;
import com.alacritystudios.encore.fragments.ContainerFragment;
import com.alacritystudios.encore.utils.MediaIdUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anuj on 2/4/17.
 */

public class LibraryViewPagerAdapter extends FragmentStatePagerAdapter {
    private String TAG;
    private List<Fragment> mFragmentList;
    private List<String> mTitle;

    public LibraryViewPagerAdapter(FragmentManager fm) {
        super(fm);
        TAG = getClass().getSimpleName();
        mFragmentList = new ArrayList<>();
        mTitle = new ArrayList<>();
    }

    @Override
    public Fragment getItem(int position) {
        Log.i(TAG, "getItem : " + String.valueOf(position));
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(Fragment fragment, String title) {
        Log.i(TAG, "addFragment");
        mFragmentList.add(fragment);
        mTitle.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitle.get(position);
    }
}
