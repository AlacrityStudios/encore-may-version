package com.alacritystudios.encore.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alacritystudios.encore.R;
import com.alacritystudios.encore.interfaces.OptionsSelectedListener;

import java.util.List;

/**
 * Creates the options list
 */

public class OptionsAdapter extends RecyclerView.Adapter<OptionsAdapter.OptionsViewHolder> {

    private List<String> mOptionsList;
    private Context mContext;
    private OptionsSelectedListener mOptionsSelectedListener;
    private final int mCallbackId;

    public OptionsAdapter(@Nullable List<String> mOptionsList, Context mContext, OptionsSelectedListener mOptionsSelectedListener, int mCallbackId) {
        this.mOptionsList = mOptionsList;
        this.mContext = mContext;
        this.mOptionsSelectedListener = mOptionsSelectedListener;
        this.mCallbackId = mCallbackId;
    }

    @Override
    public OptionsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new OptionsViewHolder(((Activity) mContext).getLayoutInflater().inflate(R.layout.recycler_view_options_item, parent, false));
    }

    @Override
    public void onBindViewHolder(OptionsViewHolder holder, int position) {
        holder.mOptionNameTextview.setText(mOptionsList.get(position));
    }

    @Override
    public int getItemCount() {
        if (mOptionsList != null) {
            return mOptionsList.size();
        } else {
            return 0;
        }
    }

    public class OptionsViewHolder extends RecyclerView.ViewHolder {

        private TextView mOptionNameTextview;

        public OptionsViewHolder(View itemView) {
            super(itemView);
            mOptionNameTextview = (TextView) itemView.findViewById(R.id.text1);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOptionsSelectedListener.onSelectOption(getAdapterPosition(), mCallbackId);
                }
            });
        }
    }
}
