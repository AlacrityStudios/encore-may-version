package com.alacritystudios.encore.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.alacritystudios.encore.R;
import com.alacritystudios.encore.interfaces.ColorPickerCallback;

import java.util.List;

/**
 * Adapter to display all color schemes.
 */

public class ColorPickerAdapter extends RecyclerView.Adapter<ColorPickerAdapter.ColorPickerViewHolder> {

    private List<Integer> colorArrayList;
    private Context mContext;
    private int mSelectedColor;
    private ColorPickerCallback mColorPickerCallback;

    public ColorPickerAdapter(List<Integer> colorArrayList, Context mContext, int mSelectedColor, ColorPickerCallback mColorPickerCallback) {
        this.colorArrayList = colorArrayList;
        this.mContext = mContext;
        this.mSelectedColor = mSelectedColor;
        this.mColorPickerCallback = mColorPickerCallback;
    }

    public class ColorPickerViewHolder extends RecyclerView.ViewHolder {

        private ImageView mColorPickerImageview;

        public ColorPickerViewHolder(View itemView) {
            super(itemView);
            mColorPickerImageview = (ImageView) itemView.findViewById(R.id.color_picker_imageview);

        }
    }

    @Override
    public ColorPickerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = ((Activity) mContext).getLayoutInflater().inflate(R.layout.recycler_view_color_picker_item, parent, false);
        return new ColorPickerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ColorPickerViewHolder holder, int position) {
        final int newColor = colorArrayList.get(position);

        if (mSelectedColor == newColor) {
            Drawable image = ActivityCompat.getDrawable(mContext, R.drawable.color_picker_selected_background);
            DrawableCompat.setTint(image, newColor);
            holder.mColorPickerImageview.setBackground(image);
        } else {
            Drawable image = ActivityCompat.getDrawable(mContext, R.drawable.color_picker_default_background);
            DrawableCompat.setTint(image, newColor);
            holder.mColorPickerImageview.setBackground(image);
        }
        holder.mColorPickerImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mColorPickerCallback.onColorSelected(newColor);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (colorArrayList != null) {
            return colorArrayList.size();
        } else {
            return 0;
        }
    }

    public void setmSelectedColor(int mSelectedColor) {
        this.mSelectedColor = mSelectedColor;
    }
}
