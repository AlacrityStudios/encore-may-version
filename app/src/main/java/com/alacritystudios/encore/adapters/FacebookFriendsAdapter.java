package com.alacritystudios.encore.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.alacritystudios.encore.R;
import com.alacritystudios.encore.models.FacebookFriendModel;
import com.alacritystudios.encore.models.FirebaseUserGeneralDetails;
import com.alacritystudios.encore.models.FirebaseUserLibraryDetails;
import com.alacritystudios.encore.models.FirebaseUserModel;
import com.alacritystudios.encore.rest.ApiClient;
import com.alacritystudios.encore.rest.ApiInterface;
import com.alacritystudios.encore.utils.ComponentUtil;
import com.alacritystudios.encore.utils.FirebaseUtil;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.facebook.AccessToken;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.JsonObject;
import com.squareup.okhttp.ResponseBody;

import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Adapter to display facebook friends info
 */

public class FacebookFriendsAdapter extends ArrayAdapter<FacebookFriendModel> {

    private List<FacebookFriendModel> mFriendsList;
    private Context mContext;
    private int mResource;
    private ImageView mRetryImageview;
    private String userName;
    private boolean isLiked;
    private Drawable mPlaceholderDrawable;

    public FacebookFriendsAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<FacebookFriendModel> objects, String userName, Drawable mPlaceholderDrawable) {
        super(context, resource, objects);
        this.mFriendsList = objects;
        this.mContext = context;
        this.mResource = resource;
        this.userName = userName;
        this.isLiked = false;
        this.mPlaceholderDrawable = mPlaceholderDrawable;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(mResource, parent, false);
            final TextView mFriendNameTextView = (TextView) convertView.findViewById(R.id.friend_name);
            final TextView mFriendSongTextView = (TextView) convertView.findViewById(R.id.friend_song_name);
            final ImageView mFriendProfilePicture = (ImageView) convertView.findViewById(R.id.friend_image);
            final ImageView mLikeFriendSong = (ImageView) convertView.findViewById(R.id.friend_like_music);
            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            database.getReference(mFriendsList.get(position).getUserId()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    final FirebaseUserModel firebaseUserModel = dataSnapshot.getValue(FirebaseUserModel.class);
                    if(firebaseUserModel != null)
                    {
                        mFriendNameTextView.setText(firebaseUserModel.getUserGeneralDetails().getUserName());
                        String uriString = firebaseUserModel.getUserGeneralDetails().getUserPhotoUrl();
                        Uri uri;
                        if (uriString != null) {
                            uri = Uri.parse(uriString);
                        } else {
                            uri = Uri.EMPTY;
                        }
                        Glide.with(mContext)
                                .load(uri)
                                .centerCrop()
                                .placeholder(mPlaceholderDrawable)
                                .dontAnimate()
                                .thumbnail(0.1f)
                                .skipMemoryCache(true)
                                .listener(new RequestListener<Uri, GlideDrawable>() {
                                    @Override
                                    public boolean onException(Exception e, Uri model, Target<GlideDrawable> target, boolean isFirstResource) {
                                        mFriendProfilePicture.setPadding(24, 24, 24, 24);
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(GlideDrawable resource, Uri model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                        mFriendProfilePicture.setPadding(0, 0, 0, 0);
                                        return false;
                                    }
                                })
                                .into(mFriendProfilePicture);

                        mFriendProfilePicture.setClipToOutline(true);

                        isLiked = false;
                        ColorStateList colours = ActivityCompat.getColorStateList(mContext, R.color.friend_likes_button_color_state);
                        Drawable drawable = ActivityCompat.getDrawable(mContext, R.drawable.ic_music_favorite);
                        DrawableCompat.wrap(drawable).mutate().setTintList(colours);
                        mLikeFriendSong.setImageDrawable(drawable);
                        if (firebaseUserModel.getUserLibraryDetails().isOnlineStatus()) {
                            mFriendNameTextView.setTextColor(ActivityCompat.getColor(mContext, R.color.colorPrimaryText));
                            if (firebaseUserModel.getUserLibraryDetails().isListening()) {
                                mLikeFriendSong.setVisibility(View.INVISIBLE);
                                mFriendSongTextView.setText(firebaseUserModel.getUserLibraryDetails().getCurrentSong());
                                mLikeFriendSong.setSelected(true);
                                mLikeFriendSong.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        //sendLike(mLikeFriendSong, mFriendsList.get(position).getUserId(), database, firebaseUserModel.getUserLibraryDetails().getCurrentSong());
                                    }
                                });
                            } else {
                                mLikeFriendSong.setVisibility(View.INVISIBLE);
                                mFriendSongTextView.setText(mContext.getString(R.string.currently_not_listening_to_anything));
                                mLikeFriendSong.setSelected(false);
                            }

                        } else {
                            mFriendNameTextView.setTextColor(ActivityCompat.getColor(mContext, R.color.colorDefault));
                            mLikeFriendSong.setVisibility(View.INVISIBLE);
                            mFriendSongTextView.setText(mContext.getString(R.string.currently_offline));
                        }
                        mFriendSongTextView.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                        mFriendSongTextView.setSingleLine(true);
                        mFriendSongTextView.setMarqueeRepeatLimit(-1);
                        mFriendSongTextView.setSelected(true);
                        mLikeFriendSong.setClickable(true);
                        mFriendProfilePicture.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showFriendDetails(position, database, firebaseUserModel.getUserLibraryDetails(), firebaseUserModel.getUserGeneralDetails());
                            }
                        });
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });



//            database.getReference(FirebaseUtil.USER_GENERAL_DETAILS).child(mFriendsList.get(position).getUserId()).child(FirebaseUtil.USER_GENERAL_DETAILS_PHOTO_URL).addValueEventListener(new ValueEventListener() {
//                @Override
//                public void onDataChange(DataSnapshot dataSnapshot) {
//                    Log.d("TAG", "On data change");
//                    String uriString = dataSnapshot.getValue(String.class);
//                    Uri uri;
//                    if (uriString != null) {
//                        uri = Uri.parse(uriString);
//                    } else {
//                        uri = Uri.EMPTY;
//                    }
//                    Glide.with(mContext)
//                            .load(uri)
//                            .centerCrop()
//                            .placeholder(mPlaceholderDrawable)
//                            .dontAnimate()
//                            .thumbnail(0.1f)
//                            .skipMemoryCache(true)
//                            .listener(new RequestListener<Uri, GlideDrawable>() {
//                                @Override
//                                public boolean onException(Exception e, Uri model, Target<GlideDrawable> target, boolean isFirstResource) {
//                                    mFriendProfilePicture.setPadding(24, 24, 24, 24);
//                                    return false;
//                                }
//
//                                @Override
//                                public boolean onResourceReady(GlideDrawable resource, Uri model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                                    mFriendProfilePicture.setPadding(0, 0, 0, 0);
//                                    return false;
//                                }
//                            })
//                            .into(mFriendProfilePicture);
//
//                    mFriendProfilePicture.setClipToOutline(true);
//                }
//
//                @Override
//                public void onCancelled(DatabaseError databaseError) {
//                    Log.d("TAG", "onCancelled");
//                }
//            });
//
//            database.getReference(FirebaseUtil.USER_LIBRARY_DETAILS).child(mFriendsList.get(position).getUserId()).addValueEventListener(new ValueEventListener() {
//                @Override
//                public void onDataChange(DataSnapshot dataSnapshot) {
//                    final FirebaseUserLibraryDetails firebaseUserLibraryDetails = dataSnapshot.getValue(FirebaseUserLibraryDetails.class);
//                    isLiked = false;
//                    ColorStateList colours = ActivityCompat.getColorStateList(mContext, R.color.friend_likes_button_color_state);
//                    Drawable drawable = ActivityCompat.getDrawable(mContext, R.drawable.ic_music_favorite);
//                    DrawableCompat.wrap(drawable).mutate().setTintList(colours);
//                    mLikeFriendSong.setImageDrawable(drawable);
//                    if (firebaseUserLibraryDetails.isOnlineStatus()) {
//                        mFriendNameTextView.setTextColor(ActivityCompat.getColor(mContext, R.color.colorPrimaryText));
//                        if (firebaseUserLibraryDetails.isListening()) {
//                            mLikeFriendSong.setVisibility(View.VISIBLE);
//                            mFriendSongTextView.setText(firebaseUserLibraryDetails.getCurrentSong());
//                            mLikeFriendSong.setSelected(true);
//                            mLikeFriendSong.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    sendLike(mLikeFriendSong, mFriendsList.get(position).getUserId(), database, firebaseUserLibraryDetails.getCurrentSong());
//                                }
//                            });
//                        } else {
//                            mLikeFriendSong.setVisibility(View.VISIBLE);
//                            mFriendSongTextView.setText(mContext.getString(R.string.currently_not_listening_to_anything));
//                            mLikeFriendSong.setSelected(false);
//                        }
//
//                    } else {
//                        mFriendNameTextView.setTextColor(ActivityCompat.getColor(mContext, R.color.colorDefault));
//                        mLikeFriendSong.setVisibility(View.GONE);
//                        mFriendSongTextView.setText(mContext.getString(R.string.currently_offline));
//                    }
//                    mFriendSongTextView.setEllipsize(TextUtils.TruncateAt.MARQUEE);
//                    mFriendSongTextView.setSingleLine(true);
//                    mFriendSongTextView.setMarqueeRepeatLimit(-1);
//                    mFriendSongTextView.setSelected(true);
//                    mLikeFriendSong.setClickable(true);
//                    mFriendProfilePicture.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            showFriendDetails(position, database, firebaseUserLibraryDetails);
//                        }
//                    });
//                }
//
//                @Override
//                public void onCancelled(DatabaseError databaseError) {
//                    Log.d("TAG", "onCancelled");
//                }
//            });


        }
        return convertView;
    }

    public void showFriendDetails(final int position, FirebaseDatabase database, final FirebaseUserLibraryDetails firebaseUserLibraryDetails, final FirebaseUserGeneralDetails firebaseUserGeneralDetails) {
        View view = ((Activity) mContext).getLayoutInflater().inflate(R.layout.dialog_facebook_friend_details, null);
        final ImageView profilePhoto = (ImageView) view.findViewById(R.id.friend_info_photo);
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        final TextView songNameTextView = (TextView) view.findViewById(R.id.friend_info_current_song);
        final TextView friendNameTextView = (TextView) view.findViewById(R.id.friend_info_name);
        final TextView friendsNumberTextView = (TextView) view.findViewById(R.id.friend_info_friends_on_encore);
        final TextView karmaTextView = (TextView) view.findViewById(R.id.friend_info_karma);
        final TextView songsInLibraryTextView = (TextView) view.findViewById(R.id.friend_info_songs_in_library);
        final TextView songsCounterTextView = (TextView) view.findViewById(R.id.friend_info_song_counter);

        friendNameTextView.setText(firebaseUserGeneralDetails.getUserName());
        friendsNumberTextView.setText(String.valueOf(firebaseUserGeneralDetails.getNoOfFriends()));
        String uriString = firebaseUserGeneralDetails.getUserPhotoUrl();
        Uri uri;
        if (uriString != null) {
            uri = Uri.parse(uriString);
        } else {
            uri = Uri.EMPTY;
        }
        Drawable image = ActivityCompat.getDrawable(mContext, R.drawable.ic_people);
        TypedValue typedValue = new TypedValue();
        mContext.getTheme().resolveAttribute(R.attr.colorPrimary, typedValue, true);
        DrawableCompat.setTint(image, typedValue.data);
        Glide.with(mContext)
                .load(uri)
                .fitCenter()
                .placeholder(image)
                .dontAnimate()
                .into(profilePhoto);
//
        songNameTextView.setText(firebaseUserLibraryDetails.getCurrentSong());
                songNameTextView.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                songNameTextView.setSingleLine(true);
                songNameTextView.setMarqueeRepeatLimit(-1);
                songNameTextView.setSelected(true);
                karmaTextView.setText(String.valueOf(firebaseUserLibraryDetails.getKarmaScore()));
                songsInLibraryTextView.setText(String.valueOf(firebaseUserLibraryDetails.getNumberOfSongs()));
                songsCounterTextView.setText(String.valueOf(firebaseUserLibraryDetails.getNumberOfSongsListened()));

//        builder.setView(view);
//        builder.setPositiveButton(R.string.okay, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.cancel();
//            }
//        });
//        final TypedValue typedValue = new TypedValue();
//        mContext.getTheme().resolveAttribute(R.attr.colorAccent, typedValue, true);
//        final AlertDialog alertDialog = builder.create();
//        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
//            @Override
//            public void onShow(DialogInterface arg0) {
//                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(typedValue.data);
//                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(typedValue.data);
//            }
//        });
//        alertDialog.show();
    }

    public void sendLike(final View view, final String destinationUserId, final FirebaseDatabase database, final String songName) {
        if (isLiked) {
            Snackbar.make(view, R.string.already_liked_song, 1000).show();
        } else {
            database.getReference().child(FirebaseUtil.USER_ACCOUNT_DETAILS).child(AccessToken.getCurrentAccessToken().getUserId())
                    .child(FirebaseUtil.USER_ACCOUNT_DETAILS_NUMBER_OF_LIKES_AVAILABLE).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(final DataSnapshot currentUserDataSnapshot) {
                    final long karmaAvailable = currentUserDataSnapshot.getValue(Long.class);
                    if (karmaAvailable == 0) {
                        ComponentUtil.errorAlertDialog(mContext, mContext.getString(R.string.not_enough_likes), false).show();
                    } else {
                        database.getReference().child(FirebaseUtil.USER_ACCOUNT_DETAILS).child(destinationUserId).child(FirebaseUtil.USER_TOKEN_DETAILS).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(final DataSnapshot dataSnapshot) {
                                final String destinationToken = dataSnapshot.getValue(String.class);
                                database.getReference().child(FirebaseUtil.USER_LIBRARY_DETAILS).child(destinationUserId).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(final DataSnapshot dataSnapshot1) {
                                        final FirebaseUserLibraryDetails firebaseUserLibraryDetails = dataSnapshot1.getValue(FirebaseUserLibraryDetails.class);
                                        if (firebaseUserLibraryDetails.isOnlineStatus()) {
                                            try {
                                                ApplicationInfo ai = mContext.getPackageManager().getApplicationInfo(mContext.getPackageName(), PackageManager.GET_META_DATA);
                                                Bundle bundle = ai.metaData;
                                                String serverKey = bundle.getString("firebase-legacy-server-key");
                                                Retrofit retrofit = ApiClient.getClient("https://fcm.googleapis.com/fcm/");
                                                ApiInterface apiInterface = retrofit.create(ApiInterface.class);
                                                JsonObject rootObject = new JsonObject();
                                                rootObject.addProperty("to", destinationToken);
                                                JsonObject dataObject = new JsonObject();
                                                dataObject.addProperty("body", "I like " + songName + "!");
                                                dataObject.addProperty("title", userName);
                                                rootObject.add("notification", dataObject);
                                                Call<ResponseBody> call = apiInterface.sendLikesMessage(rootObject, "key=" + serverKey);
                                                call.enqueue(new Callback<ResponseBody>() {
                                                    @Override
                                                    public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                                                        isLiked = true;
                                                        currentUserDataSnapshot.getRef().setValue(karmaAvailable - 1);
                                                        firebaseUserLibraryDetails.setKarmaScore(firebaseUserLibraryDetails.getKarmaScore() + 1);
                                                        dataSnapshot1.getRef().setValue(firebaseUserLibraryDetails);
                                                    }

                                                    @Override
                                                    public void onFailure(Throwable t) {
                                                        Log.i("SendLikes", "Failure");
                                                    }
                                                });
                                            } catch (PackageManager.NameNotFoundException e) {
                                                Log.e("FacebookFriendsAdapter", "Failed to load meta-data, NameNotFound: " + e.getMessage());
                                            } catch (NullPointerException e) {
                                                Log.e("FacebookFriendsAdapter", "Failed to load meta-data, NullPointer: " + e.getMessage());
                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }
}
