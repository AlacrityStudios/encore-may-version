package com.alacritystudios.encore.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alacritystudios.encore.R;
import com.alacritystudios.encore.interfaces.OptionsSelectedListener;
import com.alacritystudios.encore.models.PlaylistModel;

import java.util.List;

/**
 * Adapter for displaying playlist in a recycler view.
 */

public class AddToPlaylistAdapter extends RecyclerView.Adapter<AddToPlaylistAdapter.AddToPlaylisyViewHolder> {

    private Context mContext;
    private OptionsSelectedListener mOptionsSelectedListener;
    private List<PlaylistModel> mMediaItemList;
    private final int callbackId;

    public AddToPlaylistAdapter(Context mContext, OptionsSelectedListener mOptionsSelectedListener, List<PlaylistModel> mMediaItemList, int callbackId) {
        this.mContext = mContext;
        this.mOptionsSelectedListener = mOptionsSelectedListener;
        this.mMediaItemList = mMediaItemList;
        this.callbackId = callbackId;
    }

    @Override
    public AddToPlaylisyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = ((Activity) mContext).getLayoutInflater().inflate(R.layout.recycler_view_add_to_playlist_item, parent, false);
        return new AddToPlaylistAdapter.AddToPlaylisyViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(AddToPlaylisyViewHolder holder, int position) {
        holder.mMyMusicPlaylistName.setText(mMediaItemList.get(position).getPlaylistName());
    }

    @Override
    public int getItemCount() {
        if (mMediaItemList != null) {
            return mMediaItemList.size();
        } else {
            return 0;
        }
    }

    public class AddToPlaylisyViewHolder extends RecyclerView.ViewHolder {
        private TextView mMyMusicPlaylistName;

        public AddToPlaylisyViewHolder(View itemView) {
            super(itemView);
            mMyMusicPlaylistName = (TextView) itemView.findViewById(R.id.my_music_playlist_name);
            mMyMusicPlaylistName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOptionsSelectedListener.onSelectOption(getAdapterPosition(), callbackId);
                }
            });
        }
    }
}
