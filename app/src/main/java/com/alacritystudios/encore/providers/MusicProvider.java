package com.alacritystudios.encore.providers;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.util.Log;

import com.alacritystudios.encore.application.EncoreApplication;
import com.alacritystudios.encore.constants.CatalogueConstants;
import com.alacritystudios.encore.enums.MusicProviderState;
import com.alacritystudios.encore.interfaces.MusicProviderCallback;
import com.alacritystudios.encore.models.PlaylistModel;
import com.alacritystudios.encore.utils.MediaIdUtil;
import com.alacritystudios.encore.utils.ProviderUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Fetch and generate the music library.
 */

public class MusicProvider {
    private ConcurrentMap<Long, PlaylistModel> mMusicPlaylistDetails;
    private ConcurrentMap<Long, List<MediaMetadataCompat>> mMusicListByArtist;
    private ConcurrentMap<Long, List<MediaMetadataCompat>> mMusicListByAlbum;
    private ConcurrentMap<Long, MediaMetadataCompat> mMusicListBySongs;
    private ConcurrentMap<Long, List<MediaMetadataCompat>> mMusicListByPlaylist;
    private List<MediaBrowserCompat.MediaItem> mMediaItemListBySongs;
    private Context mContext;
    private MusicProviderState mCurrentState;
    private String TAG;

    public MusicProvider() {
        this.mMediaItemListBySongs = new ArrayList<>();
        this.mMusicPlaylistDetails = new ConcurrentHashMap<>();
        this.mMusicListByArtist = new ConcurrentHashMap<>();
        this.mMusicListByAlbum = new ConcurrentHashMap<>();
        this.mMusicListBySongs = new ConcurrentHashMap<>();
        this.mMusicListByPlaylist = new ConcurrentHashMap<>();
        this.mContext = EncoreApplication.getmContext();
        this.mCurrentState = MusicProviderState.NON_INITIALIZED;
        this.TAG = getClass().getSimpleName();
    }

    public void setmCurrentState(MusicProviderState mCurrentState) {
        this.mCurrentState = mCurrentState;
    }

    public List<MediaBrowserCompat.MediaItem> getmMediaItemListBySongs() {
        return mMediaItemListBySongs;
    }

    public ConcurrentMap<Long, PlaylistModel> getmMusicPlaylistDetails() {
        return mMusicPlaylistDetails;
    }

    public ConcurrentMap<Long, List<MediaMetadataCompat>> getmMusicListByArtist() {
        return mMusicListByArtist;
    }

    public ConcurrentMap<Long, List<MediaMetadataCompat>> getmMusicListByAlbum() {
        return mMusicListByAlbum;
    }

    public ConcurrentMap<Long, MediaMetadataCompat> getmMusicListBySongs() {
        return mMusicListBySongs;
    }

    public ConcurrentMap<Long, List<MediaMetadataCompat>> getmMusicListByPlaylist() {
        return mMusicListByPlaylist;
    }

    public boolean isInitialized() {
        return mCurrentState == MusicProviderState.INITIALIZED;
    }

    /**
     * Return the MediaMetadataCompat for the given musicID.
     *
     * @param musicId The unique, non-hierarchical music ID.
     */
    public MediaMetadataCompat getMusic(long musicId) {
        return mMusicListBySongs.containsKey(musicId) ? mMusicListBySongs.get(musicId) : null;
    }

    /**
     * Get the list of music tracks from a server and caches the track information
     * for future reference, keying tracks by musicId and grouping by genre.
     */
    public void retrieveMediaAsync(final MusicProviderCallback musicProviderCallback) {
        Log.i(TAG, "retrieveMediaAsync called");
        if (mCurrentState == MusicProviderState.INITIALIZED) {
            musicProviderCallback.onMusicCatalogReady(true);
            return;
        }
        new AsyncTask<Void, Void, MusicProviderState>() {
            @Override
            protected MusicProviderState doInBackground(Void... params) {
                retrieveMediaFromDevice();
                return mCurrentState;
            }

            @Override
            protected void onPostExecute(MusicProviderState current) {
                if (musicProviderCallback != null) {
                    musicProviderCallback.onMusicCatalogReady(current == MusicProviderState.INITIALIZED);
                }
            }
        }.execute();
    }

    /**
     * The base method to get all the required media details from the device.
     */
    public synchronized void retrieveMediaFromDevice() {
        Log.i(TAG, "retrieveMediaFromDevice");
        if (mCurrentState == MusicProviderState.NON_INITIALIZED) {
            mCurrentState = MusicProviderState.INITIALIZING;

            Cursor allSongsCursor = ProviderUtil.getAllSongsCursor(mContext);
            if (allSongsCursor.moveToFirst()) {
                do {
                    MediaMetadataCompat item = new MediaMetadataCompat.Builder()
                            .putString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID, String.valueOf(allSongsCursor.getLong(5)))
                            .putString(CatalogueConstants.GeneralConstants.CUSTOM_METADATA_TRACK_SOURCE, allSongsCursor.getString(4))
                            .putString(MediaMetadataCompat.METADATA_KEY_ALBUM, allSongsCursor.getString(2))
                            .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, allSongsCursor.getString(1))
                            .putLong(MediaMetadataCompat.METADATA_KEY_DURATION, allSongsCursor.getLong(3))
                            .putString(MediaMetadataCompat.METADATA_KEY_TITLE, allSongsCursor.getString(0))
                            .putLong(CatalogueConstants.GeneralConstants.CUSTOM_METADATA_ARTIST_ID, allSongsCursor.getLong(7))
                            .putLong(CatalogueConstants.GeneralConstants.CUSTOM_METADATA_ALBUM_ID, allSongsCursor.getLong(6))
                            .build();
                    mMusicListBySongs.put(allSongsCursor.getLong(5), item);
                    MediaBrowserCompat.MediaItem mediaItem = new MediaBrowserCompat.MediaItem(
                            new MediaDescriptionCompat.Builder()
                                    .setMediaId(MediaIdUtil.buildMediaId(MediaIdUtil.ROOT_MEDIA_SONGS, String.valueOf(allSongsCursor.getLong(5))))
                                    .setTitle(allSongsCursor.getString(0))
                                    .setSubtitle(allSongsCursor.getString(1) + " - " + allSongsCursor.getString(2))
                                    .setIconUri(ProviderUtil.getAlbumArtworkUri(allSongsCursor.getLong(6)))
                                    .build(), MediaBrowserCompat.MediaItem.FLAG_PLAYABLE
                    );

                    mMediaItemListBySongs.add(mediaItem);
                    List<MediaMetadataCompat> list;
                    list = mMusicListByArtist.get(allSongsCursor.getLong(7));
                    if (list == null) {
                        list = new ArrayList<>();
                        mMusicListByArtist.put(allSongsCursor.getLong(7), list);
                    }
                    list.add(mMusicListBySongs.get(allSongsCursor.getLong(5)));

                    list = mMusicListByAlbum.get(allSongsCursor.getLong(6));
                    if (list == null) {
                        list = new ArrayList<>();
                        mMusicListByAlbum.put(allSongsCursor.getLong(6), list);
                    }
                    list.add(mMusicListBySongs.get(allSongsCursor.getLong(5)));

                } while (allSongsCursor.moveToNext());
            }
            Cursor playlistCursor = ProviderUtil.getPlaylistCursor(mContext);
            if (playlistCursor.moveToFirst()) {
                do {
                    List<MediaMetadataCompat> playlistSongsList = new ArrayList<>();
                    Long playlistId = Long.parseLong(playlistCursor.getString(0));
                    String playlistName = playlistCursor.getString(1);
                    Cursor playlistMediaCursor = ProviderUtil.getPlaylistMediaCursor(mContext, playlistId);
                    PlaylistModel playlistModel = new PlaylistModel();
                    playlistModel.setPlaylistId(playlistId);
                    playlistModel.setPlaylistName(playlistName);
                    if (playlistMediaCursor.moveToFirst()) {
                        do {
                            MediaMetadataCompat item = new MediaMetadataCompat.Builder()
                                    .putString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID, String.valueOf(playlistMediaCursor.getLong(5)))
                                    .putString(CatalogueConstants.GeneralConstants.CUSTOM_METADATA_TRACK_SOURCE, playlistMediaCursor.getString(4))
                                    .putString(MediaMetadataCompat.METADATA_KEY_ALBUM, playlistMediaCursor.getString(2))
                                    .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, playlistMediaCursor.getString(1))
                                    .putLong(MediaMetadataCompat.METADATA_KEY_DURATION, playlistMediaCursor.getLong(3))
                                    .putString(MediaMetadataCompat.METADATA_KEY_TITLE, playlistMediaCursor.getString(0))
                                    .putLong(CatalogueConstants.GeneralConstants.CUSTOM_METADATA_ARTIST_ID, playlistMediaCursor.getLong(7))
                                    .putLong(CatalogueConstants.GeneralConstants.CUSTOM_METADATA_ALBUM_ID, playlistMediaCursor.getLong(6))
                                    .build();
                            playlistSongsList.add(item);
                            playlistModel.setNumberOfSongs(playlistModel.getNumberOfSongs() + 1);
                        } while (playlistMediaCursor.moveToNext());
                    }
                    mMusicListByPlaylist.put(playlistId, playlistSongsList);
                    playlistMediaCursor.close();
                    mMusicPlaylistDetails.put(playlistId, playlistModel);
                } while (playlistCursor.moveToNext());
            }
            playlistCursor.close();
            allSongsCursor.close();
            mCurrentState = MusicProviderState.INITIALIZED;
            Log.i(TAG, "Music catalouging is complete");
        }
    }

    public ConcurrentMap<Long, List<MediaMetadataCompat>> getAlbumListsByArtist(long inputArtistId) {
        ConcurrentMap<Long, List<MediaMetadataCompat>> newMusicAlbumListByArtist = new ConcurrentHashMap<>();
        for (MediaMetadataCompat mediaMetadataCompat : mMusicListBySongs.values()) {
            long artistId = mediaMetadataCompat.getLong(CatalogueConstants.GeneralConstants.CUSTOM_METADATA_ARTIST_ID);
            if (artistId == inputArtistId) {
                long albumId = mediaMetadataCompat.getLong(CatalogueConstants.GeneralConstants.CUSTOM_METADATA_ALBUM_ID);
                List<MediaMetadataCompat> list = newMusicAlbumListByArtist.get(albumId);
                if (list == null) {
                    list = new ArrayList<>();
                    newMusicAlbumListByArtist.put(albumId, list);
                }
                list.add(mediaMetadataCompat);
            }
        }
        return newMusicAlbumListByArtist;
    }
}
