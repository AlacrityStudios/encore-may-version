package com.alacritystudios.encore.helpers;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.alacritystudios.encore.components.CustomFrameLayout;
import com.alacritystudios.encore.utils.ConversionUtil;

/**
 * Defines the behaviour expected out of play pause button on slide of slider.
 */

public class MediaItemPlayPauseButtonBehaviour extends CoordinatorLayout.Behavior<ImageView> {
    private Context mContext;

    public MediaItemPlayPauseButtonBehaviour() {
        super();
    }

    public MediaItemPlayPauseButtonBehaviour(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, ImageView child, View dependency) {
        return dependency instanceof CustomFrameLayout;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, ImageView child, View dependency) {
        modifyImageView(child, dependency);
        return true;
    }

    private void modifyImageView(ImageView child, View dependency) {
        CustomFrameLayout customFrameLayout = (CustomFrameLayout) dependency;
        float offset = customFrameLayout.getmSlidingOffset();
        ViewGroup.LayoutParams layoutParams = child.getLayoutParams();
        layoutParams.height = (int)((1.0f - offset) * ConversionUtil.convertDpToPx(mContext, 40));
        layoutParams.width = (int)((1.0f - offset) * ConversionUtil.convertDpToPx(mContext, 40));
        child.setLayoutParams(layoutParams);
        child.setTranslationX(customFrameLayout.getmContainerLayoutHeight() - ConversionUtil.convertDpToPx(mContext, 56));
        child.setTranslationY((ConversionUtil.convertDpToPx(mContext, 60) - child.getHeight())/2);
    }
}
