package com.alacritystudios.encore.helpers;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.alacritystudios.encore.adapters.QueueAdapter;
import com.alacritystudios.encore.interfaces.ItemTouchHelperListener;

/**
 * Helper class to define the different types to touch interactions with a recycler view.
 */

public class MediaItemTouchHelperSimpleCallback extends ItemTouchHelper.SimpleCallback {

    private final ItemTouchHelperListener mItemTouchHelperListener;

    public MediaItemTouchHelperSimpleCallback(int dragDirs, int swipeDirs, ItemTouchHelperListener mItemTouchHelperListener) {
        super(dragDirs, swipeDirs);
        this.mItemTouchHelperListener = mItemTouchHelperListener;
    }

    @Override
    public boolean isLongPressDragEnabled() {
        return true;
    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return true;
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        if (viewHolder instanceof QueueAdapter.QueueViewHolder && target instanceof QueueAdapter.QueueViewHolder) {
            if (((QueueAdapter.QueueViewHolder) viewHolder).ismIsCurrentlyPlayingItem()) {
                return false;
            }
        }
        mItemTouchHelperListener.onItemMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        mItemTouchHelperListener.onItemDismiss(viewHolder.getAdapterPosition());
    }

    @Override
    public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        if (viewHolder instanceof QueueAdapter.QueueViewHolder) {
            if (((QueueAdapter.QueueViewHolder) viewHolder).ismIsCurrentlyPlayingItem()) {
                return 0;
            }
        }
        return super.getSwipeDirs(recyclerView, viewHolder);
    }
}
