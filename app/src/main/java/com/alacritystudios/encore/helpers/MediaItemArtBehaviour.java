package com.alacritystudios.encore.helpers;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.alacritystudios.encore.components.CustomFrameLayout;
import com.alacritystudios.encore.utils.ConversionUtil;

/**
 * Defines the behaviour expected out of Album art on slide of slider.
 */

public class MediaItemArtBehaviour extends CoordinatorLayout.Behavior<ImageView> {

    private Context mContext;

    public MediaItemArtBehaviour() {
        super();
    }

    public MediaItemArtBehaviour(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, ImageView child, View dependency) {
        return dependency instanceof CustomFrameLayout;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, ImageView child, View dependency) {
        modifyImageView(child, dependency);
        return true;
    }

    private void modifyImageView(ImageView child, View dependency) {
        CustomFrameLayout customFrameLayout = (CustomFrameLayout) dependency;
        int imageViewWidth = 44;
        int imageViewHeight = 44;
        float xTranslation = 0;
        float yTranslation = 0;
        int containerLayoutHeight = customFrameLayout.getmContainerLayoutHeight();
        float offset = customFrameLayout.getmSlidingOffset();
        int frameLayoutWidth = customFrameLayout.getWidth();
//        if (frameLayoutHeight <= ConversionUtil.convertDpToPx(mContext, 44)) {
//            imageViewHeight = ConversionUtil.convertDpToPx(mContext, 44);
//        } else if (frameLayoutHeight >= customFrameLayout.getmContainerLayoutHeight()) {
//            imageViewHeight = customFrameLayout.getmContainerLayoutHeight();
//
//        } else {
//            imageViewHeight = frameLayoutHeight;
//        }
        imageViewHeight = (int) (customFrameLayout.getmSlidingOffset() * containerLayoutHeight);
        imageViewWidth = (int) (customFrameLayout.getmSlidingOffset() * frameLayoutWidth);
        if (imageViewWidth <= ConversionUtil.convertDpToPx(mContext, 44)) {
            imageViewWidth = ConversionUtil.convertDpToPx(mContext, 44);
        } else if (imageViewWidth >= frameLayoutWidth) {
            imageViewWidth = -1;
        }
        if (imageViewHeight <= ConversionUtil.convertDpToPx(mContext, 44)) {
            imageViewHeight = ConversionUtil.convertDpToPx(mContext, 44);
        } else if (imageViewHeight >= customFrameLayout.getmContainerLayoutHeight()) {
            imageViewHeight = customFrameLayout.getmContainerLayoutHeight();

        }

        xTranslation = ((1.0f - offset) * ConversionUtil.convertDpToPx(mContext, 16));
        yTranslation = ((1.0f - offset) * ConversionUtil.convertDpToPx(mContext, 8));

        ViewGroup.LayoutParams layoutParams = child.getLayoutParams();
        layoutParams.height = imageViewHeight;
        layoutParams.width = imageViewWidth;
        child.setLayoutParams(layoutParams);
        child.setTranslationX(xTranslation);
        child.setTranslationY(yTranslation);
    }
}
