package com.alacritystudios.encore.helpers;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alacritystudios.encore.components.CustomFrameLayout;
import com.alacritystudios.encore.utils.ConversionUtil;

/**
 * Defines the behaviour expected out of song name on slide of slider.
 */

public class SongNameBehaviour extends CoordinatorLayout.Behavior<TextView> {
    private Context mContext;

    public SongNameBehaviour() {
        super();
    }

    public SongNameBehaviour(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, TextView child, View dependency) {
        return dependency instanceof CustomFrameLayout;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, TextView child, View dependency) {
        modifyTextView(child, dependency);
        return true;
    }

    private void modifyTextView(TextView child, View dependency) {
        CustomFrameLayout customFrameLayout = (CustomFrameLayout) dependency;
        float offset = customFrameLayout.getmSlidingOffset();
        float fontSize = ((1.0f - offset) * 16);
        child.setTextSize(fontSize);
        child.setTranslationX(ConversionUtil.convertDpToPx(mContext, 76));
        ViewGroup.LayoutParams layoutParams = child.getLayoutParams();
        layoutParams.height = ConversionUtil.convertDpToPx(mContext, 60);
        layoutParams.width = customFrameLayout.getmContainerLayoutHeight() - ConversionUtil.convertDpToPx(mContext, 148);
        child.setLayoutParams(layoutParams);
        //child.setTranslationY(ConversionUtil.convertDpToPx(mContext, 30));
    }
}
