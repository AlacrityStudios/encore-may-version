package com.alacritystudios.encore.activities;

import android.app.ActivityManager;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.session.PlaybackState;
import android.net.ParseException;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.RemoteException;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.alacritystudios.encore.R;
import com.alacritystudios.encore.activities.base.BaseActivity;
import com.alacritystudios.encore.adapters.DrawerAdapter;
import com.alacritystudios.encore.adapters.FacebookFriendsAdapter;
import com.alacritystudios.encore.adapters.LibraryViewPagerAdapter;
import com.alacritystudios.encore.constants.CatalogueConstants;
import com.alacritystudios.encore.enums.SortOrder;
import com.alacritystudios.encore.fragments.ContainerFragment;
import com.alacritystudios.encore.fragments.MediaPlayerFragment;
import com.alacritystudios.encore.fragments.SleepTimerFragment;
import com.alacritystudios.encore.interfaces.DialogConfirmationCallback;
import com.alacritystudios.encore.interfaces.DrawerItemClickListener;
import com.alacritystudios.encore.interfaces.MediaBrowserFragmentListener;
import com.alacritystudios.encore.interfaces.SleepTimerCallback;
import com.alacritystudios.encore.models.DrawerModel;
import com.alacritystudios.encore.models.FacebookFriendModel;
import com.alacritystudios.encore.services.MusicService;
import com.alacritystudios.encore.utils.AppStartUtil;
import com.alacritystudios.encore.utils.ComponentUtil;
import com.alacritystudios.encore.utils.FirebaseUtil;
import com.alacritystudios.encore.utils.MediaIdUtil;
import com.alacritystudios.encore.utils.NetworkUtil;
import com.alacritystudios.encore.utils.PreferenceUtil;
import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class LandingActivity extends BaseActivity implements BaseActivity.BaseActivityImpl, DrawerItemClickListener,
        SmartTabLayout.TabProvider, ViewPager.OnPageChangeListener, MediaBrowserFragmentListener, DialogConfirmationCallback,
        SleepTimerCallback {

    private static final int RETRY_LOGIN_ATTEMPT_REFERENCE = 420;
    private Toolbar mToolbar;
    private DrawerLayout mNavigationDrawerLayout;
    private ActionBarDrawerToggle mNavigationDrawerToggle;
    private View mHeaderView;
    private ListView mNavigationDrawerListView;
    private DrawerAdapter mNavigationDrawerAdapter;
    private List<DrawerModel> mNavigationDrawerModelList;
    private SmartTabLayout mLibraryViewTabLayout;
    private FragmentManager mFragmentManager;
    private LibraryViewPagerAdapter mLibraryViewPagerAdapter;
    private ViewPager mLibraryViewPager;
    private ContainerFragment mSongContainerFragment;
    private ContainerFragment mArtistContainerFragment;
    private ContainerFragment mAlbumContainerFragment;
    private ContainerFragment mPlaylistContainerFragment;
    private MediaPlayerFragment mMediaPlayerFragment;
    private SlidingUpPanelLayout mSlidingPanel;
    private int mPanelHeight;
    private SortOrder mSortOrder;
    private String mSearchQueryString;
    private MediaBrowserCompat mMediaBrowserCompat;
    private MediaControllerCompat mMediaControllerCompat;
    private MenuItem mSearchMenuItem;
    private FacebookFriendsAdapter mFacebookFriendsAdapter;
    private List<FacebookFriendModel> mFacebookfriends;
    private Drawable mPlaceholderAccountDrawable;
    private DrawerLayout mFacebookFriendsDrawerLayout;
    private ActionBarDrawerToggle mFacebookFriendsDrawerToggle;
    private ListView mFacebookFriendsDrawerListView;
    private FirebaseUser mFirebaseUser;
    private CountDownTimer mCountDownTimer;
    private TypedValue mColorAccentTypedValue;
    private TypedValue mColorPrimaryTypedValue;
    private SleepTimerFragment mSleepTimerFragment;
    private InterstitialAd mInterstitialAd;


    private MediaBrowserCompat.ConnectionCallback mConnectionCallback = new MediaBrowserCompat.ConnectionCallback() {
        @Override
        public void onConnected() {
            super.onConnected();
            AppStartUtil.sendReloadLibraryLocalBroadcast();
        }
    };

    private final MediaControllerCompat.Callback mMediaControllerCallback =
            new MediaControllerCompat.Callback() {
                @Override
                public void onSessionDestroyed() {
                    super.onSessionDestroyed();
                    Log.d(TAG, "onSessionDestroyed");
                }

                @Override
                public void onSessionEvent(String event, Bundle extras) {
                    super.onSessionEvent(event, extras);
                    Log.d(TAG, "onSessionEvent");
                }

                @Override
                public void onQueueChanged(List<MediaSessionCompat.QueueItem> queue) {
                    super.onQueueChanged(queue);
                    Log.d(TAG, "onQueueChanged");
                }

                @Override
                public void onQueueTitleChanged(CharSequence title) {
                    super.onQueueTitleChanged(title);
                    Log.d(TAG, "onQueueTitleChanged");
                }

                @Override
                public void onExtrasChanged(Bundle extras) {
                    super.onExtrasChanged(extras);
                    Log.d(TAG, "onExtrasChanged");
                }

                @Override
                public void onAudioInfoChanged(MediaControllerCompat.PlaybackInfo info) {
                    super.onAudioInfoChanged(info);
                    Log.d(TAG, "onAudioInfoChanged");
                }

                @Override
                public void binderDied() {
                    super.binderDied();
                    Log.d(TAG, "binderDied");
                }

                @Override
                public void onPlaybackStateChanged(PlaybackStateCompat state) {
                    Log.d(TAG, "onPlaybackStateChanged");
                    if (state.getState() == PlaybackState.STATE_ERROR) {
                        mMediaControllerCompat.getTransportControls().stop();
                        mSlidingPanel.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                    }
                    if (shouldShowControls()) {
                        showPlaybackControls();
                    } else {
                        Log.d(TAG, "mediaControllerCallback.onPlaybackStateChanged: " + "hiding controls because state is " + (state == null ? "null" : state.getState()));
                        hidePlaybackControls();
                    }
                }

                @Override
                public void onMetadataChanged(MediaMetadataCompat metadata) {
                    Log.d(TAG, "onMetadataChanged");
                    if (shouldShowControls()) {
                        showPlaybackControls();
                    } else {
                        Log.d(TAG, "mediaControllerCallback.onMetadataChanged: " + "hiding controls because metadata is null");
                        hidePlaybackControls();
                    }
                }
            };

    private BroadcastReceiver mUserLoginReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
            setupHeaderLayout();
            setupFriendsLayout();
            setupDrawerLayout();
        }
    };

    @Override
    public CountDownTimer fetchCountDownTimer() {
        return mCountDownTimer;
    }

    @Override
    public void setCountDownTimer(CountDownTimer countDownTimer) {
        mCountDownTimer = countDownTimer;
    }

    @Override
    public void onCountDownFinish() {
        if (mMediaControllerCompat != null) {
            mMediaControllerCompat.getTransportControls().pause();
        }
    }

    @Override
    public void onDialogConfirm(int buttonReference) {
        switch (buttonReference) {
            case RETRY_LOGIN_ATTEMPT_REFERENCE:
                Intent intent = new Intent(mContext, SettingsActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void initialiseUI() {
        mNavigationDrawerLayout = (DrawerLayout) findViewById(R.id.navigation_drawer_layout);
        mNavigationDrawerListView = (ListView) findViewById(R.id.navigation_drawer_listview);
        mFacebookFriendsDrawerLayout = (DrawerLayout) findViewById(R.id.facebook_friends_drawer_layout);
        mFacebookFriendsDrawerListView = (ListView) findViewById(R.id.facebook_friends_drawer_listview);
        mLibraryViewTabLayout = (SmartTabLayout) findViewById(R.id.tab_layout_library_view);
        mLibraryViewTabLayout.setCustomTabView(this);
        mLibraryViewPager = (ViewPager) findViewById(R.id.viewpager_library_view);
        mSlidingPanel = (SlidingUpPanelLayout) findViewById(R.id.sliding_player_control_layout);
        setupToolbar();
        setupHeaderLayout();
        setupDrawerLayout();
        setupFriendsLayout();
        setupTabLayout();
        setupSlidingPanel();
    }

    @Override
    public void setListeners() {

    }

    @Override
    public void setContextAndTags() {
        TAG = getClass().getSimpleName();
        mContext = LandingActivity.this;
    }

    @Override
    public void initialiseOtherComponents() {
        setTaskDescription();
        mColorAccentTypedValue = new TypedValue();
        mColorPrimaryTypedValue = new TypedValue();
        getTheme().resolveAttribute(R.attr.colorPrimary, mColorPrimaryTypedValue, true);
        getTheme().resolveAttribute(R.attr.colorAccent, mColorAccentTypedValue, true);
        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        IntentFilter filter = new IntentFilter("com.alacritystudios.encore.RELOAD_FRIENDS_DATA");
        LocalBroadcastManager.getInstance(mContext).registerReceiver(mUserLoginReceiver, filter);
        mPlaceholderAccountDrawable = ActivityCompat.getDrawable(mContext, R.drawable.ic_person);
        DrawableCompat.wrap(mPlaceholderAccountDrawable).mutate().setTint(mColorPrimaryTypedValue.data);
        mFragmentManager = getSupportFragmentManager();
        mNavigationDrawerModelList = new ArrayList<>();
        mNavigationDrawerModelList.add(new DrawerModel(getString(R.string.my_music), R.drawable.ic_music_note));
        //mNavigationDrawerModelList.add(new DrawerModel(getString(R.string.equalizer), R.drawable.ic_equalizer));
        mNavigationDrawerModelList.add(new DrawerModel(getString(R.string.future_updates), R.drawable.ic_update));
        mNavigationDrawerModelList.add(new DrawerModel(getString(R.string.sleep_timer), R.drawable.ic_timer));
        mNavigationDrawerModelList.add(new DrawerModel(getString(R.string.share_the_app), R.drawable.ic_music_share));
        mNavigationDrawerModelList.add(new DrawerModel(getString(R.string.settings), R.drawable.ic_settings));
        mNavigationDrawerAdapter = new DrawerAdapter(mContext, R.layout.recycler_view_drawer_item, mNavigationDrawerModelList);
        mSearchQueryString = "";
        mSleepTimerFragment = SleepTimerFragment.newInstance(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContextAndTags();
        setTheme(AppStartUtil.getCurrentColorSchemeTheme(mContext, false));
        setContentView(R.layout.activity_landing);
        Log.i(TAG, "onCreate()");
        initialiseOtherComponents();
        initialiseUI();
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_ad_unit_id));
        mInterstitialAd.loadAd(new AdRequest.Builder().addTestDevice("254A95527DBC0D8C10F9CC87F9B7FE47").build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                mInterstitialAd.show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
            }
        });
    }

    @Override
    protected void onStart() {
        Log.i(TAG, "onStart()");
        super.onStart();
        if (PreferenceUtil.getCurrentSortOrderPreference(mContext) == 0) {
            mSortOrder = SortOrder.ASCENDING;
        } else {
            mSortOrder = SortOrder.DESCENDING;
        }
        mNavigationDrawerAdapter.setSelectedPosition(0);
        mNavigationDrawerAdapter.notifyDataSetChanged();
        if (mMediaBrowserCompat == null) {
            mMediaBrowserCompat = new MediaBrowserCompat(mContext, new ComponentName(mContext, MusicService.class), mConnectionCallback, null);
        }
        if (!mMediaBrowserCompat.isConnected()) {
            mMediaBrowserCompat.connect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        Log.i(TAG, "onStop()");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG, "onDestroy()");
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mNavigationDrawerLayout.openDrawer(Gravity.START);
                return true;
            case R.id.menu_invert_sort_order:
                if (mSortOrder == SortOrder.ASCENDING) {
                    mSortOrder = SortOrder.DESCENDING;
                    PreferenceUtil.setCurrentSortOrderPreference(mContext, 1);
                } else {
                    mSortOrder = SortOrder.ASCENDING;
                    PreferenceUtil.setCurrentSortOrderPreference(mContext, 0);
                }
                // changeSortOrder(mSortOrder);
                break;
            case R.id.menu_show_friends:
                if (!NetworkUtil.isNetworkAvailable(mContext)) {
                    ComponentUtil.errorAlertDialog(mContext, getString(R.string.connectivity_error), false);
                } else if (NetworkUtil.isNetworkAvailable(mContext) && AccessToken.getCurrentAccessToken() == null) {
                    ComponentUtil.confirmationAlertDialog(mContext, getString(R.string.login_error), getString(R.string.login_error_title), this, RETRY_LOGIN_ATTEMPT_REFERENCE);
                } else {
                    mFacebookFriendsDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                    mFacebookFriendsDrawerLayout.openDrawer(Gravity.END);
                }
                break;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_landing_activity, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        mSearchMenuItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) mSearchMenuItem.getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        ((EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text)).setHintTextColor(ActivityCompat.getColor(mContext, R.color.colorWhite));
        ((EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text)).addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mSearchQueryString = s.toString();
                AppStartUtil.sendReloadMusicListLocalBroadcast();
            }
        });

        MenuItemCompat.setOnActionExpandListener(mSearchMenuItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                mSearchQueryString = "";
                AppStartUtil.sendReloadMusicListLocalBroadcast();
                return true;
            }
        });
        return true;
    }

    @Override
    public void onItemClicked(int position) {
        mNavigationDrawerLayout.closeDrawers();
        Intent intent = null;
        switch (position - 1) {
            case 0:
                break;
            case 1:
                ComponentUtil.listAlertDialog(mContext, Arrays.asList(mContext.getResources().getStringArray(R.array.future_updates)), mContext.getString(R.string.future_updates)).show();
                break;
            case 2:
                showTimerDialog();
                break;
            case 3:
                shareTheApp();
                break;
            case 4:
                intent = new Intent(mContext, SettingsActivity.class);
                break;
        }
        if (intent != null) {
            startActivity(intent);
        }
    }

    @Override
    public View createTabView(ViewGroup container, int position, PagerAdapter adapter) {
        Drawable image;
        LayoutInflater inflater = LayoutInflater.from(container.getContext());
        View tab = inflater.inflate(R.layout.layout_custom_tab, container, false);
        CircleImageView icon = (CircleImageView) tab.findViewById(R.id.tab_image);
        TextView text = (TextView) tab.findViewById(R.id.tab_text);
        switch (position) {
            case 0:
                image = ActivityCompat.getDrawable(mContext, R.drawable.ic_music_now_playing);
                DrawableCompat.wrap(image).mutate().setTint(mColorAccentTypedValue.data);
                icon.setImageDrawable(image);
                icon.setPadding(12, 12, 12, 12);
                icon.setBackground(ContextCompat.getDrawable(mContext, R.drawable.border_circle_white));
                text.setText(getString(R.string.songs));
                break;
            case 1:
                image = ActivityCompat.getDrawable(mContext, R.drawable.ic_music_artist);
                DrawableCompat.wrap(image).mutate().setTint(mColorAccentTypedValue.data);
                icon.setImageDrawable(image);
                icon.setPadding(12, 12, 12, 12);
                icon.setBackground(ContextCompat.getDrawable(mContext, R.drawable.border_circle_white));
                text.setText(getString(R.string.artists));
                break;
            case 2:
                image = ActivityCompat.getDrawable(mContext, R.drawable.ic_music_album);
                DrawableCompat.wrap(image).mutate().setTint(mColorAccentTypedValue.data);
                icon.setImageDrawable(image);
                icon.setPadding(12, 12, 12, 12);
                icon.setBackground(ContextCompat.getDrawable(mContext, R.drawable.border_circle_white));
                text.setText(getString(R.string.albums));
                break;
            case 3:
                image = ActivityCompat.getDrawable(mContext, R.drawable.ic_music_playlist);
                DrawableCompat.wrap(image).mutate().setTint(mColorAccentTypedValue.data);
                icon.setImageDrawable(image);
                icon.setPadding(12, 12, 12, 12);
                icon.setBackground(ContextCompat.getDrawable(mContext, R.drawable.border_circle_white));
                text.setText(getString(R.string.playlists));
                break;
            default:
                throw new IllegalStateException("Invalid position: " + position);
        }
        return tab;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        Log.i(TAG, "onPageSelected: " + position);
        ((ContainerFragment) mLibraryViewPagerAdapter.getItem(position)).clearBackStack();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public String getmSearchQueryString() {
        return mSearchQueryString;
    }

    @Override
    public MediaBrowserCompat getmMediaBrowserCompat() {
        return mMediaBrowserCompat;
    }

    @Override
    public MediaControllerCompat getmMediaControllerCompat() {
        return mMediaControllerCompat;
    }

    @Override
    public void onMediaItemClicked(MediaBrowserCompat.MediaItem mediaItem) {
        Log.i(TAG, "onMediaItemClicked()");
        if (mSearchMenuItem.isActionViewExpanded()) {
            mSearchMenuItem.collapseActionView();
        }

        if (mediaItem.isBrowsable()) {
            ((ContainerFragment) mLibraryViewPagerAdapter.getItem(mLibraryViewPager.getCurrentItem())).onMediaItemSelected(mediaItem);
        } else if (mediaItem.isPlayable()) {
            Log.d(TAG, "Item is playable");

            if (mMediaControllerCompat == null) {
                try {
                    mMediaControllerCompat = new MediaControllerCompat(mContext, mMediaBrowserCompat.getSessionToken());
                    mMediaControllerCompat.registerCallback(mMediaControllerCallback);
                    loadMediaPlayerFragment();
                } catch (RemoteException ex) {
                    Log.e(TAG, ex.getMessage());
                }
            }
            mMediaControllerCompat.getTransportControls().playFromMediaId(mediaItem.getMediaId(), null);
        }
    }

    @Override
    public void onBackPressed() {
        if (mSlidingPanel.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
            mSlidingPanel.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        } else {
            ContainerFragment mSelectedContainer = (ContainerFragment) mLibraryViewPagerAdapter.getItem(mLibraryViewPager.getCurrentItem());
            mSelectedContainer.onBackKeyPress();
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mNavigationDrawerToggle.syncState();
        mFacebookFriendsDrawerToggle.syncState();
    }

    /**
     * Setup the custom toolbar.
     */
    private void setupToolbar() {
        Log.i(TAG, "setupToolbar()");
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    /**
     * Set up the header layout for the drawers.
     */
    public void setupHeaderLayout() {
        Log.i(TAG, "setupHeaderLayout()");
        if (mHeaderView != null) {
            mFacebookFriendsDrawerListView.removeHeaderView(mHeaderView);
            mNavigationDrawerListView.removeHeaderView(mHeaderView);
        }
        mHeaderView = getLayoutInflater().inflate(R.layout.recycler_view_drawer_header, mNavigationDrawerListView, false);
        String photoUrlString = PreferenceUtil.getFacebookImagePreference(mContext);
        Uri photoUri;
        try {
            photoUri = Uri.parse(photoUrlString);
        } catch (ParseException ex) {
            photoUri = null;
        }
        Drawable image = new ColorDrawable(ActivityCompat.getColor(mContext, R.color.colorWhite));
        Glide.with(mContext)
                .load(photoUri)
                .centerCrop()
                .dontAnimate()
                .error(image)
                .into((CircleImageView) mHeaderView.findViewById(R.id.drawer_user_image));
    }

    /**
     * Setup custom the drawer layout.
     */
    private void setupDrawerLayout() {
        Log.i(TAG, "setupDrawerLayout()");
        mNavigationDrawerToggle = new android.support.v7.app.ActionBarDrawerToggle(this, mNavigationDrawerLayout, R.string.app_name, R.string.app_name);
        mNavigationDrawerToggle.syncState();
        mNavigationDrawerListView.removeHeaderView(mHeaderView);
        mNavigationDrawerListView.addHeaderView(mHeaderView);
        mNavigationDrawerListView.setAdapter(mNavigationDrawerAdapter);
        mNavigationDrawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onItemClicked(position);
            }
        });
        mNavigationDrawerLayout.addDrawerListener(mNavigationDrawerToggle);
    }

    /**
     * Sets up the sliding panel
     */
    private void setupSlidingPanel() {
        Log.i(TAG, "setupSlidingPanel");
        mPanelHeight = mSlidingPanel.getPanelHeight();
        mSlidingPanel.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                mMediaPlayerFragment.setOffsetOfCustomFrameLayout(slideOffset);
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                if (newState == SlidingUpPanelLayout.PanelState.EXPANDED) {
                    mNavigationDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                }
                if (newState == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                    mNavigationDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                }
            }
        });
        mSlidingPanel.setPanelHeight(0);
    }

    public void setupTabLayout() {
        Log.i(TAG, "setupTabLayout()");
        mLibraryViewPagerAdapter = new LibraryViewPagerAdapter(mFragmentManager);

        mSongContainerFragment = ContainerFragment.newInstance(MediaIdUtil.ROOT_MEDIA_SONGS);
        mArtistContainerFragment = ContainerFragment.newInstance(MediaIdUtil.ROOT_MEDIA_ARTISTS);
        mAlbumContainerFragment = ContainerFragment.newInstance(MediaIdUtil.ROOT_MEDIA_ALBUMS);
        mPlaylistContainerFragment = ContainerFragment.newInstance(MediaIdUtil.ROOT_MEDIA_PLAYLISTS);
        mLibraryViewPagerAdapter.addFragment(mSongContainerFragment, getString(R.string.songs));
        mLibraryViewPagerAdapter.addFragment(mArtistContainerFragment, getString(R.string.artists));
        mLibraryViewPagerAdapter.addFragment(mAlbumContainerFragment, getString(R.string.albums));
        mLibraryViewPagerAdapter.addFragment(mPlaylistContainerFragment, getString(R.string.playlists));

        mLibraryViewPager.setAdapter(mLibraryViewPagerAdapter);
        mLibraryViewPager.setOffscreenPageLimit(3);
        mLibraryViewTabLayout.setCustomTabView(this);
        mLibraryViewTabLayout.setViewPager(mLibraryViewPager);
        mLibraryViewPager.addOnPageChangeListener(this);
    }

    /**
     * Loads the media control fragment
     */
    private void loadMediaPlayerFragment() {
        mMediaPlayerFragment = new MediaPlayerFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.media_controls_container, mMediaPlayerFragment);
        fragmentTransaction.commit();
    }

    /**
     * Check if the MediaSession is active and in a "playback-able" state
     * (not NONE and not STOPPED).
     *
     * @return true if the MediaSession's state requires playback controls to be visible.
     */
    protected boolean shouldShowControls() {
        if (mMediaControllerCompat == null || mMediaControllerCompat.getMetadata() == null || mMediaControllerCompat.getPlaybackState() == null) {
            return false;
        }
        switch (mMediaControllerCompat.getPlaybackState().getState()) {
            case PlaybackState.STATE_ERROR:
            case PlaybackState.STATE_NONE:
                return false;
            default:
                return true;
        }
    }

    protected void showPlaybackControls() {
        Log.i(TAG, "showPlaybackControls");
        mSlidingPanel.setPanelHeight(mPanelHeight);
    }

    protected void hidePlaybackControls() {
        Log.i(TAG, "hidePlaybackControls");
        mSlidingPanel.setPanelHeight(0);
    }

    /**
     * Setup custom the drawer layout.
     */
    private void setupFriendsLayout() {
        Log.i(TAG, "setupFriendsLayout()");
        mFacebookFriendsDrawerToggle = new android.support.v7.app.ActionBarDrawerToggle(this, mFacebookFriendsDrawerLayout, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                mFacebookFriendsDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            }
        };
        mFacebookFriendsDrawerToggle.syncState();
        mFacebookFriendsDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        mFacebookFriendsDrawerListView.removeHeaderView(mHeaderView);
        mFacebookFriendsDrawerListView.addHeaderView(mHeaderView);
        mFacebookfriends = new ArrayList<>();
        if (NetworkUtil.isNetworkAvailable(mContext) && AccessToken.getCurrentAccessToken() != null) {
            final String userId = AccessToken.getCurrentAccessToken().getUserId();
            final FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            if (NetworkUtil.isNetworkAvailable(getApplicationContext()))
                firebaseDatabase.getReference(userId).child(FirebaseUtil.USER_ACCOUNT_DETAILS).child(FirebaseUtil.USER_TOKEN_DETAILS).setValue(PreferenceUtil.getFCMToken(mContext));
            new GraphRequest(AccessToken.getCurrentAccessToken(), "/" + userId + "/friends", null, HttpMethod.GET, new GraphRequest.Callback() {
                public void onCompleted(GraphResponse response) {
                    Log.i(TAG, response.toString());
                    try {
                        JSONArray jsonArrayFriends = response.getJSONObject().getJSONArray("data");
                        Log.i(TAG, jsonArrayFriends.toString());
                        if (jsonArrayFriends.length() == 0) {
                            mFacebookfriends.add(new FacebookFriendModel());
                            mFacebookFriendsAdapter = new FacebookFriendsAdapter(LandingActivity.this, R.layout.recycler_view_facebook_friends_error, mFacebookfriends, mFirebaseUser.getDisplayName(), mPlaceholderAccountDrawable);
                            mFacebookFriendsDrawerListView.setAdapter(mFacebookFriendsAdapter);
                        } else {
                            firebaseDatabase.getReference(userId).child(FirebaseUtil.USER_GENERAL_DETAILS).child(FirebaseUtil.USER_GENERAL_DETAILS_NUMBER_OF_FRIENDS).setValue(jsonArrayFriends.length());
                            for (int i = 0; i < jsonArrayFriends.length(); i++) {
                                FacebookFriendModel facebookFriendModel = new FacebookFriendModel();
                                facebookFriendModel.setUserId(jsonArrayFriends.getJSONObject(i).getString("id"));
                                facebookFriendModel.setUserName(jsonArrayFriends.getJSONObject(i).getString("name"));
                                mFacebookfriends.add(facebookFriendModel);
                            }
                            mFacebookFriendsAdapter = new FacebookFriendsAdapter(LandingActivity.this, R.layout.recycler_view_facebook_friends_item, mFacebookfriends, mFirebaseUser.getDisplayName(), mPlaceholderAccountDrawable);
                            mFacebookFriendsDrawerListView.setAdapter(mFacebookFriendsAdapter);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }).executeAsync();
        }
        mFacebookFriendsDrawerLayout.addDrawerListener(mFacebookFriendsDrawerToggle);
    }

    private Bitmap getBitmap(int drawableRes) {
        Drawable drawable = ActivityCompat.getDrawable(mContext, drawableRes);
        DrawableCompat.wrap(drawable).mutate().setTint(ActivityCompat.getColor(mContext, R.color.colorWhite));
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public void setTaskDescription() {
        TypedValue typedValue = new TypedValue();
        getTheme().resolveAttribute(R.attr.colorPrimary, typedValue, true);
        ActivityManager.TaskDescription taskDescription = new ActivityManager.TaskDescription(getString(R.string.app_name), getBitmap(R.drawable.ic_application_logo), typedValue.data);
        setTaskDescription(taskDescription);
    }

    public void showTimerDialog() {
        Log.i(TAG, "showTimerDialog()");
        mSleepTimerFragment.show(getSupportFragmentManager(), "");
    }

    public void shareTheApp() {
        Log.i(TAG, "shareTheApp()");
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.app_share_message));
        startActivity(Intent.createChooser(share, "Share Encore"));
    }
}