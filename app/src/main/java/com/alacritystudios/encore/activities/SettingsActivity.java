package com.alacritystudios.encore.activities;

import android.app.ActivityManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;

import com.alacritystudios.encore.R;
import com.alacritystudios.encore.application.EncoreApplication;
import com.alacritystudios.encore.fragments.SettingsFragment;
import com.alacritystudios.encore.utils.AppStartUtil;
import com.alacritystudios.encore.utils.FacebookLoginUtil;
import com.alacritystudios.encore.utils.PreferenceUtil;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(AppStartUtil.getCurrentColorSchemeTheme(EncoreApplication.getmContext(), true));
        setTaskDescription();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment(), "TAG")
                .commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getSupportFragmentManager().findFragmentByTag("TAG").onActivityResult(requestCode, resultCode, data);
    }

    private Bitmap getBitmap(int drawableRes) {
        Drawable drawable = ActivityCompat.getDrawable(EncoreApplication.getmContext(), drawableRes);
        DrawableCompat.wrap(drawable).mutate().setTint(ActivityCompat.getColor(EncoreApplication.getmContext(), R.color.colorWhite));
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public void setTaskDescription()
    {
        TypedValue typedValue = new TypedValue();
        getTheme().resolveAttribute(R.attr.colorPrimary, typedValue, true);
        ActivityManager.TaskDescription taskDescription = new ActivityManager.TaskDescription(getString(R.string.app_name), getBitmap(R.drawable.ic_application_logo), typedValue.data);
        setTaskDescription(taskDescription);
    }
}
