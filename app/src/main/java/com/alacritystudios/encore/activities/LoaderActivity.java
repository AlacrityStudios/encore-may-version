package com.alacritystudios.encore.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alacritystudios.encore.R;
import com.alacritystudios.encore.activities.base.BaseActivity;
import com.alacritystudios.encore.application.EncoreApplication;
import com.alacritystudios.encore.interfaces.MusicProviderCallback;
import com.alacritystudios.encore.providers.MusicProvider;
import com.alacritystudios.encore.utils.AnimationUtils;
import com.alacritystudios.encore.utils.AppStartUtil;
import com.alacritystudios.encore.utils.ComponentUtil;
import com.alacritystudios.encore.utils.FirebaseUtil;
import com.alacritystudios.encore.utils.PlaylistUtils;
import com.alacritystudios.encore.utils.PreferenceUtil;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;

public class LoaderActivity extends BaseActivity implements BaseActivity.BaseActivityImpl {

    private static final int MY_PERMISSIONS_REQUEST_RECORD_AUDIO = 322;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 323;

    private ImageView mApplicationLogoImageView;
    private ProgressBar mProgressBar;
    private TextView mStatusTextView;
    private ScaleAnimation mScaleAnimation;
    private Context mContext;

    private CallbackManager mFacebookCallbackManager;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    public void initialiseUI() {
        mApplicationLogoImageView = (ImageView) findViewById(R.id.application_logo);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mApplicationLogoImageView.setVisibility(View.VISIBLE);
        mApplicationLogoImageView.startAnimation(mScaleAnimation);
    }

    @Override
    public void setListeners() {
        mScaleAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                checkForPermissions();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    @Override
    public void setContextAndTags() {
        TAG = getClass().getSimpleName();
        mContext = LoaderActivity.this;
    }

    @Override
    public void initialiseOtherComponents() {
        mScaleAnimation = AnimationUtils.setScaleAnimation(400);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContextAndTags();
        setTheme(AppStartUtil.getCurrentColorSchemeTheme(mContext, false));
        setContentView(R.layout.activity_loader);
        initialiseOtherComponents();
        mFacebookCallbackManager = CallbackManager.Factory.create();
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
            }
        };
        LoginManager.getInstance().registerCallback(mFacebookCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AuthCredential credential = FacebookAuthProvider.getCredential(loginResult.getAccessToken().getToken());
                mAuth.signInWithCredential(credential).addOnCompleteListener((Activity) mContext, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser firebaseUser = mAuth.getCurrentUser();
                            if (firebaseUser != null && firebaseUser.getPhotoUrl() != null) {
                                PreferenceUtil.setFacebookImagePreference(mContext, firebaseUser.getPhotoUrl().toString());
                            }
                            FirebaseUtil.loginNewUser();
                        }
                        checkForFavoritePlaylist();
                        fetchMusicLibrary();
                    }
                });
            }

            @Override
            public void onCancel() {
                int a = 1 + 2;
            }

            @Override
            public void onError(FacebookException exception) {
                int a = 1 + 2;
            }
        });
        setListeners();
        initialiseUI();
    }

    /**
     * Asynchronously fetch the music library from device.
     */
    public void fetchMusicLibrary() {
        Log.i(TAG, "fetchMusicLibrary()");
        final MusicProvider musicProvider = new MusicProvider();
        musicProvider.retrieveMediaAsync(new MusicProviderCallback() {
            @Override
            public void onMusicCatalogReady(boolean success) {
                if (success) {
                    EncoreApplication.setmMusicProvider(musicProvider);
                    Intent intent = new Intent(mContext, LandingActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    /**
     * Check and create favorite playlist.
     */
    public void checkForFavoritePlaylist() {
        Log.i(TAG, "checkForFavoritePlaylist()");
        if (PreferenceUtil.getFavoritePlaylistId(mContext) == 0) {
            AppStartUtil.createFavoritePlaylist(mContext);
        } else {
            if (!PlaylistUtils.findFavoritePlaylist(mContext)) {
                AppStartUtil.createFavoritePlaylist(mContext);
            }
        }
    }

    /**
     *  Ask the user for his Facebook login.
     */
    public void askUserForFacebookLogin() {
        Log.i(TAG, "checkForFavoritePlaylist()");
        if (AccessToken.getCurrentAccessToken() == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.AlertDialogTheme);
            builder.setTitle(getString(R.string.login_with_facebook));
            builder.setMessage(getString(R.string.login_with_facebook_message));
            builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    LoginManager.getInstance().logInWithReadPermissions((Activity) mContext, Arrays.asList("public_profile", "user_friends"));
                }
            });
            builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    checkForFavoritePlaylist();
                    fetchMusicLibrary();
                }
            });
            final AlertDialog alertDialog = builder.create();
            final TypedValue typedValue = new TypedValue();
            mContext.getTheme().resolveAttribute(R.attr.colorAccent, typedValue, true);
            alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface arg0) {
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(typedValue.data);
                    alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(typedValue.data);
                }
            });
            alertDialog.show();

        } else {
            checkForFavoritePlaylist();
            fetchMusicLibrary();
        }
    }

    /**
     * Check for permission.
     * <p>
     * If present - Proceed with full functionality.
     * <p>
     * If not present - Ask user for permissions.
     */
    public void checkForPermissions() {
        Log.i(TAG, "checkForPermissions()");
        boolean isRecordAudioGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED;
        boolean isReadExternalStorageGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
        if (isReadExternalStorageGranted) {
            mProgressBar.setVisibility(View.VISIBLE);
            AppStartUtil.showUserAsOnline(mContext);
            askUserForFacebookLogin();
        } else {
            askPermissions(isRecordAudioGranted, isReadExternalStorageGranted);
        }
    }

    /**
     * Check for previously given persmissions
     */
    public void askPermissions(boolean isRecordAudioGranted, boolean isReadExternalStorageGranted) {
        Log.i(TAG, "askPermissions()");
//        if (!isRecordAudioGranted) {
//            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, MY_PERMISSIONS_REQUEST_RECORD_AUDIO);
//            return;
//        }
        if (!isReadExternalStorageGranted) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            return;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.i(TAG, "Return from taking permission()");
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_RECORD_AUDIO:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkForPermissions();
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECORD_AUDIO)) {
                        showRationaleForGivenString(getString(R.string.record_audio_permissions), MY_PERMISSIONS_REQUEST_RECORD_AUDIO);
                    } else {
                        ComponentUtil.errorAlertDialog(mContext, getString(R.string.permissions_error_message), true);
                    }
                }
                break;
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkForPermissions();
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        showRationaleForGivenString(getString(R.string.read_external_storage_permissions), MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    } else {
                        ComponentUtil.errorAlertDialog(mContext, getString(R.string.permissions_error_message), true);
                    }
                }
                break;
        }
    }


    /**
     * Show the rationale for a given String
     */
    private void showRationaleForGivenString(String inputString, final int permission) {
        Log.d(TAG, "showRationaleForGivenString() " + inputString);
        final TypedValue typedValue = new TypedValue();
        getTheme().resolveAttribute(R.attr.colorAccent, typedValue, true);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogTheme);
        final AlertDialog alertDialog = builder
                .setTitle("Permission Required")
                .setMessage(inputString)
                .setPositiveButton(getString(R.string.okay), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (permission) {
                            case MY_PERMISSIONS_REQUEST_RECORD_AUDIO:
                                ActivityCompat.requestPermissions((Activity) mContext, new String[]{Manifest.permission.RECORD_AUDIO}, MY_PERMISSIONS_REQUEST_RECORD_AUDIO);
                                dialog.dismiss();
                                break;
                            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                                ActivityCompat.requestPermissions((Activity) mContext, new String[]{Manifest.permission.RECORD_AUDIO}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                                dialog.dismiss();
                                break;
                            default:
                                dialog.dismiss();

                        }
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        checkForPermissions();
                    }
                })
                .setCancelable(false).create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(typedValue.data);
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(typedValue.data);
            }
        });
        alertDialog.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mFacebookCallbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
