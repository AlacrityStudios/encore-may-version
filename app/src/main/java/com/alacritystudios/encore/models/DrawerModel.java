package com.alacritystudios.encore.models;

/**
 * Model for storing drawer item data
 */

public class DrawerModel {
    private String name;
    private int drawableResourceId;

    public DrawerModel(String name, int drawableResourceId) {
        this.name = name;
        this.drawableResourceId = drawableResourceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDrawableResourceId() {
        return drawableResourceId;
    }

    public void setDrawableResourceId(int drawableResourceId) {
        this.drawableResourceId = drawableResourceId;
    }
}
