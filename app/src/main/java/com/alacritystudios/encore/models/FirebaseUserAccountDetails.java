package com.alacritystudios.encore.models;

/**
 * FirebaseUserAccountDetails model
 */

public class FirebaseUserAccountDetails {
    public String userTokenDetails;
    public boolean isPaidAccount;
    public int numberOfAvailableLikes;

    public FirebaseUserAccountDetails() {
    }

    public FirebaseUserAccountDetails(String userTokenDetails, boolean isPaidAccount, int numberOfAvailableLikes) {
        this.userTokenDetails = userTokenDetails;
        this.isPaidAccount = isPaidAccount;
        this.numberOfAvailableLikes = numberOfAvailableLikes;
    }

    public String getUserTokenDetails() {
        return userTokenDetails;
    }

    public void setUserTokenDetails(String userTokenDetails) {
        this.userTokenDetails = userTokenDetails;
    }

    public boolean isPaidAccount() {
        return isPaidAccount;
    }

    public void setPaidAccount(boolean paidAccount) {
        isPaidAccount = paidAccount;
    }

    public int getNumberOfAvailableLikes() {
        return numberOfAvailableLikes;
    }

    public void setNumberOfAvailableLikes(int numberOfAvailableLikes) {
        this.numberOfAvailableLikes = numberOfAvailableLikes;
    }
}
