package com.alacritystudios.encore.models;

/**
 * FirebaseUserLibraryDetails model
 */

public class FirebaseUserLibraryDetails {
    public String currentSong;
    public long karmaScore;
    public long numberOfSongs;
    public long numberOfSongsListened;
    public boolean onlineStatus;
    public boolean isListening;

    public FirebaseUserLibraryDetails() {
    }

    public FirebaseUserLibraryDetails(String currentSong, long karmaScore, long numberOfSongs, long numberOfSongsListened, boolean onlineStatus, boolean isListening) {
        this.currentSong = currentSong;
        this.karmaScore = karmaScore;
        this.numberOfSongs = numberOfSongs;
        this.numberOfSongsListened = numberOfSongsListened;
        this.onlineStatus = onlineStatus;
        this.isListening = isListening;
    }

    public String getCurrentSong() {
        return currentSong;
    }

    public void setCurrentSong(String currentSong) {
        this.currentSong = currentSong;
    }

    public long getKarmaScore() {
        return karmaScore;
    }

    public void setKarmaScore(long karmaScore) {
        this.karmaScore = karmaScore;
    }

    public long getNumberOfSongs() {
        return numberOfSongs;
    }

    public void setNumberOfSongs(long numberOfSongs) {
        this.numberOfSongs = numberOfSongs;
    }

    public long getNumberOfSongsListened() {
        return numberOfSongsListened;
    }

    public void setNumberOfSongsListened(long numberOfSongsListened) {
        this.numberOfSongsListened = numberOfSongsListened;
    }

    public boolean isOnlineStatus() {
        return onlineStatus;
    }

    public void setOnlineStatus(boolean onlineStatus) {
        this.onlineStatus = onlineStatus;
    }

    public boolean isListening() {
        return isListening;
    }

    public void setListening(boolean listening) {
        isListening = listening;
    }
}
