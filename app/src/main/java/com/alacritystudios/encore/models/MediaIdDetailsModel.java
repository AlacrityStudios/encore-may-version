package com.alacritystudios.encore.models;

import com.alacritystudios.encore.enums.MediaIdHierarchy;

import java.util.Map;

/**
 * Store mediaId related information about media
 */

public class MediaIdDetailsModel {

    private MediaIdHierarchy mMediaIdHierarchy;
    private long mMusicId;
    private String mHierarchyAwareMediaID;
    private Map<String, String> mMediaHierarchyMap;
    private String mRootHierarchy;
    private String mHighestTierHierarchy;

    public MediaIdHierarchy getmMediaIdHierarchy() {
        return mMediaIdHierarchy;
    }

    public void setmMediaIdHierarchy(MediaIdHierarchy mMediaIdHierarchy) {
        this.mMediaIdHierarchy = mMediaIdHierarchy;
    }

    public long getmMusicId() {
        return mMusicId;
    }

    public void setmMusicId(long mMusicId) {
        this.mMusicId = mMusicId;
    }

    public String getmHierarchyAwareMediaID() {
        return mHierarchyAwareMediaID;
    }

    public void setmHierarchyAwareMediaID(String mHierarchyAwareMediaID) {
        this.mHierarchyAwareMediaID = mHierarchyAwareMediaID;
    }

    public Map<String, String> getmMediaHierarchyMap() {
        return mMediaHierarchyMap;
    }

    public void setmMediaHierarchyMap(Map<String, String> mMediaHierarchyMap) {
        this.mMediaHierarchyMap = mMediaHierarchyMap;
    }

    public String getmRootHierarchy() {
        return mRootHierarchy;
    }

    public void setmRootHierarchy(String mRootHierarchy) {
        this.mRootHierarchy = mRootHierarchy;
    }

    public String getmHighestTierHierarchy() {
        return mHighestTierHierarchy;
    }

    public void setmHighestTierHierarchy(String mHighestTierHierarchy) {
        this.mHighestTierHierarchy = mHighestTierHierarchy;
    }
}
