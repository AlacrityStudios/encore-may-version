package com.alacritystudios.encore.models;

/**
 * FirebaseUserModel model
 */

public class FirebaseUserModel {

    private FirebaseUserAccountDetails userAccountDetails;
    private FirebaseUserGeneralDetails userGeneralDetails;
    private FirebaseUserLibraryDetails userLibraryDetails;

    public FirebaseUserModel() {
    }

    public FirebaseUserModel(FirebaseUserAccountDetails userAccountDetails, FirebaseUserGeneralDetails userGeneralDetails, FirebaseUserLibraryDetails userLibraryDetails) {
        this.userAccountDetails = userAccountDetails;
        this.userGeneralDetails = userGeneralDetails;
        this.userLibraryDetails = userLibraryDetails;
    }

    public FirebaseUserAccountDetails getUserAccountDetails() {
        return userAccountDetails;
    }

    public void setUserAccountDetails(FirebaseUserAccountDetails userAccountDetails) {
        this.userAccountDetails = userAccountDetails;
    }

    public FirebaseUserGeneralDetails getUserGeneralDetails() {
        return userGeneralDetails;
    }

    public void setUserGeneralDetails(FirebaseUserGeneralDetails userGeneralDetails) {
        this.userGeneralDetails = userGeneralDetails;
    }

    public FirebaseUserLibraryDetails getUserLibraryDetails() {
        return userLibraryDetails;
    }

    public void setUserLibraryDetails(FirebaseUserLibraryDetails userLibraryDetails) {
        this.userLibraryDetails = userLibraryDetails;
    }
}
