package com.alacritystudios.encore.models;

/**
 * FirebaseUserGeneralDetails model
 */

public class FirebaseUserGeneralDetails {
    public String userName;
    public String userEmail;
    public String userPhotoUrl;
    public Long numberOfFriends;

    public FirebaseUserGeneralDetails() {
    }

    public FirebaseUserGeneralDetails(String userName, String userEmail, String userPhotoUrl, Long numberOfFriends) {
        this.userName = userName;
        this.userEmail = userEmail;
        this.userPhotoUrl = userPhotoUrl;
        this.numberOfFriends = numberOfFriends;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPhotoUrl() {
        return userPhotoUrl;
    }

    public void setUserPhotoUrl(String userPhotoUrl) {
        this.userPhotoUrl = userPhotoUrl;
    }

    public Long getNoOfFriends() {
        return numberOfFriends;
    }

    public void setNoOfFriends(Long noOfFriends) {
        this.numberOfFriends = noOfFriends;
    }
}
