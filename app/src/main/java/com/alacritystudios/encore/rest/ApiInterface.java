package com.alacritystudios.encore.rest;

import com.google.gson.JsonObject;
import com.squareup.okhttp.ResponseBody;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Header;
import retrofit.http.POST;

/**
 * Retrofit interface
 */

public interface ApiInterface {
    @POST("send")
    public Call<ResponseBody> sendLikesMessage(@Body JsonObject body, @Header("Authorization") String key);
}
