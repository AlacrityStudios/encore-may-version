package com.alacritystudios.encore.components;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/**
 * A custom frame layout to suit our needs.
 */

public class CustomFrameLayout extends FrameLayout {

    private float mSlidingOffset;
    private int mContainerLayoutHeight;

    public CustomFrameLayout(@NonNull Context context) {
        super(context);
    }

    public CustomFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CustomFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr, @StyleRes int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public float getmSlidingOffset() {
        return mSlidingOffset;
    }

    public void setmSlidingOffset(float mSlidingOffset) {
        this.mSlidingOffset = mSlidingOffset;
    }

    public int getmContainerLayoutHeight() {
        return mContainerLayoutHeight;
    }

    public void setmContainerLayoutHeight(int mContainerLayoutHeight) {
        this.mContainerLayoutHeight = mContainerLayoutHeight;
    }
}
