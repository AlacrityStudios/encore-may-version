package com.alacritystudios.encore.preferences;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.preference.DialogPreference;
import android.util.AttributeSet;

import com.alacritystudios.encore.R;

/**
 * Preference for representing color scheme.
 */

public class ColorSchemePickerPreference extends DialogPreference {

    private String TAG = getClass().getSimpleName();
    private int mColorScheme;
    private int mDialogLayoutResId = R.layout.dialog_color_scheme_picker;

    public ColorSchemePickerPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTitle(R.string.color_scheme);
        setPositiveButtonText(R.string.okay);
        setNegativeButtonText(R.string.cancel);
        setDialogIcon(null);
    }

    public int getmColorScheme() {
        return mColorScheme;
    }

    public void setmColorScheme(int mColorScheme) {
        this.mColorScheme = mColorScheme;
        persistInt(mColorScheme);
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getInt(index, 0);
    }

    @Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {
        setmColorScheme(restorePersistedValue ? getPersistedInt(mColorScheme) : (int) defaultValue);
    }

    @Override
    public int getDialogLayoutResource() {
        return mDialogLayoutResId;
    }

}
