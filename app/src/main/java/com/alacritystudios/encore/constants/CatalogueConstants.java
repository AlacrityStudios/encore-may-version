package com.alacritystudios.encore.constants;

/**
 * Stores commonly used catalogue contants.
 */

public class CatalogueConstants {
    public static final String CATALOGUE_REFRESH = "__CATALOGUE_REFRESH__";

    public interface GeneralConstants {
        String mUnknownName = "<unknown>";
        long mUnknownId = 0;
        String CUSTOM_METADATA_TRACK_SOURCE = "__SOURCE__";
        String CUSTOM_METADATA_ARTIST_ID = "__ARTIST_ID__";
        String CUSTOM_METADATA_ALBUM_ID = "__ALBUM_ID__";
    }

    public interface UriConstants {
        String mAlbumArtBaseUri = "content://media/external/audio/albumart";
    }

    public interface SongConstants {
        String DURATION = "DURATION";
    }
}
